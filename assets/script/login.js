$(document).ready(function() {
    $('#form-login').submit(function(e) {
        e.preventDefault();

        var login = $('#login').val();
        var pwd = $('#pwd').val();
        
        $('#msgbox').removeClass().addClass('msgboxloading').text('Loading...').fadeIn(1000);
        
        $.ajax({
            url:'portal/xlogin',
            type: 'POST',
            async: false,
            dataType: 'json',
            data: { login: login, pwd: pwd },
            success: function(res) {
                if(res.status == 1)
                    document.location = res.parameter;
                else {
                    //alert(res.parameter);
                    $("#msgbox").fadeTo(200, 0.1, function()
                    { 
                        $(this).html('Login failed...').addClass('msgboxerr').fadeTo(900, 1);
                    });
                }
            }
        });
    });
});