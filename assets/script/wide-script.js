var _notification_delay_refresh = 60000;
var _relance_delay_refresh = 70000;

/* Modal dialog functions */
function showModalDialog(idDialog)
{
    $("#" + idDialog).modal();
}

function hideModalDialog()
{
    $.modal.close();
}

/* Regexp validation */
function testPattern(value, pattern, option)
{
    if(!option)
        option = '';

    var regExp = new RegExp(pattern,"");
    
    return regExp.test(value);
}

function redirect(url)
{
    document.location = url;
}

function isNumber(val) {
    return ( ! isNaN(val) );
}

function initCurrentyMask(elt) {
    if( elt ) {
        $(elt).autoNumeric('init');
    
        return false;
    }
    
    $('.currency').autoNumeric('init');
}

function disableCurrencyMask() {
    $('.currency').autoNumeric('destroy');
}