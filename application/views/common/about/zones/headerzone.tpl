        <!-- UI dialogs -->
        {logout_dialog_content}
        <!-- End of UI dialogs -->
        <div id="topnav">
            <ul>
                <li id="first"><a href="{base_url}homepage">Accueil</a></li>
                <li class="active"><a href="#">A propos</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description" style="width: 700px;">
                "Have not I commanded thee? Be strong and of a good courage; be not afraid, neither be thou dismayed: for the Lord thy God is with thee whithersoever thou goest." Joshua 1 :9.
            </p>
            <p></p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" /></a>
            </div>
        </div>
        <div class="clear"></div>  