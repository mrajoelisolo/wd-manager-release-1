<script type="text/javascript">
    function logout()
    {
        $.ajax({
            url: '{base_url}portal/xlogout',
            type: 'get',
            async: false,
            datatype: 'html',
            success: function(data) {
                document.location = data;
            }
        });
    }
    
    $(function() {
       $('#btn_logout').bind('click', function() {
            //UI Dialog
            var idDialogAddCotation = "#dialog-confirm-logout";                   
            $( idDialogAddCotation ).dialog( "destroy" );
            $( idDialogAddCotation ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Oui": function() {
                        logout(); //Call from
				    },
 			        "Non": function() {
                        $( this ).dialog( "close" );
                    }
 			    }
            });
            //end of dialog                        
        }); 
    });
</script>