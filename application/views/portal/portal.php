<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <link href="<?php echo css_url('portal'); ?>" rel="stylesheet" type="text/css" />
	<script src="<?php  echo js_url('jquery.min'); ?>"></script>
    <script src="<?php  echo js_url('login'); ?>"></script>

	<title>Wide manager - portal</title>
</head>

<body>

<div id="wrap">
    <div id="login-header">
        <h1 id="sitename">
            Wide manager
            <img src="<?php echo path_img(); ?>beta_icon.png" alt="beta_logo">
            <span id="description">The contract of confidence</span>
        </h1>
        <div class="clear"></div>
        <div id="login-content">
            <form id="form-login" action="#" method="post">
                <fieldset>
                    <label for="login">
                        Login:
                        <input class="inputtext" type="text" id="login" name="login" />
                    </label>
                    <label for="pwd">
                        Password:
                        <input class="inputtext" type="password" id="pwd" name="pwd" />
                    </label>
                    <p><input id="btn-login" type="submit" value="Login" /><span id="msgbox" style="display:none"></span></p>
                </fieldset>
            </form>
        </div>
    </div>
    <div id="login-footer">
        <div class="footer-content">
            <p>Copyright (c) 2013 - Myrmidon Software Engineering</p>
        </div>
    </div>
</div>

</body>
</html>