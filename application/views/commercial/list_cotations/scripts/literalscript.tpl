<script type="text/javascript">
    $(function(){
        $('#txtClient').click(function() {
            $('#chkClient').attr('checked', '');
        });

        $.post("{base_url}services/dateservices/today", function(data) {
            $( "#txtDateReception" ).val(data);
        })
        
        $( "#txtDateReception" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {
                $('#chkDateReception').attr('checked', '');
            }
        });
    
        $('#btnRechercher').click(function() {
            var f_client         = $('#txtClient').val();
            var f_date_reception = $('#txtDateReception').val();
    
            var criteria = '';
    
            if( $('#chkClient').attr('checked') ) {
                if( f_client != '' ) {
                    criteria += 'customer/' +  f_client;
                }
            }
    
            if( $('#chkDateReception').attr('checked') ) {
                if( f_date_reception != '' ) {
                    if( criteria != '' )
                        criteria += '/';
        
                    criteria += 'date_reception/' + f_date_reception.replace('/', '-').replace('/', '-');
                }
            }
    
            document.location.href = '{base_url}commercial/list_cotations/page/' + criteria;
        });
    
        $(".uibtn").button();
    });
</script>