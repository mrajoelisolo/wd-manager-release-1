        <div id="homepage">
            <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="3">Filtre de recherche</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="checkbox" id="chkClient"></td>
                        <td class="col1"><strong>Client</strong></td>
                        <td><input class="input-txt" type="text" name="txtClient" id="txtClient"></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox" id="chkDateReception"></td>
                        <td><strong>Date de réception</strong></td>
                        <td><input class="input-date" type="text" name="txtDateReception" id="txtDateReception"></td>
                    </tr>
                    <tr>
                        <td colspan="3"><a href="{base_url}commercial/list_cotations">Afficher tout</a></td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="button" class="uibtn" id="btnRechercher" value="Rechercher"></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table class="inner-tbl-edit" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>N° PRF</th>
                                        <th>Client</th>
                                        <th>Date de réception</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {var_cotations}
                                    <tr id="cadre_row_{cadre_id}">
                                        <td>{cotation_number}</td>
                                        <td>{customer_name}</td>
                                        <td>{strDateReception}</td>
                                        <td>
                                            <a href="{base_url}commercial/sended_proforma_details/show/{cotation_id}" target="_blank">Voir proforma</a>
                                        </td>
                                    </tr>
                                    {/var_cotations}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            {is_pagination}
                            <div class="wrap-pagination">
                                {var_pagination}
                            </div>
                            {/is_pagination}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>