<script type="text/javascript">
    $(function() {
        loadSendedCotations(); //Call
        
        $("#tbl-sended-cotations-data tbody tr").live('click', function() {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/commercialservices/fetch_sended_cotation',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call
                    setSendedCotationForm(json, tsel); //Call
                }
            });
        });
        
        $('#btnAccept').click(function() {
            $( '#dialog-confirm-accept-cotation' ).dialog( "destroy" );
            $( '#dialog-confirm-accept-cotation' ).dialog({
                resizable: false,
                height:140,
                modal: true,
     			buttons: {
 			        "Confirmer": function() {
 			            $.ajax({
                            url: "{base_url}services/commercialservices/accept_sended_cotation",
                            type: "post",
                            dataType: "json",
                            data: $("#form-sended-cotation").serialize(),
                            success: function(res) {
                                $( '#dialog-confirm-accept-cotation' ).dialog( "close" );
                                hideModalDialog('dialog-box');        
                                $('#' + res.id_selected_row).remove();
                            }
                        });
                    },
                    "Annuler": function() {
                        $( '#dialog-confirm-accept-cotation' ).dialog( "close" );
                    }
                }
            });
        });

        $('#btnReject').click(function() {
            $( '#dialog-confirm-reject-cotation' ).dialog( "destroy" );
            $( '#dialog-confirm-reject-cotation' ).dialog({
                resizable: false,
                height:140,
                modal: true,
     			buttons: {
     			    "Confirmer": function() {
                        $.ajax({
                            url: "{base_url}services/commercialservices/reject_sended_cotation",
                            type: "post",
                            dataType: "json",
                            data: $("#form-sended-cotation").serialize(),
                            success: function(res) {
                            $( '#dialog-confirm-reject-cotation' ).dialog( "close" );
                                hideModalDialog('dialog-box');

                                $('#' + res.id_selected_row).remove();
                            }
                        });
                    },
                    "Annuler": function() {
                        $( '#dialog-confirm-reject-cotation' ).dialog( "close" );
                    }
                }
            });
        });
        
        $('#btnShowReport').click(function() {
            var idCotation = $('#txtSendedCotationId').val();
            
            window.open('{base_url}commercial/pending_proforma_details/show/' + idCotation, '_blank');
        });
        
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
    });
</script>