<script type="text/javascript">
    /**
     * Charger la liste des cotations dont le status est envoyé
     */
    function loadSendedCotations(destroy)
    {
        if( ! destroy )
            destroy = false;

        $('#tbl-sended-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_sended_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    /**
     * Affiche les détails de la cotation selectionné dans la liste
     */
    function setSendedCotationForm(json, idSelectedRow)
    {
        clearSendedCotationForm(); //Call
        
        $('#idSelectedRow').val(idSelectedRow);
        
        $('#txtSendedCotationId').val(json.cotation_id);
        $('#cmbCotationSource').val(json.cotation_source);
        $('#txtCotationReceptionDate').val(json.reception_date);
        $('#txtCotationLimitSendDate').val(json.limit_send_date);
        $('#cmbCotationCommercial').val(json.commercial_fullname);
        $('#txtCotationNumberCustomer').val(json.cotation_number_customer);
        $('#txtCotationNumber').val(json.cotation_number);
        $('#txtCotationCustomerName').val(json.customer_name);
        $('#txtCotationCustomerAddress').val(json.customer_address);
        $('#txtCotationCustomerMail').val(json.customer_mail);
        $('#txtCotationCustomerPhone').val(json.customer_phone);
        $('#txtCotationPurchaseName').val(json.purchaser_name);
        $('#txtCotationValidityStart').val(json.validity_start);
        $('#txtCotationValidityEnd').val(json.validity_end);
        
        for( var i = 0; i < json.request_proformas.length; i++ )
        {
            var proforma = json.request_proformas[i];

            addNewCotationRequestRow(proforma.designation, proforma.qty); //Call                    
        }
        
        for( var i = 0; i < json.proposition_proformas.length; i++ )
        {
            var proforma = json.proposition_proformas[i];
 
            addNewCotationPropositionRow(proforma.designation, proforma.qty, proforma.price_unit); //Call                    
        }

        removeFirstCotationRequestProformaRow(); //Call
        removeFirstCotationPropositionProformaRow(); //Call
    }
    
    /**
     * Ajoute une ligne dans la designation demande
     */
    function addNewCotationRequestRow(designation, qty)
    {
        var idTable = 'tbl-cotation-request-proforma';
        
        var lastRow = $('#' + idTable + ' tr:last');
        var rowCount = $('#' + idTable + ' tr').size();
        var newRow = lastRow.clone();
        
        newRow.children().each(function(k, v) {
            var content = $(v).children()[0];
    
            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(designation); break;
                case 3: $(content).val(qty); break;
            }
        });
        lastRow.after(newRow);
    }
    
    /**
     * Enlève le premier élément de la liste des proposition de désignations
     */
    function removeFirstCotationRequestProformaRow()
    {
        $('#tbl-cotation-request-proforma tr').each(function(i, row)
        {
            if(i == 1)
            {
                $(row).remove();
                return false;
            }
        });
    }
    
    /**
     * Efface les lignes des désignations de demandes
     */
    function clearCotationRequestProformaRows()
    {
        $('#tbl-cotation-request-proforma tr').each(function(i, row) {
            //Proforma designation
            var oDesignation = $(row).children()[2];    
            var txtDesignation = $(oDesignation).children()[0];
    
            //Proforma quantity
            var oQty = $(row).children()[3];    
            var txtQty = $(oQty).children()[0];
    
            if( i == 1 )
            {                            
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /**
     * Ajoute une nouvelle ligne dans la proposition de désignations
     */
    function addNewCotationPropositionRow(designation, qty, price) {
        var idTable = 'tbl-cotation-proposition-proforma';

        var lastRow = $('#' + idTable + ' tr:last');
        var rowCount = $('#' + idTable + ' tr').size();
                   
        var newRow = lastRow.clone();
        newRow.children().each(function(k, v) {
            var content = $(v).children()[0];

            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(designation); break;
                case 3: $(content).val(qty); break;
                case 4: $(content).val(price); break;
            }
        });
        lastRow.after(newRow);
    }
    
    /**
     * Supprime le premier enregistrement des cotations dans la proposition de designations
     */
    function removeFirstCotationPropositionProformaRow()
    {
        $('#tbl-cotation-proposition-proforma tr').each(function(i, row){
            if(i == 1)
            {
                $(row).remove();
                return false;
            }
        });
    }
    
    /**
     * Efface les lignes de la designation de propositions
     */
    function clearCotationPropositionProformaRows()
    {
        $('#tbl-cotation-proposition-proforma tr').each(function(i, row) {
            //Proforma designation
            var oDesignation = $(row).children()[2];    
            var txtDesignation = $(oDesignation).children()[0];
    
            //Proforma quantity
            var oQty = $(row).children()[3];    
            var txtQty = $(oQty).children()[0];
            
            if( i == 1 )
            {                            
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /**
     * Vide le formulaire d'édition de cotations envoyés
     */
    function clearSendedCotationForm()
    {
        $('#txtCotationNumber').val('');

        clearCotationRequestProformaRows(); //Call
        clearCotationPropositionProformaRows(); //Call
    }

    /**
     * Charger la liste des cotations dont le status est envoyé
     */
    function loadSendedCotations(destroy)
    {
        if( ! destroy )
            destroy = false;

        $('#tbl-sended-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_sended_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    /**
     * Affiche les détails de la cotation selectionné dans la liste
     */
    function setSendedCotationForm(json, idSelectedRow)
    {
        clearSendedCotationForm(); //Call
        
        $('#idSelectedRow').val(idSelectedRow);
        
        $('#txtSendedCotationId').val(json.cotation_id);
        $('#cmbCotationSource').val(json.cotation_source);
        $('#txtCotationReceptionDate').val(json.reception_date);
        $('#txtCotationLimitSendDate').val(json.limit_send_date);
        $('#cmbCotationCommercial').val(json.commercial_fullname);
        $('#txtCotationNumberCustomer').val(json.cotation_number_customer);
        $('#txtCotationNumber').val(json.cotation_number);
        $('#txtCotationCustomerName').val(json.customer_name);
        $('#txtCotationCustomerAddress').val(json.customer_address);
        $('#txtCotationCustomerMail').val(json.customer_mail);
        $('#txtCotationCustomerPhone').val(json.customer_phone);
        $('#txtCotationPurchaseName').val(json.purchaser_name);
        $('#txtCotationValidityStart').val(json.validity_start);
        $('#txtCotationValidityEnd').val(json.validity_end);
        
        for( var i = 0; i < json.request_proformas.length; i++ )
        {
            var proforma = json.request_proformas[i];

            addNewCotationRequestRow(proforma.designation, proforma.qty); //Call                    
        }
        
        for( var i = 0; i < json.proposition_proformas.length; i++ )
        {
            var proforma = json.proposition_proformas[i];
 
            addNewCotationPropositionRow(proforma.designation, proforma.qty, proforma.price_unit); //Call                    
        }

        removeFirstCotationRequestProformaRow(); //Call
        removeFirstCotationPropositionProformaRow(); //Call
    }
    
    /**
     * Ajoute une ligne dans la designation demande
     */
    function addNewCotationRequestRow(designation, qty)
    {
        var idTable = 'tbl-cotation-request-proforma';
        
        var lastRow = $('#' + idTable + ' tr:last');
        var rowCount = $('#' + idTable + ' tr').size();
        var newRow = lastRow.clone();
        
        newRow.children().each(function(k, v) {
            var content = $(v).children()[0];
    
            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(designation); break;
                case 3: $(content).val(qty); break;
            }
        });
        lastRow.after(newRow);
    }
    
    /**
     * Enlève le premier élément de la liste des proposition de désignations
     */
    function removeFirstCotationRequestProformaRow()
    {
        $('#tbl-cotation-request-proforma tr').each(function(i, row)
        {
            if(i == 1)
            {
                $(row).remove();
                return false;
            }
        });
    }
    
    /**
     * Efface les lignes des désignations de demandes
     */
    function clearCotationRequestProformaRows()
    {
        $('#tbl-cotation-request-proforma tr').each(function(i, row) {
            //Proforma designation
            var oDesignation = $(row).children()[2];    
            var txtDesignation = $(oDesignation).children()[0];
    
            //Proforma quantity
            var oQty = $(row).children()[3];    
            var txtQty = $(oQty).children()[0];
    
            if( i == 1 )
            {                            
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /**
     * Ajoute une nouvelle ligne dans la proposition de désignations
     */
    function addNewCotationPropositionRow(designation, qty, price) {
        var idTable = 'tbl-cotation-proposition-proforma';

        var lastRow = $('#' + idTable + ' tr:last');
        var rowCount = $('#' + idTable + ' tr').size();
                   
        var newRow = lastRow.clone();
        newRow.children().each(function(k, v) {
            var content = $(v).children()[0];

            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(designation); break;
                case 3: $(content).val(qty); break;
                case 4: $(content).val(price); break;
            }
        });
        lastRow.after(newRow);
    }
    
    /**
     * Supprime le premier enregistrement des cotations dans la proposition de designations
     */
    function removeFirstCotationPropositionProformaRow()
    {
        $('#tbl-cotation-proposition-proforma tr').each(function(i, row){
            if(i == 1)
            {
                $(row).remove();
                return false;
            }
        });
    }
    
    /**
     * Efface les lignes de la designation de propositions
     */
    function clearCotationPropositionProformaRows()
    {
        $('#tbl-cotation-proposition-proforma tr').each(function(i, row) {
            //Proforma designation
            var oDesignation = $(row).children()[2];    
            var txtDesignation = $(oDesignation).children()[0];
    
            //Proforma quantity
            var oQty = $(row).children()[3];    
            var txtQty = $(oQty).children()[0];
            
            if( i == 1 )
            {                            
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /**
     * Vide le formulaire d'édition de cotations envoyés
     */
    function clearSendedCotationForm()
    {
        $('#txtCotationNumber').val('');

        clearCotationRequestProformaRows(); //Call
        clearCotationPropositionProformaRows(); //Call
    }
</script>