        <div id="homepage">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-sended-cotations-data" width="100%">
                <thead>
            		<tr>
                        <th>Cotation Id</th>
                        <th>N&ordm; PRF</th>
                        <th>Client</th>
                        <th>Designation demande</th>
                        <th>Designation proposition</th>
                        <th>DEB</th>
                        <th>FIN</th>
            		</tr>
            	</thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <!-- Modal dialog -->
        <div id="dialog-overlay"></div>
    	<div id="dialog-box">
            <div class="dialog-header">
                <h2>Cotation envoy&eacute;</h2>
            </div>
    		<div class="dialog-content">
    			<div id="dialog-message">
                    <!-- Load the form with from template parser -->
                    {content_pending_dispatch}
                </div>
            </div>
            <div class="dialog-footer">
                <p>
                    <input type="submit" id="btnReject" value="Non accord&eacute;" class="btnGradientRed" />
                    &nbsp;
                    <input type="submit" id="btnAccept" value="Accord&eacute;" class="btnGradientGreen" />
                    &nbsp;
                    <input type="submit" id="btnShowReport" value="Afficher etat" class="btnGradientGreen" />
                    &nbsp;
                    <input type="submit" id="btnClose" value="Fermer" class="btnGradientBlue" />
                </p>
            </div>
        </div>