        <!-- UI dialogs -->
        {logout_dialog_content}
        <!-- End of UI dialogs -->
        <div id="topnav">
            <ul>
                <!--
                <li id="first"><a href="{base_url}homepage">Accueil</a></li>
                <li><a href="{base_url}/commercial/list_cotations">Cotations traitées</a></li>
                -->
                <li class="active"><a href="#">Détails de proforma</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}todo_img.png" width="64px" height="64px" />
                Détails de proforma
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" /></a>
            </div>
        </div>
        <div class="clear"></div>  