<script type="text/javascript">
    function createPDFReport() {
        $('#form-report-cotation').submit();    
    }
    
    function backToPreviousPage() {
        document.location.href = '{base_url}commercial/list_cotations';
    }
    
    $(function() {
        $( "#txtCotationDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {}
        });
    });
</script>