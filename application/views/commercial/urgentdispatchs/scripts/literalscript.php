<script type="text/javascript">
    /**
     * Charger la liste des dispatchs en attente (chargement spécifique pour les cotations urgentes)
     */
    function loadUrgentDispatchs(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-urgent-dispatchs-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_urgent_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }

    //Calls after document loading
    $(function() {
        loadUrgentDispatchs(); //Call
        
        $("#tbl-urgent-dispatchs-data tbody tr").live('click', function() {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/commercialservices/fetch_pending_dispatch',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call from pendingcotations scripts
                    setPendingDispatchForm(json, tsel); //Call
                }
            });
        });
        
        /**
         * Button event handlers
         */
        $('#btnSend').click(function() {
            //Input validation
            var bFormValidated = validateDispatchForm();
            if( ! bFormValidated )
            {
                $( '#dialog-input-error' ).dialog( "destroy" );
                $( '#dialog-input-error' ).dialog({
                    resizable: false,
                    height: 140,
                    modal: true,
         			buttons: {
     			        "OK": function() {
     			            $( '#dialog-input-error' ).dialog( "close" );
                        }
     			    }
                });        
                
                return false;
            }

            //disableCurrencyMask();

            //UI Dialog
            var idDialogSendCotation = "#dialog-confirm-send-cotation";                   
            $( idDialogSendCotation ).dialog( "destroy" );
            $( idDialogSendCotation ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Confirmer": function() {
				        $.ajax({
                            url: "{base_url}services/commercialservices/send_dispatch",
                            type: "post",
                            dataType: "json",
                            data: $("#form-pending-dispatch").serialize(),
                            success: function(res) {
                                $( idDialogSendCotation ).dialog( "close" );
                                hideModalDialog('dialog-box');
                                //loadData(true); //instead of use this, use jQuery
                                $('#' + res.id_selected_row).remove();
                                
                                //Report the proforma
                                document.location.href = res.redirection; 
                            }
                        });
                    },
                    "Annuler": function() {
   					    $( idDialogSendCotation ).dialog( "close" );
                    }
                }
            });
            //end of dialog
        });
        
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
                
        $('#btn-dispatch-add-row1,#btn-dispatch-add-row2').click(function() {
            addDispatchPropositionProformaRow(); return false; //Call from pendingcotations scripts
        });
        
        $('#btn-dispatch-remove-selected-rows1,#btn-dispatch-remove-selected-rows2').click(function() {
            removeSelectedPropositionProformaRows(); return false; //Call from pendingcotations scripts
        });
        
        $('#btn-copy-requests-to-propositions').click(function() {
            copyRequestsToPropositions(); //Call from pendingcotation scripts
        });
        
        /***
         * UI button appearance
         */
        $( ".inner-btn" ).button();
    });
</script>