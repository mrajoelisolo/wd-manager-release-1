        <div id="homepage">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-urgent-dispatchs-data" width="100%">
                <thead>
            		<tr>
                        <th>Cotation Id</th>
                        <th>N&ordm; cotation client</th>
                        <th>Client</th>
                        <th>Designation demande</th>
                        <th>Date limite d'envoi</th>
            		</tr>
            	</thead>
                <tbody>
                </tbody>
            </table>
        </div>
        
        <!-- Modal dialog -->
        <div id="dialog-overlay"></div>
    	<div id="dialog-box">
            <div class="dialog-header">
                <h2>Dispatch urgent</h2>
            </div>    
    		<div class="dialog-content">
    			<div id="dialog-message">
                    <!-- Load the form with from template parser -->
                    {content_pending_dispatch}
                </div>
            </div>
            <div class="dialog-footer">
                <p>
                    <input type="button" id="btnSend" value="Traiter" class="btnGradientGreen" />
                    &nbsp;
                    <input type="button" id="btnClose" value="Fermer" class="btnGradientBlue" />
                </p>
            </div>
        </div>