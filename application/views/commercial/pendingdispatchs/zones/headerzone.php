        <!-- UI dialogs -->
        {logout_dialog_content}
        
        <div id="dialog-confirm-send-cotation" title="Envoi de cotations" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Confirmez vous l'envoi de cotation ?</p>
        </div>
        
        <div id="dialog-input-error" title="Saisie incorrecte" style="display: none;">
	       <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Votre saisie est incompl&ecirc;te ou incorrecte</p>
        </div>
        <!-- End of UI dialogs -->
        
        <div id="topnav">
            <ul>
                <li id="first"><a href="{base_url}homepage">Accueil</a></li>
                <li class="active"><a href="#">Cotations</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}edit_img.png" width="64px" height="64px" />
                Liste des cotations &aacute; envoyer
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" /></a>
            </div>
        </div>
        <div class="clear"></div>  