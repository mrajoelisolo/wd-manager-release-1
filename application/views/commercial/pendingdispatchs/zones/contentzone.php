        <div id="homepage">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbldata" width="100%">
                <thead>
            		<tr>
                        <th>Cotation Id</th>
                        <th>N&ordm; cotation client</th>
                        <th>Client</th>
                        <th>Designation demande</th>
                        <th>Date limite d'envoi</th>
            		</tr>
            	</thead>
                <tbody>
                </tbody>
            </table>
        </div>
        
        <!-- Modal dialog -->
        <div id="dialog-overlay"></div>
    	<div id="dialog-box">
            <div class="dialog-header">
                <h2>Dispatch a traiter</h2>
            </div>
    		<div class="dialog-content">
    			<div id="dialog-message">
                    <!-- Load the form with from template parser -->
                    {content_pending_dispatch}
                </div>
            </div>
            <div class="dialog-footer">
                <p>
                    <input type="button" id="btnSend" value="Envoyer" class="btnGradientGreen" />
                    &nbsp;
                    <input type="button" id="btnClose" value="Fermer" class="btnGradientBlue" />
                </p>
            </div>
        </div>

        <div class="message-dialog" id="dialog-new-dispatchs">    
      		<div class="dialog-content">
     			<div id="dialog-message">
                    <!-- Load data from ajax -->
                    <div style="float:right">
                        <img src="{path_img}new_message_img.png" height="48px" alt="todo_img"/>
                    </div>
                    <h2 class="subhead">Message</h2>
                    <strong>Vous avez reçu de nouveaux dispatchs</strong>
                </div>
            </div>
            <div class="dialog-footer">
                <p>
                    <input type="submit" onclick="closeModalDialog();" value="Fermer" class="btnGradientRed" />
                </p>
            </div>
        </div>