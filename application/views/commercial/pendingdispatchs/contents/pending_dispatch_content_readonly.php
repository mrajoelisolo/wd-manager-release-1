<form id="form-pending-dispatch" action="#" method="post">
    <input type="hidden" name="idSelectedRow" id="idSelectedRow" value="" />
    <input type="hidden" id="txtCotationId" name="txtCotationId" value="" />
    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2">G&eacute;n&eacute;ral</th>
        </tr>
        <tr>
            <td class="col1"><strong>Source</strong></td>
            <td class="col2">
                <input class="input-txt" type="text" name="cmbCotationSource" id="cmbDispatchSource" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td class="col1"><strong>Date r&eacute;ception</strong></td>
            <td class="col2"><input class="input-date" type="text" name="txtReceptionDate" id="txtDispatchReceptionDate" readonly="readonly" /></td>
        </tr>
        <tr>
            <td class="col1"><strong>Date limite d'envoi</strong></td>
            <td class="col2"><input class="input-date" type="text" name="txtLimitSendDate" id="txtDispatchLimitSendDate" readonly="readonly" /></td>
        </tr>
        <tr>
            <td class="col1"><strong>Commercial</strong></td>
            <td class="col2">
                <input class="input-txt" type="text" name="cmbCommercial" id="cmbDispatchCommercial" readonly="readonly" />
            </td>
        </tr>
        <tr>
            <td class="col1"><strong>Num&eacute;ro cotation client</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtNumberCustomer" id="txtDispatchNumberCustomer" readonly="readonly" /></td>
        </tr>
    </table>
                    
    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2">Designation demande</th>
        </tr>
        <tr>
            <td class="col2">
                <table id="tbl-dispatch-request-proforma" class="inner-tbl-edit" cellspacing="0">
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Référence</th>
                        <th>Designation</th>
                        <th>Quantit&eacute;</th>
                    </tr>
                    <tr>
                        <td class="col1"><strong>1</strong></td>
                        <td class="col2"><input type="checkbox" /></td>
                        <td class="col4"><input class="input-txt" type="text" name="arrReferences[]" readonly="readonly"></td>
                        <td class="col3"><textarea name="arrDesignations[]" readonly="readonly" ></textarea></td>
                        <td class="col4"><input class="input-txt" type="text" name="arrQuantitys[]" value="0" readonly="readonly"  /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                    
    <input type="hidden" name="txtCustomerId" id="txtDispatchCustomerId" value="-1" />
    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
        <th colspan="2">Coordonn&eacute;es client</th>
        <tr>
            <td class="col1"><strong>Nom</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtCustomerName" id="txtDispatchCustomerName" readonly="readonly"  /></td>
        </tr>
        <tr>
            <td class="col1"><strong>Adresse</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtCustomerAddress" id="txtDispatchCustomerAddress" readonly="readonly"  /></td>
        </tr>
        <tr>
            <td class="col1"><strong>Email</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtCustomerMail" id="txtDispatchCustomerMail" readonly="readonly"  /></td>
        </tr>
        <tr>
            <td class="col1"><strong>T&eacute;l</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtCustomerPhone" id="txtDispatchCustomerPhone" readonly="readonly"  /></td>
        </tr>
        <tr>
            <td class="col1"><strong>Nom de purchase</strong></td>
            <td class="col2"><input class="input-txt" type="text" name="txtPurchaseName" id="txtDispatchPurchaseName" readonly="readonly"  /></td>
        </tr>
    </table>
                    
    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
        <th colspan="2">Validit&eacute; de l'offre</th>
        <tr>
            <td class="col1"><strong>DEB</strong></td>
            <td class="col2"><input class="input-date" type="text" name="txtValidityStart" id="txtDispatchValidityStart" readonly="readonly"  /></td>
        </tr>
        <tr>
            <td class="col1"><strong>FIN</strong></td>
            <td class="col2"><input class="input-date" type="text" name="txtValidityEnd" id="txtDispatchValidityEnd" readonly="readonly"  /></td>
        </tr>
    </table>                    
</form>