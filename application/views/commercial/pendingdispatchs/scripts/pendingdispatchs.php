<script type="text/javascript">
    /**
     * Charger la liste des dispatchs en attente
     */
    function loadPendingDispatchs(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbldata').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_pending_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }

    /**
     * A function used to set the editor of the selected proforma
     */
    function setPendingDispatchForm(json, idSelectedRow)
    {   
        clearDispatchForm(); //CALL
        
        $('#idSelectedRow').val(idSelectedRow);
                
        $('#txtCotationId').val(json.cotation_id);
        $('#cmbDispatchSource').val(json.cotation_source);
        $('#txtDispatchReceptionDate').val(json.reception_date);
        $('#txtDispatchLimitSendDate').val(json.limit_send_date);
        $('#cmbDispatchCommercial').val(json.commercial_fullname);
        $('#txtDispatchNumberCustomer').val(json.cotation_number_customer);
        $('#txtDispatchCustomerName').val(json.customer_name);
        $('#txtDispatchCustomerAddress').val(json.customer_address);
        $('#txtDispatchCustomerMail').val(json.customer_mail);
        $('#txtDispatchCustomerPhone').val(json.customer_phone);
        $('#txtDispatchPurchaseName').val(json.purchaser_name);
        $('#txtDispatchValidityStart').val(json.validity_start);
        $('#txtDispatchValidityEnd').val(json.validity_end);
        $('#txtDispatchCotationNumber').val(json.cotation_number);
    
        for( var i = 0; i < json.proformas.length; i++ )
        {
            var proforma = json.proformas[i];
            addNewDispatchRequestRow(proforma.ref, proforma.designation, proforma.qty); //CALL                    
        }
        removeFirstDispatchRequestProformaRow(); //CALL
    }

    /*
     * Editing the proformas row of the proforma editor
     */
    function addNewDispatchRequestRow(reference, designation, qty) {;
        var lastRow = $('#tbl-dispatch-request-proforma tr:last');

        var rowCount = $('#tbl-dispatch-request-proforma tr').size();

        var newRow = lastRow.clone();
        newRow.children().each(function(k, v) {
            var content = $(v).children()[0];
                    
            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(reference); break;
                case 3: $(content).val(designation); break;
                case 4: $(content).val(qty); break;
            }
        });
        lastRow.after(newRow);
    }
    
    /**
     * Remove the first row of the request proforma
     */
    function removeFirstDispatchRequestProformaRow()
    {
        $('#tbl-dispatch-request-proforma tr').each(function(i, row){
            if(i == 1)
            {
                $(row).remove();
                return false;
            }
        });
        
        //Added since 06/02/2013 : Bug fixed, so the rows will start from 1 instead of 2
        var count = 0;
        $('#tbl-dispatch-request-proforma tr').each(function(i, row) {
                var val = $(row).children()[0];        
                var nb = $(val).children()[0];
                $(nb).html(count);
            
                count++;
        });
    }

    function clearDispatchRequestProformaRows()
    {
        $('#tbl-dispatch-request-proforma tr').each(function(i, row) {
            //Proforma reference
            var oReference = $(row).children()[2];
            var txtReference = $(oReference).children()[0];
            
            //Proforma designation
            var oDesignation = $(row).children()[3];
            var txtDesignation = $(oDesignation).children()[0];
                            
            //Proforma quantity
            var oQty = $(row).children()[4];
            var txtQty = $(oQty).children()[0];
                    
            if( i == 1 )
            {   
                $(txtReference).val('');
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /**
     * Functions for the proposition proforma
     */
    function addDispatchPropositionProformaRow() {
            var lastRow = $('#tbl-dispatch-proposition-proforma tr:last');
            var rowCount = $('#tbl-dispatch-proposition-proforma tr').size();

            var newRow = lastRow.clone();
            newRow.children().each(function(k, v) {
            var content = $(v).children()[0];
                
            switch(k) {
                case 0: $(content).html(rowCount);
                case 1: $(content).removeAttr('checked'); break;
                case 2: $(content).val(''); break;
                case 3: $(content).val(''); break;
                case 4: $(content).val('0'); break;
                case 5: {
                    $(content).val('0');
                    //initCurrentyMask(content);
                    break; 
                };
            }
        });
        lastRow.after(newRow);
    }
    
    function removeSelectedPropositionProformaRows() {
        $('#tbl-dispatch-proposition-proforma tr').each(function(i, row) {
            if(i > 1) {
                var val = $(row).children()[1];
                var chk = $(val).children()[0];
                if($(chk).attr('checked')) {
                    $(row).remove();
                }
            }
        });

        var count = 2;
        $('#tbl-dispatch-proposition-proforma tr').each(function(i, row) {
            if(i > 1) {
                var val = $(row).children()[0];
                var nb = $(val).children()[0];
                $(nb).html(count);                
                count++;
            }
        });
    }
    
    function clearDispatchPropositionProformaRows() {
        $('#tbl-dispatch-proposition-proforma tr').each(function(i, row) {
            //Proforma reference
            var oReference = $(row).children()[2];
            var txtReference = $(oReference).children()[0];
            
            //Proforma designation
            var oDesignation = $(row).children()[3];
            var txtDesignation = $(oDesignation).children()[0];
                        
            //Proforma quantity
            var oQty = $(row).children()[4];
            var txtQty = $(oQty).children()[0];
                        
            if( i == 1 )
            {                            
                $(txtReference).val('');
                $(txtDesignation).val('');   
                $(txtQty).val(0);
            }
            else if( i > 1 )
            {
                $(row).remove();
            }
        });
    }
    
    /***
     * Vérifie la validité des données saisies (email, num téléphone, champs vide...)
     */
     function validateDispatchForm() {
        var res = true;
        
        var errorColor = '#ffc1c1';        
        bCotationNumber = true;
        if($('#txtDispatchCotationNumber').val().trim() == '')
        {
            $('#txtDispatchCotationNumber').css('background-color', errorColor);
            bCotationNumber = false;
            res = false;
        }
        else
        {
            $('#txtDispatchCotationNumber').removeAttr('style')
        }
        
        $('#tbl-dispatch-proposition-proforma tr').each(function(i, row) {
            if( i > 0 ) {
                //Designation field
                var oDesignation = $(row).children()[3];
                var txtDesignation = $(oDesignation).children()[0];
                var txtDesignationValue = $(txtDesignation).val();
    
                if( txtDesignationValue.trim() == '' )
                {
                    $(txtDesignation).css('background-color', errorColor);
                    res = false;
                }
                else {
                    $(txtDesignation).removeAttr('style');
                }
                
                //Quantity field
                var oQty = $(row).children()[4];
                var txtQty = $(oQty).children()[0];
                var txtQtyValue = $(txtQty).val();

                if( ! isNumber(txtQtyValue) || txtQtyValue <= 0 )
                {
                    $(txtQty).css('background-color', errorColor);
                    res = false;
                }
                else
                {
                    $(txtQty).removeAttr('style');
                }
                
                //Price field
                var oPrice = $(row).children()[5];
                var txtPrice = $(oPrice).children()[0];
                var txtPriceValue = $(txtPrice).val();

                if( ! isNumber(txtPriceValue) || txtPriceValue <= 0 )
                {
                    $(txtPrice).css('background-color', errorColor);
                    res = false;
                }
                else
                {
                    $(txtPrice).removeAttr('style');
                }
            }
        });
                
        return res;
     }
    
    function clearDispatchForm()
    {
        $('#txtDispatchCotationNumber').val('');
            
        clearDispatchRequestProformaRows();
        clearDispatchPropositionProformaRows();
    }
    
    /***
     * Ajoute une nouvelle ligne dans la proposition
     * @since 06/02/2013
     */
    function addNewDispatchPropositionRow(valReference, valDesignation, valQty) {
        var rowCount = $('#tbl-dispatch-proposition-proforma tr').size();

        if( rowCount == 2 ) {
            var firstRow = $('#tbl-dispatch-proposition-proforma tr')[1];
            
            $(firstRow).each(function(k, v) {
                var cellNum         = $(v).children()[0];
                var cellChecked     = $(v).children()[1];
                var cellReference   = $(v).children()[2];
                var cellDesignation = $(v).children()[3];
                var cellQty         = $(v).children()[4];
                var cellPU          = $(v).children()[5];
                
                var chkSelected    = $(cellChecked).children()[0];
                var txtReference   = $(cellReference).children()[0];
                var txtDesignation = $(cellDesignation).children()[0];
                var txtQty         = $(cellQty).children()[0];
                var txtPU          = $(cellPU).children()[0];
                
                $(chkSelected).removeAttr('checked');
                $(txtReference).val(valReference);
                $(txtDesignation).val(valDesignation);
                $(txtQty).val(valQty);
                $(txtPU).val(0);
            });
            
            var lastRow = $('#tbl-dispatch-proposition-proforma tr:last');
            
            var newRow = lastRow.clone();
            newRow.children().each(function(k, v) {
                var content = $(v).children()[0];
                        
                switch(k) {
                    case 0: $(content).html(2);
                    case 1: $(content).removeAttr('checked'); break;
                    case 2: $(content).val(''); break;
                    case 3: $(content).val(''); break;
                    case 4: $(content).val(0); break;
                    case 5: $(content).val(0); break;
                }
            });
            lastRow.after(newRow);
        }
        else
        {
            var lastRow = $('#tbl-dispatch-proposition-proforma tr:last');

            lastRow.children().each(function(k, v) {
                var content = $(v).children()[0];
                
                switch(k) {
                    case 0: $(content).html(rowCount - 1);
                    case 1: $(content).removeAttr('checked'); break;
                    case 2: $(content).val(valReference); break;
                    case 3: $(content).val(valDesignation); break;
                    case 4: $(content).val(valQty); break;
                }
            });
            
            var newRow = lastRow.clone();
            lastRow.after(newRow);
        }
    }
    
    /***
     * Copie toutes les lignes des demandes vers les lignes de la proposition
     */
    function copyRequestsToPropositions() {
        clearRequestsRows();
        
        $('#tbl-dispatch-request-proforma tr').each(function(i, row) {
            if( i > 0 )
            {
                var oReference   = $(row).children()[2];
                var txtReference = $(oReference).children()[0];
                var valReference = $(txtReference).val();
                
                var oDesignation = $(row).children()[3];
                var txtDesignation = $(oDesignation).children()[0];
                var valDesignation = $(txtDesignation).val();
                
                var oQty = $(row).children()[4];
                var txtQty = $(oQty).children()[0];
                var valQty = $(txtQty).val();
                
                addNewDispatchPropositionRow(valReference, valDesignation, valQty);
            }
        });
        
        var lastRow = $('#tbl-dispatch-proposition-proforma tr:last');
        $(lastRow).remove();

        //initCurrentyMask();
    }
    
    function clearRequestsRows() {
        var rowCount = $('#tbl-dispatch-proposition-proforma tr').size();
        
        $('#tbl-dispatch-proposition-proforma tr').each(function(i, row) {
            if( i > 1 ) {
                $(row).remove();
            }
        });
    }

    $(function() {
        //initCurrentyMask();
    });
</script>