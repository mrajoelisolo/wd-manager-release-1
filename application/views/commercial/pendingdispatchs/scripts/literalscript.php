<script type="text/javascript">
    var _is_window_focused = true;
    var _bModalDialogDisplayed = false;

    var _timer = $.timer(function() {
        if( _bModalDialogDisplayed ) return false;
  
        $.ajax({
            url: '{base_url}services/commercialservices/check_new_dispatch',
            type: 'post',
            dataType: 'json',
            data: {},
            success: function(res) {
                if( res.status == 1 ) {
                    showModalDialog('dialog-new-dispatchs', 'dialog-overlay'); //Call
                    _bModalDialogDisplayed = true;
                    
                    //Load data : the new dispatchs
                    loadPendingDispatchs(true);

                    setTimerNotification(false);

                    if( ! _is_window_focused ) {
                        alert('Vous avez de nouveaux dispatchs');
                    }
                }
            }
        });
    });

    //Active ou désactive le timer de notification
    function setTimerNotification(enable) {
        if( enable ) {
            _timer.play();
        }
        else {
            _timer.pause();
        }
    }

    function closeModalDialog() {
         hideModalDialog();
        setTimerNotification(true);
        _bModalDialogDisplayed = false;
    }

    $(function() {
        //Setting the variable for chekcing if window are focused or not
		$(window).focus(function() {
			_is_window_focused = true;
		});

		$(window).blur(function() {
			_is_window_focused = false;
		});
        
        loadPendingDispatchs();
        
        $("#tbldata tbody tr").live('click', function() {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/commercialservices/fetch_pending_dispatch',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call
                    setPendingDispatchForm(json, tsel); //Call
                }
            });
        });
        
        /**
         * Button event handlers
         */
        $('#btnSend').click(function() {
            //Input validation
            var bFormValidated = validateDispatchForm();
            if( ! bFormValidated )
            {
                $( '#dialog-input-error' ).dialog( "destroy" );
                $( '#dialog-input-error' ).dialog({
                    resizable: false,
                    height: 140,
                    modal: true,
         			buttons: {
     			        "OK": function() {
     			            $( '#dialog-input-error' ).dialog( "close" );
                        }
     			    }
                });        
                
                return false;
            }

            //UI Dialog
            var idDialogSendCotation = "#dialog-confirm-send-cotation";                   
            $( idDialogSendCotation ).dialog( "destroy" );
            $( idDialogSendCotation ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Confirmer": function() {
				        $.ajax({
                            url: "{base_url}services/commercialservices/send_dispatch",
                            type: "post",
                            dataType: "json",
                            data: $("#form-pending-dispatch").serialize(),
                            success: function(res) {
                                $( idDialogSendCotation ).dialog( "close" );
                                hideModalDialog('dialog-box');
                                //loadData(true); //instead of use this, use jQuery
                                $('#' + res.id_selected_row).remove();
                                
                                var idCotation = res.id_selected_row;//$('#txtCotationId').val();
                                if( idCotation )
                                {
                                    document.location.href = '{base_url}commercial/cotationreporting/edit/' + idCotation;                                    
                                }
                                else
                                {
                                    alert('Erreur, veuillez fermer la formulaire et réouvrir cette cotation');
                                }
                            }
                        });
                    },
                    "Annuler": function() {
   					    $( idDialogSendCotation ).dialog( "close" );
                    }
             			}
            });
            //end of dialog
        });
        
        $('#btnClose').click(function() {
            hideModalDialog();
        });
                
        $('#btn-dispatch-add-row1,#btn-dispatch-add-row2').click(function() {
            addDispatchPropositionProformaRow(); return false; //Call
        });
        
        $('#btn-dispatch-remove-selected-rows1,#btn-dispatch-remove-selected-rows2').click(function() {
            removeSelectedPropositionProformaRows(); return false; //Call
        });
        
        $('#btn-copy-requests-to-propositions').click(function() {
            copyRequestsToPropositions(); return false; //Call
        });
        
        /***
         * UI button appearance
         */
        $( ".inner-btn" ).button();
        
        _timer.set({ time: _notification_delay_refresh, autostart: true });        
    });
</script>