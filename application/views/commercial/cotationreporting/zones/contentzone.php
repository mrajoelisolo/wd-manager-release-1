<div id="homepage">
    <div class="bodycontent">
        <form id="form-report-cotation" method="post" action="{base_url}commercial/cotationreporting/topdf">
            <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="2">General</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col1"><strong>QUOTE NUM</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCotationNumber" id="txtCotationNumber" value="{var_cotation_number}" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>Date</strong></td>
                        <td class="col2"><input class="input-date" type="text" name="txtCotationDate" id="txtCotationDate" value=""></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>Customer</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCustomer" id="txtCustomer" value="{var_customer}" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>Reqst for Quotation</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCotationSource" id="txtCotationSource" value="{var_cotation_source}" readonly="readonly"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>Rsble Quotation</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCommercialFullname" id="txtCommercialFullname" value="{var_commercial_fullname}" readonly="readonly"></td>
                    </tr>
                </tbody>
            </table>
            
            <!--
                DESIGNATION TABLE
            -->
            <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Designations</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <table class="inner-tbl-edit" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
                                        <th>Designation</th>
                                        <th>Qty</th>
                                        <th>P.U H.T in Ar</th>
                                        <th>Amount H.T en Ar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {var_designations}
                                    <tr>
                                        <td style="width: 60px;">
                                            <input type="text" value="{ref}" name="txtRef[]" value="" style="width: 50px;" readonly="readonly">
                                        </td>
                                        <td style="auto;">
                                            <textarea name="txtDesignation[]" readonly="readonly">{designation}</textarea>
                                        </td>
                                        <td style="width: 50px;">
                                            <input type="text" value="{qty}" name="txtQty[]" style="width: 40px; text-align: center;" readonly="readonly">
                                        </td>
                                        <td style="width: 180px;">
                                            <input type="text" value="{price_unit}" name="txtPU[]" style="width: 170px; text-align: right;" readonly="readonly">
                                        </td>
                                        <td style="width: 180px;">
                                            <input type="text" value="{amount}" style="width: 170px; text-align: right;" readonly="readonly">
                                        </td>
                                    </tr>
                                    {/var_designations}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="inner-tbl-edit" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>BASES EXCLUDE VAT</th>
                                        <th>VAT 20%</th>
                                        <th>MTTC</th>
                                        <th>NET PAID IN AR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 25%;">
                                            <input type="text" name="txtBaseExcludeVat" id="txtBaseExcludeVat" value="{var_bases_exclude_vat}" style="width: 100px;" readonly="readonly">
                                        </td>
                                        <td style="width: 25%;">
                                            <input type="text" name="txtVat" id="txtVat" value="{var_vat_20p}" style="width: 100px;" readonly="readonly">
                                        </td>
                                        <td style="width: 25%;">
                                            <input type="text" name="txtMttc" id="txtMttc" value="{var_mttc}" style="width: 100px;" readonly="readonly">
                                        </td>
                                        <td style="width: 25%;">
                                            <input type="text" name="txtNetPaid" id="txtNetPaid" value="{var_net_paid}" style="width: 100px;" readonly="readonly">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!--
                END OF DESIGNATION TABLE
            -->
            
            <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="2">CONDITIONS OF PAYMENT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col1"><strong>MODE</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtPaymentMode" id="txtPaymentMode" value="{var_payment_mode}"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>DELAI</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtPaymentDelay" id="txtPaymentDelay" value="{var_payment_delai}"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>VTY OF QTAT</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCotationValidity" id="txtCotationValidity" value="{var_cotation_validity}"></td>
                    </tr>
                    <tr>
                        <td class="col1"><strong>DELIVERY</strong></td>
                        <td class="col2"><input class="input-txt" type="text" name="txtCotationDelivery" id="txtCotationDelivery" value="{var_cotation_delivery}"></td>
                    </tr>
                </tbody>
            </table>
            
            <div class="div-btnbar">                    
                <td><input type="button" class="btnGradientBlue" value="G&eacute;n&eacute;rer" onclick="createPDFReport();"></td>
                <td><input type="button" class="btnGradientGreen" value="Retour à la liste des dispatchs" onclick="backToPreviousPage();"></td>
            </div>
        </form>
        <div id="msgConfirmation" class="toggleSuccess" style="display: none;">
            <p></p>
        </div>
    </div>
</div>