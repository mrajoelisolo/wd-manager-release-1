<script type="text/javascript">
    /***
    * Affiche message de confirmation succes/erreur
    */
    function showConfirmation(msg, error) {
        if(error) {
            $('#msgConfirmation').removeClass('toggleSuccess');
            $('#msgConfirmation').addClass('toggleError');

            msg = 'Le formulaire est incomplet';
        }
        else
        {
            $('#msgConfirmation').removeClass('toggleError');
            $('#msgConfirmation').addClass('toggleSuccess');
        }
        
        $('#msgConfirmation p').html(msg);
        $('#msgConfirmation').show('bounce', {}, callback);
    }
                
    function callback() {
        setTimeout(function() {
            $( "#msgConfirmation:visible" ).removeAttr( "style" ).fadeOut();
        }, 10000);
    }
    
    function createPDFReport() {
        $('#form-report-cotation').submit();    
    }
    
    function backToPreviousPage() {
        document.location.href = '{base_url}commercial/urgentdispatchs';
    }
    
    $(function() {
        $.post("{base_url}services/dateservices/today", function(data) {
            $( "#txtCotationDate" ).val(data);
        });
        
        $( "#txtCotationDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {}
        });
    });
</script>