                <form id="form-pending-dispatch" action="#" method="post">
                    <input type="hidden" name="idSelectedRow" id="idSelectedRowDispatch" value="" />
                    <input type="hidden" id="txtCotationId" name="txtCotationId" value="" />
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <th colspan="2">G&eacute;n&eacute;ral</th>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Source</strong></td>
                            <td class="col2">
                                <input class="input-txt" type="text" name="cmbCotationSource" id="cmbCotationSourceDispatch" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Date r&eacute;ception</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtReceptionDate" id="txtReceptionDateDispatch" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Date limite d'envoi</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtLimitSendDate" id="txtLimitSendDateDispatch" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Commercial</strong></td>
                            <td class="col2">
                                <input class="input-txt" type="text" name="cmbCommercial" id="cmbCommercialDispatch" readonly="readonly" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Num&eacute;ro cotation client</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCotationNumberCustomer" id="txtCotationNumberCustomerDispatch" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Num&eacute;ro PRF</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCotationNumber" id="txtCotationNumberDispatch" /></td>
                        </tr>
                    </table>
                    
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <th colspan="2">Designation demande</th>
                        </tr>
                        <tr>
                            <td class="col2">
                                <table id="tbl-proforma-dispatch" class="inner-tbl-edit" cellspacing="0">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Designation</th>
                                        <th>Quantit&eacute;</th>
                                    </tr>
                                    <tr>
                                        <td class="col1"><strong>1</strong></td>
                                        <td class="col2"><input type="checkbox" /></td>
                                        <td class="col3"><textarea name="arrDesignations[]" readonly="readonly" ></textarea></td>
                                        <td class="col4"><input class="input-txt" type="text" name="arrQuantitys[]" value="0" readonly="readonly"  /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <th colspan="2">Designation propos&eacute;</th>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" class="inner-btn" id="btn-add-row-dispatch1" value="Ajouter" />
                                <input type="button" class="inner-btn" id="btn-remove-selected-rows-dispatch1" value="Supprimer la  selection" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col2">
                                <table id="tbl-proforma-dispatch2" class="inner-tbl-edit" cellspacing="0">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Designation</th>
                                        <th>Quantit&eacute;</th>
                                        <th>P.U HT</th>
                                    </tr>
                                    <tr>
                                        <td class="col1"><strong>1</strong></td>
                                        <td class="col2"><input type="checkbox" /></td>
                                        <td class="col3"><textarea name="arrDesignations2[]"></textarea></td>
                                        <td class="col4"><input class="input-txt" type="text" name="arrQuantitys2[]" value="0" /></td>
                                        <td class="col5"><input class="input-txt" type="text" name="arrPricesUnit2[]" value="0" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" class="inner-btn" id="btn-add-row-dispatch2" value="Ajouter" />
                                <input type="button" class="inner-btn" id="btn-remove-selected-rows-dispatch2" value="Supprimer la selection" />
                            </td>
                        </tr>
                    </table>
                    
                    <input type="hidden" name="txtCustomerId" id="txtCustomerIdDispatch" value="-1" />
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">Coordonn&eacute;es client</th>
                        <tr>
                            <td class="col1"><strong>Nom</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerName" id="txtCustomerNameDispatch" readonly="readonly"  /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Adresse</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerAddress" id="txtCustomerAddressDispatch" readonly="readonly"  /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Email</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerMail" id="txtCustomerMailDispatch" readonly="readonly"  /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>T&eacute;l</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerPhone" id="txtCustomerPhoneDispatch" readonly="readonly"  /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Nom de purchase</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtPurchaseName" id="txtPurchaseName" readonly="readonly"  /></td>
                        </tr>
                    </table>
                    
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">Validit&eacute; de l'offre</th>
                        <tr>
                            <td class="col1"><strong>DEB</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtValidityStart" id="txtValidityStartDispatch" readonly="readonly"  /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>FIN</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtValidityEnd" id="txtValidityEndDispatch" readonly="readonly"  /></td>
                        </tr>
                    </table>                    
                </form>