        <input id="txtAccountState" type="hidden" value="{var_account_state}" />
        <input id="txtRelanceState" type="hidden" value="{var_relance_state}" />
        
        <!-- UI dialogs -->
        {logout_dialog_content}
        
        <div id="dialog-confirm-accept-cotation" title="Confirmation" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Accorder la cotation ?</p>
        </div>
        
        <div id="dialog-confirm-reject-cotation" title="Confirmation" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Ne pas accorder la cotation ?</p>
        </div>
        <!--End of UI dialogs -->

        <div id="topnav">
            <ul>
                <li id="first" class="active"><a href="#">Accueil</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}home_img.png" alt="header_img" width="64px" height="64px">
                Bonjour {var_firstname}, bienvenue dans Wide manager en tant que commercial
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" alt="logout_img" /></a>
            </div>
        </div>
        <div class="clear"></div>