    <div id="homepage">
        <div class="rightcol">
            <h2 class="subhead">Tableau de bord</h2>
            <ul>
                <li class="alert_icon">Vous avez <strong>{var_count_urgent_proformas}</strong> cotations urgentes. <a href="{base_url}commercial/urgentdispatchs">Voir</a></li>
                <li class="alert_icon">Vous avez <strong>{var_count_relance_cotations}</strong> cotations a relancer. <a href="{base_url}commercial/relancesendedcotations">Voir</a></li>
                <li class="bluenotification_icon">Vous avez <strong>{var_count_pending_dispatchs}</strong> cotations dispatchs non encore trait&eacute;s.</li>
                <!-- TO DO
                <li class="bluenotification_icon">Vous avez <strong>{var_count_sended_cotations}</strong> cotations en attente de validation.</li>
                -->
            </ul>
        </div>
            
        <div class="leftcol">
            <h2 class="subhead">Actions</h2>
            <ul>
                <li class="warning_icon">Traiter les dispatchs en attente. <a href="{base_url}commercial/pendingdispatchs">Voir la liste</a></li>
                <li class="tasks_icon" id="action_validate_cotation">Voir les cotations &agrave; valider. <a href="{base_url}commercial/sendedcotations">Voir la liste</a></li>
                <li class="book_icon">Liste des cotations. <a href="{base_url}commercial/list_cotations">Voir la liste</a></li>
                <li class="idea_icon"><a href="{base_url}commercial/workflow">Diagramme workflow</a></li>
            </ul>
        </div>
        
        <!--
        <br class="clear" />
        <div class="onecol">
        </div>
        <br class="clear" />
        <div class="onecol">
        </div>
        -->
    </div>
    <br class="clear" />

    <!-- Modal dialog : Urgent dispatchs -->
    <div id="dialog-overlay"></div>
   	<div class="notification-dialog" id="dialog-new-dispatchs">    
  		<div class="dialog-content">
 			<div id="dialog-message">
                <!-- Load data from ajax -->
                <div style="float:right">
                    <img src="{path_img}new_message_img.png" height="48px" alt="urgent_img"/>
                </div>
                <h2 class="subhead">Vous avez des dispatchs urgents a traiter</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-new-dispatchs-data" width="100%">
                    <thead>
                		<tr>
                            <th>Cotation Id</th>
                            <th>N&ordm; cotation client</th>
                            <th>Client</th>
                            <th>Designation demande</th>
                            <th>Date limite d'envoi</th>
                		</tr>
                	</thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="dialog-footer">
            <p>
                <input type="submit" id="btnShowUrgentDispatchsForm" onclick="showUrgentDispatchsForm();" value="Traiter" class="btnGradientGreen" />
                &nbsp;
                <input type="submit" id="btnCloseUrgentDispatchsForm" onclick="closeModalDialog();" value="Fermer" class="btnGradientRed" />
            </p>
        </div>
    </div>

    <!-- Modal dialog : Urgent dispatchs -->
    <div id="dialog-overlay"></div>
   	<div class="notification-dialog" id="dialog-urgent-dispatchs">    
  		<div class="dialog-content">
 			<div id="dialog-message">
                <!-- Load data from ajax -->
                <div style="float:right">
                    <img src="{path_img}urgent_img.png" alt="urgent_img"/>
                </div>
                <h2 class="subhead">Vous avez des dispatchs urgents a traiter</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-urgent-dispatchs-data" width="100%">
                    <thead>
                		<tr>
                            <th>Cotation Id</th>
                            <th>N&ordm; cotation client</th>
                            <th>Client</th>
                            <th>Designation demande</th>
                            <th>Date limite d'envoi</th>
                		</tr>
                	</thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="dialog-footer">
            <p>
                <input type="submit" id="btnShowUrgentDispatchsForm" onclick="showUrgentDispatchsForm();" value="Traiter" class="btnGradientGreen" />
                &nbsp;
                <input type="submit" id="btnCloseUrgentDispatchsForm" onclick="closeModalDialog();" value="Fermer" class="btnGradientRed" />
            </p>
        </div>
    </div>
    
    <!-- Modal dialog : Sended cotations, relance -->
    <div class="notification-dialog" id="dialog-relance-cotations">    
  		<div class="dialog-content">
 			<div id="dialog-message">
                <!-- Load data from ajax -->
                <div style="float:right">
                    <img src="{path_img}todo_img.png" alt="todo_img"/>
                </div>
                <h2 class="subhead">Vous avez des cotations a relancer</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-relance-cotations-data" width="100%">
                    <thead>
                		<tr>
                            <th>Cotation Id</th>
                            <th>N&ordm; PRF</th>
                            <th>Client</th>
                            <th>Designation demande</th>
                            <th>Designation proposition</th>
                            <th>DEB</th>
                            <th>FIN</th>
                		</tr>
                	</thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="dialog-footer">
            <p>
                <input type="submit" id="btnShowRelanceCotationsForm" onclick="showRelanceCotationsForm();" value="Traiter" class="btnGradientGreen" />
                &nbsp;
                <input type="submit" id="btnCloseSendedCotationsForm" onclick="closeModalDialog();" value="Fermer" class="btnGradientRed" />
            </p>
        </div>
    </div>
    
    <audio src="{path_sound}radio_sound.wav" id="sound_newmessage"></audio>