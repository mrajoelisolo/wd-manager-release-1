<script type="text/javascript">
    var _is_window_focused = true;
    var _bModalDialogDisplayed = false;

    var _timerDispatch = $.timer(function() {
        if( _bModalDialogDisplayed ) return false;
  
        $.ajax({
            url: '{base_url}services/commercialservices/check_new_dispatch',
            type: 'post',
            dataType: 'json',
            data: {},
            success: function(res) {
                if( res.status == 1 ) {
                    showModalDialog('dialog-new-dispatchs', 'dialog-overlay'); //Call
                    _bModalDialogDisplayed = true;
                    
                    //Load data : the new dispatchs
                    loadNewDispatchs(true);

                    setTimerNotification(false);
                    playSound('sound_newmessage');

                    if( ! _is_window_focused ) {
                        alert('Vous avez de nouveaux messages');
                    }
                }
            }
        });
    });

    var _timerRelance = $.timer(function() {
        if( _bModalDialogDisplayed ) return false;

        $.ajax({
            url: '{base_url}services/commercialservices/check_relance',
            type: 'post',
            dataType: 'json',
            data: {},
            success: function(res) {
                if( res.status == 1 ) {
                    showModalDialog('dialog-relance-cotations', 'dialog-overlay'); //Call
                    _bModalDialogDisplayed = true;
                    
                    //Load data : the new dispatchs
                    loadRelanceCotations(true);

                    setTimerNotification(false);
                    playSound('sound_newmessage');

                    if( ! _is_window_focused ) {
                        alert('Vous avez de nouveaux messages');
                    }
                }
            }
        });
    });

    /**
     * Vérifie si des dispatchs urgents existent et vérouille le compte si c'est le cas
     */
    function checkAndLockIfUrgentDispatchExists() {   
        var accountState = $('#txtAccountState').val();

        if(accountState == 'LOCKED')
        {
            $('#action_validate_cotation').attr('class', 'lock_icon').html('Vous ne pouvez pas traiter les cotations envoy&eacute, vous devez traiter les dispatchs urgentes d\'abord');

            showModalDialog('dialog-urgent-dispatchs', 'dialog-overlay'); //Call
            
            setTimerNotification(false);                                    
            _bModalDialogDisplayed = true;
            
            //Load data : the urgent dispatchs
            loadUrgentDispatchs();
            
            return true;
        }
        
        return false;
    }

    /**
     * Vérifie s'il éxiste des cotations à relancer
     */
    function checkIfRelanceExists() {
        var relanceState = $('#txtRelanceState').val();
            
        if( relanceState == 'RELANCE' )
        {
            showModalDialog('dialog-relance-cotations', 'dialog-overlay'); //Call
            _bModalDialogDisplayed = true;

            loadRelanceCotations();

            return true;
        }
        
        return false;
    }

    /**
     * Charger la liste des dispatchs urgents
     */
    function loadUrgentDispatchs(destroy) {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-urgent-dispatchs-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_urgent_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    /**
     * Charger les nouveaux dispatche en attentes
     */
    function loadNewDispatchs(destroy) {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-new-dispatchs-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_new_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });   
    }
    
    /**
     * Charger la liste des cotations a relancer
     */
    function loadRelanceCotations(destroy) {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-relance-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_relance_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }

    //Ferme la fenêtre modal
    function closeModalDialog() {
        hideModalDialog();
        setTimerNotification(true);
        _bModalDialogDisplayed = false;
        stopSound('sound_newmessage');
    }

    //Affiche la liste des dispatchs urgents
    function showUrgentDispatchsForm()
    {
        document.location.href = '{base_url}commercial/urgentdispatchs';
    }

    //Affiche la liste des relances de cotations
    function showRelanceCotationsForm()
    {
        document.location = '{base_url}commercial/relancesendedcotations';
    }

    //Active ou désactive le timer de notification
    function setTimerNotification(enable) {
        if( enable ) {
            _timerDispatch.play();
            _timerRelance.play();
        }
        else {
            _timerDispatch.pause();
            _timerRelance.pause();
        }
    }

    function playSound(element_id) {
        //var audio = $('#' + element_id)[0];
        //audio.play();
    }

    function stopSound(element_id) {
        //var audio = $('#' + element_id)[0];
        //audio.pause();
        //audio.load();
    }

    $(function() {
        var bUrgentDispatchsExists = checkAndLockIfUrgentDispatchExists()
        
        //If urgent dispatchs don't exists, we can continue to check if relance exists
        if( ! bUrgentDispatchsExists ) {
            checkIfRelanceExists(); //Call
        }
        else {
        //If urgent dialog exists, a dialog appears, we must toggle the _bNewDispatchDisplayed variable
            setTimerNotification(false);
            _bModalDialogDisplayed = false;
        }
        
        //Setting the variable for chekcing if window are focused or not
		$(window).focus(function() {
			_is_window_focused = true;
		});

		$(window).blur(function() {
			_is_window_focused = false;
		});

        _timerDispatch.set({ time: _notification_delay_refresh, autostart: true });
        _timerRelance.set({ time: _relance_delay_refresh, autostart: true });
    });
</script>