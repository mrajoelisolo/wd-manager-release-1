<script type="text/javascript">
    /**
     * Charger la liste des cotations a relancer
     */
    function loadRelanceCotations(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-relance-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/commercialservices/source_relance_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    $(function(){
       loadRelanceCotations();
       
       $("#tbl-relance-cotations-data tr").live('click', function()
        {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/commercialservices/fetch_sended_cotation',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call
                    setSendedCotationForm(json, tsel); //Call
                }
            });
        });
        
        $('#btnAccept').click(function()
        {
            $( '#dialog-confirm-accept-cotation' ).dialog( "destroy" );
            $( '#dialog-confirm-accept-cotation' ).dialog({
                resizable: false,
                height:140,
                modal: true,
     			buttons: {
 			        "Confirmer": function() {
 			            $.ajax({
                            url: "{base_url}services/commercialservices/accept_sended_cotation",
                            type: "post",
                            dataType: "json",
                            data: $("#form-sended-cotation").serialize(),
                            success: function(res) {
                                $( '#dialog-confirm-accept-cotation' ).dialog( "close" );
                                hideModalDialog('dialog-box');        
                                $('#' + res.id_selected_row).remove();
                            }
                        });
                    },
                    "Annuler": function() {
                        $( '#dialog-confirm-accept-cotation' ).dialog( "close" );
                    }
                }
            });
        });

        $('#btnReject').click(function()
        {
            $( '#dialog-confirm-reject-cotation' ).dialog( "destroy" );
            $( '#dialog-confirm-reject-cotation' ).dialog({
                resizable: false,
                height:140,
                modal: true,
     			buttons: {
     			    "Confirmer": function() {
                        $.ajax({
                            url: "{base_url}services/commercialservices/reject_sended_cotation",
                            type: "post",
                            dataType: "json",
                            data: $("#form-sended-cotation").serialize(),
                            success: function(res) {
                            $( '#dialog-confirm-reject-cotation' ).dialog( "close" );
                                hideModalDialog('dialog-box');

                                $('#' + res.id_selected_row).remove();
                            }
                        });
                    },
                    "Annuler": function() {
                        $( '#dialog-confirm-reject-cotation' ).dialog( "close" );
                    }
                }
            });
        });
                
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
    });
</script>