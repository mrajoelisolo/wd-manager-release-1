<div id="homepage">
    <div class="bodycontent">
        <form id="form-search-cotation" action="#" method="post">
            <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th colspan="4">Filtre</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>Num&eacute;ro de cotation</strong></td>
                        <td><input type="checkbox" id="chkCotationNumber"></td>
                        <td><input class="input-txt" type="text" name="txtCotationNumber" id="txtCotationNumber"></td>
                    </tr>
                    <tr>
                        <td><strong>Date</strong></td>
                        <td><input type="checkbox" id="chkCotationDate"></td>
                        <td><input class="input-date" type="text" name="txtCotationDate" id="txtCotationDate"></td>
                    </tr>
                    <tr>
                        <td><strong>Commercial</strong></td>
                        <td><input type="checkbox" id="chkCommercial"></td>
                        <td>
                            <select class="select-cmb" name="cmbCommercial" id="cmbCommercial">
                                {var_commercials}
                                    <option value="{account_id}">{first_name} {last_name}</option>
                                {/var_commercials}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input type="submit" value="Chercher" id="btnSearchCotation">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <strong>Note :</strong> Cocher les cases pour activer les filtrages
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    
        <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Cotation number</th>
                    <th>Commercial</th>
                    <th>Date de reception</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {var_cotations}
                <tr>
                    <td>{cotation_number}</td>
                    <td>{commercial_firstname}</td>
                    <td>{reception_date}</td>
                    <td><a href="{base_url}dispatcher/reportingsettings/edit/{cotation_id}" target="_blank">Afficher</a></td>
                </tr>
                {/var_cotations}
            </tbody>
        </table>
        
        <div class="wrap-pagination">
            {var_pagination}
        </div>
    </div>
</div>