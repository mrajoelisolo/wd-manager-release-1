<script type="text/javascript">
    $(function() {
        $.post("{base_url}services/dateservices/today", function(data) {
            $( "#txtCotationDate" ).val(data);
        });
        
        $( "#txtCotationDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {}
        });
        
        $('#form-search-cotation').submit(function(e) {
            e.preventDefault();
            
            var f_cotation_num = $('#txtCotationNumber').val();
            var f_cotation_date = $('#txtCotationDate').val();
            var f_commercial_id = $('#cmbCommercial').val();
            
            var criteria = '';
            
            var chkCotationNum = $('#chkCotationNumber').attr('checked');
            if( f_cotation_num != '' && chkCotationNum == 'checked') {
                criteria += 'cnum/' +  f_cotation_num;
            }

            var chkCotationDate = $('#chkCotationDate').attr('checked');
            if( f_cotation_date != '' && chkCotationDate == 'checked' ) {
                if( criteria != '' )
                    criteria += '/';

                criteria += 'cdate/' +  f_cotation_date.replace('/', '-').replace('/', '-');
            }

            var chkCommercial = $('#chkCommercial').attr('checked');
            if( f_commercial_id != '' && chkCommercial == 'checked' ) {
                if( criteria != '' )
                    criteria += '/';

                criteria += 'com/' +  f_commercial_id;
            }
            
            document.location.href = '{base_url}dispatcher/reporting/show/' + criteria;
        });
        
        $( "#btnSearchCotation" ).button();
    });
</script>