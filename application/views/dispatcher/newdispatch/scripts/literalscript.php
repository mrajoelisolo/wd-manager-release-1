<script type="text/javascript">
    $(function() {
        /***
        * Récupérer certaines données du serveur, les dates
        */                
        $.post("{base_url}services/dateservices/today", function(data) {
            $( "#txtReceptionDate" ).val(data);
            $( "#txtValidityStart" ).val(data);
        
            $( "#txtLimitSendDate" ).datepicker( "option", "minDate", data );
            $( "#txtLimitSendDate" ).val(data);
        });
        
        $.post("{base_url}services/dateservices/today", { daysafter:15 } , function(data) {
            $( "#txtValidityEnd" ).val(data);
            $( "#txtValidityEnd" ).datepicker( "option", "minDate", data );
        });
        
        /**
        * JQuery UI Datepicker
        */
        //Reception date and limit
        $( "#txtReceptionDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {
                $( "#txtLimitSendDate" ).datepicker( "option", "minDate", selectedDate );
            }
        });
                
        $( "#txtLimitSendDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {
                $( "#txtReceptionDate" ).datepicker( "option", "maxDate", selectedDate );
            }
        });

        //Validation date
        $( "#txtValidityStart" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
 			onSelect: function( selectedDate ) {
                $( "#txtValidityEnd" ).datepicker( "option", "minDate", selectedDate );
            }
  		});
                
  		$( "#txtValidityEnd" ).datepicker({
 			defaultDate: "+1w",
 			changeMonth: true,
 			numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {
                $( "#txtValidityStart" ).datepicker( "option", "maxDate", selectedDate );
            }
  		});
        
        /**
        * Effacer le formulaire principal
        */
        function clearProformaForm()
        {
            $('#txtCotationNumberCustomer').val('');
            $('#txtCustomerName').val('');
            $('#txtCustomerAddress').val('');
            $('#txtCustomerMail').val('');
            $('#txtCustomerPhone').val('');
            $('#txtPurchaseName').val('');
                    
            clearRequestProformaRows(); //CALL
        }
        
        /**
        * Effacer les lignes des designations demandes et propositions
        */
        function clearRequestRows()
        {
            $('#tbl-proforma tr').each(function(i, row) {
                //Proforma ref
                var oReference = $(row).children()[2];
                var txtReference = $(oReference).children()[0];
                
                //Proforma designation
                var oDesignation = $(row).children()[3];
                var txtDesignation = $(oDesignation).children()[0];

                //Proforma quantity
                var oQty = $(row).children()[4];
                var txtQty = $(oQty).children()[0];
                        
                if( i == 1 )
                {   
                    $(txtReference).val('');
                    $(txtDesignation).val('');   
                    $(txtQty).val(0);
                }
                else if( i > 1 )
                {
                    $(row).remove();
                }
            });
        }
        
        function validateProformaForm()
        {
            var res = true;
            var errorColor = '#ffc1c1';

            //Customer name
            if($('#txtCustomerName').val().trim() == '')
            {
                $('#txtCustomerName').css('background-color', errorColor);
                res = false;
            }
            else { $('#txtCustomerName').removeAttr('style'); }
            
            //Customer address
            if($('#txtCustomerAddress').val().trim() == '')
            {
                $('#txtCustomerAddress').css('background-color', errorColor);
                res = false;
            }
            else { $('#txtCustomerAddress').removeAttr('style'); }
            
            //Email validation
            var bCustomerMail = testPattern($('#txtCustomerMail').val(), "^[a-z0-9.-_]+@[a-z]{2,}[.][a-z]{2,4}$", "i");  //CALL
            if(!bCustomerMail)
            {
                $('#txtCustomerMail').css('background-color', errorColor);
                res = false;
            }
            else { $('#txtCustomerMail').removeAttr('style'); }
            
            //Customer phone
            if($('#txtCustomerPhone').val().trim() == '')
            {
                $('#txtCustomerPhone').css('background-color', errorColor);
                res = false;
            }
            else { $('#txtCustomerPhone').removeAttr('style'); }
            
            //Purchaser name
            if($('#txtPurchaseName').val().trim() == '')
            {
                $('#txtPurchaseName').css('background-color', errorColor);
                res = false;
            }
            else { $('#txtPurchaseName').removeAttr('style'); }
            
            //Checking request proformas rows
            $('#tbl-proforma tr').each(function(i, row) {
                if( i > 0 )
                {
                    //Proforma designation
                    var oDesignation = $(row).children()[3];
                    var txtDesignation = $(oDesignation).children()[0];
                    var txtDesignationValue = $(txtDesignation).val();
    
                    if( txtDesignationValue.trim() == '' )
                    {
                        $(txtDesignation).css('background-color', errorColor);
                        res = false;
                    }
                    else { $(txtDesignation).removeAttr('style'); }
                                
                    //Proforma quantity
                    var oQty = $(row).children()[4];
                    var txtQty = $(oQty).children()[0];
                    var txtQtyValue = $(txtQty).val();

                    if( ! isNumber(txtQtyValue) || txtQtyValue <= 0 )
                    {
                        $(txtQty).css('background-color', errorColor);
                        res = false;
                    }
                    else
                    {
                        $(txtQty).removeAttr('style');
                    }
                }
            });
            
            return res;
        }
        
        /***
        * Efface toutes les champs du client si l'on édite l'une des champs
        */
        $('txtCustomerMail, #txtCustomerPhone, #txtPurchaseName').change(function() {
            if( $('#txtCustomerId').val() == -1 ) return false;
         
            var inputs = Array('txtCustomerMail', 'txtCustomerPhone', 'txtPurchaseName');
            var currentInput = $(this).attr('id');
                    
            for(var i = 0; i < inputs.length; i++)
            {
                var input = inputs[i];
                if( input == currentInput ) continue;
                        
                $('#' + input).val('');
            }
                     
            $('#txtCustomerId').val(-1);
         });
         
         /**
         * Gestion de l'autocompletion des champs du client
         */
         var json_options = {
            script:'codeigniter_uri',
            varname:'{base_url}services/dispatcherservices/source_customers/',
            json:true,
            shownoresults:true,
            maxresults:16,
            callback: function (obj) { 
                $('#txtCustomerId').val(obj.id);
                $('#txtCustomerName').val(obj.customer_name);
                $('#txtCustomerAddress').val(obj.customer_address);
                $('#txtCustomerMail').val(obj.customer_mail);
                $('#txtCustomerPhone').val(obj.customer_phone);
                $('#txtPurchaseName').val(obj.purchaser_name);
            }
         };
         $('#txtCustomerName').autoComplete(json_options);
         
         /**
         * Afficher une confirmation de suppression des lignes du designation de demandes
         */
         function showConfirmRemoveSelectedRequestProformas()
         {
            var idDialogAddCotation = "#dialog-confirm-remove-elements";                   
            $( idDialogAddCotation ).dialog( "destroy" );
            $( idDialogAddCotation ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Oui": function() {
                        removeSelectedRequestProformasRows();  //CALL
     				},
                    "Non": function() {
                        $( this ).dialog( "close" );
                    }
 			    }
            });                        
        }
        
        /**
        * Ajouter une ligne dans la designation de demandes
        */
        function addRequestProformaRow()
        {
            var lastRow = $('#tbl-proforma tr:last');       
            var rowCount = $('#tbl-proforma tr').size();
            var newRow = lastRow.clone();
            
            newRow.children().each(function(k, v) {
                var content = $(v).children()[0];
        
                switch(k) {
                    case 0: $(content).html(rowCount);
                    case 1: $(content).removeAttr('checked'); break;
                    case 2: $(content).val(''); break;
                    case 3: $(content).val(''); break;
                    case 4: $(content).val('1'); break;
                }
            });
            lastRow.after(newRow);
        }
        
        /**
        * Compter le nombre de lignes sélectionnées dans la designation de demandes
        */        
        function countSelectedRequestProformaRows() {
            var res = 0;
                            
            $('#tbl-proforma tr').each(function(i, row) {
                if(i > 1) {
                    var val = $(row).children()[1];
                
                    var chk = $(val).children()[0];
                    if($(chk).attr('checked')) {
                        res++;
                    }
                }
            });
                            
            return res;
        }
        
        /**
        * Supprimer les lignes sélectionnées dans la designation de demandes
        */
        function removeSelectedRequestProformaRows() {
            $('#tbl-proforma tr').each(function(i, row) {
                    if(i > 1) {
                        var val = $(row).children()[1];
                            
                    var chk = $(val).children()[0];
                    if($(chk).attr('checked')) {
                        $(row).remove();
                    }
                }
            });
                    
            var count = 2;
            $('#tbl-proforma tr').each(function(i, row) {
                if(i > 1) {
                    var val = $(row).children()[0];        
                    var nb = $(val).children()[0];
                    $(nb).html(count);
                            
                    count++;
                }
            });
        }
        
        /**
        * Supprimer toutes les lignes de la designation de demandes 
        */
        function clearRequestProformaRows()
        {
            $('#tbl-proforma tr').each(function(i, row) {
                //Proforma reference
                var oReference = $(row).children()[2];
                var txtReference = $(oReference).children()[0];
                
                //Proforma designation
                var oDesignation = $(row).children()[3];
                var txtDesignation = $(oDesignation).children()[0];
    
                //Proforma quantity
                var oQty = $(row).children()[4];
                var txtQty = $(oQty).children()[0];
    
                if( i == 1 )
                {                            
                    $(txtDesignation).val('');   
                    $(txtQty).val(0);
                }
                else if( i > 1 )
                {
                    $(row).remove();
                }
            });
        }
        
        var idDialogAddCotation = "#dialog-confirm-add-cotation";                   
        $( idDialogAddCotation ).dialog( "destroy" );
        $( "#form-cotation" ).submit(function(e) {
            e.preventDefault();
                    
            var bFormValidated = validateProformaForm(); //CALL
            if( ! bFormValidated )
            {
                showConfirmation('error'); //CALL
                return false;
            }
                    
            $( idDialogAddCotation ).dialog({
                resizable: false,
                height:140,
                modal: true,
     			buttons: {
                    "Confirmer": function() {
                        $.ajax({
                            url: "{base_url}services/dispatcherservices/save_dispatch",
                            type: "post",
                            dataType: "json",
                            data: $("#form-cotation").serialize(),
                            success: function(res) {
                                $( idDialogAddCotation ).dialog( "close" );
                                    //Show notification
                                clearProformaForm(); //CALL
                                showConfirmation(); //CALL
                            }
                        });
                    },
                    "Annuler": function() {
                        $( idDialogAddCotation ).dialog( "close" );
                    }
                }
            });
        });
        
        /***
        * Affiche message de confirmation succes/erreur
        */
        function showConfirmation(error) {
            var msg = 'La cotation saisie a &eacute;t&eacute; enregistr&eacute;e';
        
            if(error)
            {
                $('#msgConfirmation').removeClass('toggleSuccess');
                $('#msgConfirmation').addClass('toggleError');
        
                msg = 'Le formulaire est incomplet';
            }
            else
            {
                $('#msgConfirmation').removeClass('toggleError');
                $('#msgConfirmation').addClass('toggleSuccess');
            }

            $('#msgConfirmation p').html(msg);
            $('#msgConfirmation').show('bounce', {}, callback);
        }
                
        function callback() {
            setTimeout(function() {
                $( "#msgConfirmation:visible" ).removeAttr( "style" ).fadeOut();
            }, 5000);
        }
        
        //Event handlers
        $('#btn-add-row1,#btn-add-row2').click(function() {
            addRequestProformaRow(); return false; //CALL
        });
        
        $('#btn-remove-selected-rows1,#btn-remove-selected-rows2').click(function() {
            removeSelectedRequestProformaRows(); return false;
        });
        
        /***
        * UI button appearance
        */
        $( "input:button" ).button();
    });
</script>