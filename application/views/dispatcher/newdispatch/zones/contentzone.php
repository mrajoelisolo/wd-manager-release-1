        <div id="homepage">
            <div class="bodycontent">
                <h2 class="subhead">Nouvelle cotation</h2>
                <form id="form-cotation" action="#" method="post">
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">G&eacute;n&eacute;ral</th>
                        <tr>
                            <td class="col1"><strong>Source</strong></td>
                            <td class="col2">
                                <select class="select-cmb" name="cmbCotationSource">
                                    <option value="PHONE">T&eacute;l&eacute;phone</option>
                                    <option value="PHYSICAL">Physique</option>
                                    <option value="MAIL">Mail</option>
                                    <option value="OTHER">Other</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Date r&eacute;ception</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtReceptionDate" id="txtReceptionDate" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Date limite d'envoi</strong></td>
                            <td class="col2"><input class="input-date" type="text" name="txtLimitSendDate" id="txtLimitSendDate" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Commercial</strong></td>
                            <td class="col2">
                                <select class="select-cmb" name="cmbCommercial">
                                    {var_commercials}
                                        <option value="{id}">{fullname}</option>
                                    {/var_commercials}
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Num&eacute;ro cotation client</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCotationNumberCustomer" id="txtCotationNumberCustomer" /></td>
                        </tr>
                    </table>
                    
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">Designation demande</th>
                        <tr>
                            <td>
                                <input type="button" id="btn-add-row1" value="Ajouter" />
                                <input type="button" id="btn-remove-selected-rows1" value="Supprimer la  selection" />
                            </td>
                        </tr>
                        <tr>
                            <td class="col2">
                                <table id="tbl-proforma" class="inner-tbl-edit" cellspacing="0">
                                    <th></th>
                                    <th></th>
                                    <th>Ref</th>
                                    <th>Designation</th>
                                    <th>Quantit&eacute;</th>
                                    <tr>
                                        <td class="col1"><strong>1</strong></td>
                                        <td class="col2"><input type="checkbox" /></td>
                                        <td class="col4"><input class="input-txt" type="text" name="arrReferences[]"></td>
                                        <td class="col3"><textarea name="arrDesignations[]"></textarea></td>
                                        <td class="col4"><input class="input-txt" type="text" name="arrQuantitys[]" value="1"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="btn-add-row2" value="Ajouter" />
                                <input type="button" id="btn-remove-selected-rows2" value="Supprimer la selection" />
                            </td>
                        </tr>
                    </table>
                    
                    <input type="hidden" name="txtCustomerId" id="txtCustomerId" value="-1" />
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">Coordonn&eacute;es client</th>
                        <tr>
                            <td class="col1"><strong>Nom société</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerName" id="txtCustomerName" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Adresse</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerAddress" id="txtCustomerAddress" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Nom de purchase</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtPurchaseName" id="txtPurchaseName" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Email</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerMail" id="txtCustomerMail" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>T&eacute;l</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtCustomerPhone" id="txtCustomerPhone" /></td>
                        </tr>
                    </table>
                    
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">Validit&eacute; de l'offre</th>
                        <tr>
                            <td class="col1"><strong>DEB</strong></td>
                            <td class="col2"><input class="input-date" id="txtValidityStart" type="text" name="txtValidityStart" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>FIN</strong></td>
                            <td class="col2"><input class="input-date" id="txtValidityEnd" type="text" name="txtValidityEnd" /></td>
                        </tr>
                    </table>
                    
                    <div class="div-btnbar">                    
                        <td><input type="submit" class="btnGradientBlue" value="Envoyer" /></td>
                    </div>                    
                </form>
                <div id="msgConfirmation" class="toggleSuccess" style="display: none;">
                    <p>La cotation saisie a &eacute;t&eacute; enregistr&eacute;e</p>
                </div>
            </div>
        </div>