        <!-- UI dialogs -->
        {logout_dialog_content}
        
        <div id="dialog-confirm-add-cotation" title="Ajout de nouvelle cotation" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Confirmez vous l'ajout de cotation ?</p>
        </div>
        
        <div id="dialog-confirm-remove-elements" title="Suppression des &eacute;l&eacute;ments s&eacute;lectionn&eacute;s" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Etes vous sur de vouloir supprimer les &eacute;l&eacute;ments s&eacute;lectionn&eacute;s ?</p>
        </div>
        <!-- End of UI dialogs -->
        
        <div id="topnav">
            <ul>
                <li id="first"><a href="{base_url}homepage">Accueil</a></li>
                <li class="active"><a href="#">Nouveau dispatch</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}add_img.png" width="64px" height="64px" />
                Nouvelle cotation a dispatcher
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" /></a>
            </div>
        </div>
        <div class="clear"></div> 