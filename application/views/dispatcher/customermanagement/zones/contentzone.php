        <div id="homepage">
            <div class="bodycontent">
                <h2 class="subhead">Liste des clients</h2>
              
                <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nom client</th>
                        <th>Adresse</th>
                        <th>Email</th>
                        <th>T&eacute;l</th>
                        <th>Nom de purchase</th>
                        <th colspan="2">Actions</th>
                    </tr>
                </thead>  
                {var_customers}
                <tr>
                    <td><strong>{customer_name}<strong></td>
                    <td>{customer_address}</td>
                    <td>{customer_mail}</td>
                    <td>{customer_phone}</td>
                    <td>{purchaser_name}</td>
                    <td>
                        <input type="button" value="Editer" onclick="editCustomer({customer_id});">
                    </td>
                    <td>
                        <input type="button" value="Supprimer" onclick="deleteCustomer({customer_id});">
                    </td>
                </tr>
                {/var_customers}
                </table>
            </div>
        </div>