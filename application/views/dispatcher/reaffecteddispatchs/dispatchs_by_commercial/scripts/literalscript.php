<script type="text/javascript">
    function loadDispatchsByCommercial(commercialId, destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-commercials-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_dispatchs_by_commercial_id/' + commercialId,
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    $(function() {        
        var commercialId = $('#txtCommercialId').val();
        loadDispatchsByCommercial(commercialId);
        
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
        
        $('.inner-btn').button();
    });
/*
 var json = JSON.parse(res);
                    
                    for( var i = 0; i < json.commercials.length; i++ )
                    {    
                        var commercial = json.commercials[i][0];
                        
                        var htmlExpr = '<td>' + commercial.last_name + '</td>' +
                                        '<td>' + commercial.first_name + '</td>' +
                                        '<td><input type="submit" value="S&eacute;lectionner" class="btn-select-commercial"></td>';
                        
                        if( i == 0 )
                        {
                            var lastRow = $('#tbl-commercials tr:last');
                            lastRow.html(htmlExpr);
                        }
                        else
                        {
                            var lastRow = $('#tbl-commercials tr:last');
                            lastRow.after(htmlExpr);
                        }
                    }
                    
                    $('.btn-select-commercial').button();
                    
                    $.ajax({
                url: '{base_url}services/dispatcherservices/source_customers_except_one',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                   
                }
            });
*/
</script>