<script type="text/javascript">
    function loadCommercials(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-commercials-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_commercials',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    //Calls after document loading
    $(function() {
        //loadPendingDispatchs(); //Call
        loadCommercials(); //Call
        
        $("#tbl-commercials-data tbody tr").live('click', function() {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            
            var url = '{base_url}dispatcher/reaffecteddispatchs/commercial_dispatchs/' + rowid;
            redirect(url);
        });
        
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
        
        $('.inner-btn').button();
    });
</script>