        <!-- UI dialogs -->
        {logout_dialog_content}
        
        <div id="dialog-confirm-create-report" title="G&eacute;n&eacute;ration de proforma" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Confirmez la création de proforma ?</p>
        </div>
        <!-- End of UI dialogs -->

        <div id="topnav">
            <ul>
                <li id="first" class="active"><a href="#">Reporting</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                Edition avant reporting de proforma
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" alt="logout_img" /></a>
            </div>
        </div>
        <div class="clear"></div>