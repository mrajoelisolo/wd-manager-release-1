<script type="text/javascript">
    $(function() {
        $.post("{base_url}services/dateservices/today", function(data) {
            $( "#txtCotationDate" ).val(data);
        });
        
        $( "#txtCotationDate" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 2,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {}
        });
    });
    
    function createPDFReport() {
        var res = true;
    
        var idDialogCreateReport = '#dialog-confirm-create-report';
        $( idDialogCreateReport ).dialog( "destroy" );
        $( idDialogCreateReport ).dialog({
            resizable: false,
            height:140,
            modal: true,
            buttons: {
                "Oui": function() {
                    $('#form-report-cotation').submit();
                },
                "Non": function() {
                    res = false;
                    $( this ).dialog( "close" );
                }
            }
        });
    
        return res;
    }
</script>