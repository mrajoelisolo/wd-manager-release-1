        <div id="homepage">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-relance-cotations-data" width="100%">
                <thead>
            		<tr>
                        <th>Cotation Id</th>
                        <th>N&ordm; PRF</th>
                        <th>Client</th>
                        <th>Designation demande</th>
                        <th>Designation proposition</th>
                        <th>DEB</th>
                        <th>FIN</th>
                        <th>Commercial</th>
            		</tr>
            	</thead>
                <tbody>
                </tbody>
            </table>
        </div>
        
        <!-- Modal dialog -->
        <div id="dialog-overlay"></div>
    	<div id="dialog-box">    
            <div class="dialog-header">
                <h2>Relance cotation</h2>
            </div>
    		<div class="dialog-content">
    			<div id="dialog-message">
                    <!-- Load the form with from template parser -->
                    {content_sended_cotations}
                </div>
            </div>
            <div class="dialog-footer">
                <p>
                    <input type="submit" id="btnClose" value="Fermer" class="btnGradientBlue" />
                </p>
            </div>
        </div>