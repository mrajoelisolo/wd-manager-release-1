<script type="text/javascript">
    /**
     * Charger la liste des cotations a relancer
     */
    function loadRelanceCotations(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-relance-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_relance_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    $(function(){
        loadRelanceCotations();
        
        $("#tbl-relance-cotations-data tr").live('click', function()
        {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/dispatcherservices/fetch_sended_cotation',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call
                    setSendedCotationForm(json, tsel); //Call
                }
            });
        });
        
        $('#btnClose').click(function() {
            hideModalDialog();
        });
    });
</script>