        <input type="hidden" id="txtCountUrgentCotations" value="{var_count_urgent_dispatchs}" />
        <input id="txtAccountState" type="hidden" value="{var_account_state}" />
        <input id="txtRelanceState" type="hidden" value="{var_relance_state}" />
        
        <!-- UI dialogs -->
        {logout_dialog_content}
        <!--End of UI dialogs -->

        <div id="topnav">
            <ul>
                <li id="first" class="active"><a href="#">Accueil</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}home_img.png" alt="header_img" width="64px" height="64px">
                Bonjour {var_firstname}, bienvenue dans Wide manager en tant que dispatcher
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" alt="logout_img" /></a>
            </div>
        </div>
        <div class="clear"></div>