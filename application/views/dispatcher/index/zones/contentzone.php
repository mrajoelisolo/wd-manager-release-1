    <div id="homepage">
        <div class="rightcol">
            <h2 class="subhead">Tableau de bord</h2>
            <ul>
                <!--
                <li class="list_icon">Voir la <a href="{base_url}dispatcher/sendedcotations">liste</a> de toutes les cotations.</li>
                -->
                <li class="alert_icon">Il y a <strong>{var_count_relance_cotations}</strong> cotations a relancer. <a href="{base_url}dispatcher/relancesendedcotations">Voir</a></li>
                <li class="alert_icon">Il y a <strong>{var_count_urgent_dispatchs}</strong> dispatchs urgents. <a href="{base_url}dispatcher/urgentdispatchs">Voir</a></li>
                <li class="bluenotification_icon">Il y a <strong>{var_count_pending_dispatchs}</strong> dispatchs non encore trait&eacute;s. <a href="{base_url}dispatcher/pendingdispatchs">Voir</a></li>
                <li class="bluenotification_icon">Il y a <strong>{var_count_sended_dispatchs}</strong> cotations envoy&eacute;es. <a href="{base_url}dispatcher/sendedcotations">Voir</a></li>
            </ul>
        </div>
    
        <div class="leftcol">
            <h2 class="subhead">Actions</h2>
            <ul>
                <li class="add_icon"><a href="{base_url}dispatcher/newdispatch">Nouveau dispatch</a></li>
                <li class="report_icon"><a href="{base_url}dispatcher/reporting/show">Reporting de proforma accordés</a></li>
                <li class="idea_icon"><a href="{base_url}dispatcher/workflow">Diagramme workflow</a></li>
                <!--
                <li class="contacts_icon"><a href="{base_url}dispatcher/customermanagement">Gestion des clients</a></li>
                <li class="folderbackup_icon"><a href="{base_url}dispatcher/reaffecteddispatchs">R&eacute;affectation</a> de dispatchs</li>
                -->
            </ul>
        </div>
    </div>

    <!-- Modal dialog : Urgent dispatchs -->
    <div id="dialog-overlay"></div>
   	<div class="notification-dialog" id="dialog-urgent-dispatchs">    
  		<div class="dialog-content">
 			<div id="dialog-message">
                <!-- Load data from ajax -->
                <div style="float:right">
                    <img src="{path_img}urgent_img.png" alt="urgent_img"/>
                </div>
                <h2 class="subhead">Il y a des dispatchs urgentes en attente de traitement</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-urgent-dispatchs-data" width="100%">
                    <thead>
                		<tr>
                            <th>Cotation Id</th>
                            <th>N&ordm; cotation client</th>
                            <th>Client</th>
                            <th>Designation demande</th>
                            <th>Date limite d'envoi</th>
                            <th>Commercial</th>
                		</tr>
                	</thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="dialog-footer">
            <p>
                <input type="submit" id="btnShowUrgentDispatchsForm" value="Voir" class="btnGradientGreen" />
                &nbsp;
                <input type="submit" id="btnCloseUrgentDispatchsForm" value="Fermer" class="btnGradientRed" />
            </p>
        </div>
    </div>
    
    <!-- Modal dialog : Sended cotations, relance -->
    <div class="notification-dialog" id="dialog-relance-cotations">    
  		<div class="dialog-content">
 			<div id="dialog-message">
                <!-- Load data from ajax -->
                <div style="float:right">
                    <img src="{path_img}todo_img.png" alt="todo_img"/>
                </div>
                <h2 class="subhead">Il y a des cotations a relancer</h2>
                <table cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-relance-cotations-data" width="100%">
                    <thead>
                		<tr>
                            <th>Cotation Id</th>
                            <th>N&ordm; PRF</th>
                            <th>Client</th>
                            <th>Designation demande</th>
                            <th>Designation proposition</th>
                            <th>DEB</th>
                            <th>FIN</th>
                            <th>Commercial</th>
                		</tr>
                	</thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="dialog-footer">
            <p>
                <input type="submit" id="btnShowRelanceCotationsForm" value="Voir" class="btnGradientGreen" />
                &nbsp;
                <input type="submit" id="btnCloseSendedCotationsForm" value="Fermer" class="btnGradientRed" />
            </p>
        </div>
    </div>