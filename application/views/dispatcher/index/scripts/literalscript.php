<script type="text/javascript">
    /**
     * Vérifie si des dispatchs urgents existent et vérouille le compte si c'est le cas
     */
    function checkAndLockIfUrgentDispatchExists()
    {   
        var accountState = $('#txtAccountState').val();

        if(accountState == 'URGENT_DISPATCHS')
        {
            showModalDialog('dialog-urgent-dispatchs', 'dialog-overlay'); //Call
            //Load data : the urgent dispatchs
            loadUrgentDispatchs();
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Vérifie s'il éxiste des cotations à relancer
     */
    function checkIfRelanceExists()
    {
        var relanceState = $('#txtRelanceState').val();
            
        if( relanceState == 'RELANCE' )
        {
            showModalDialog('dialog-relance-cotations', 'dialog-overlay'); //Call
            loadRelanceCotations();
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Charger la liste des dispatchs urgents
     */
    function loadUrgentDispatchs(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-urgent-dispatchs-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_urgent_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }

    /**
     * Charger la liste des cotations a relancer
     */
    function loadRelanceCotations(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-relance-cotations-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_relance_cotations',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }

    $(function() {
        var bUrgentDispatchsExists = checkAndLockIfUrgentDispatchExists(); //Call
        //If urgent dispatchs don't exists, we can continue to check if relance exists
        if( ! bUrgentDispatchsExists )
        {
            checkIfRelanceExists(); //Call
        }
        
        $('#btnShowUrgentDispatchsForm').click(function() {
            document.location = '{base_url}dispatcher/urgentdispatchs';
        });
        
        $('#btnShowRelanceCotationsForm').click(function() {
            document.location = '{base_url}dispatcher/relancesendedcotations';
        });
        
        $('#btnCloseUrgentDispatchsForm, #btnCloseSendedCotationsForm').click(function()
        {
            hideModalDialog();
        });
    });
</script>