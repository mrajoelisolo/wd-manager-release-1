<script type="text/javascript">
    /**
     * Charger la liste des dispatchs en attente (chargement spécifique pour les cotations urgentes)
     */
    function loadUrgentDispatchs(destroy)
    {
        if( ! destroy )
            destroy = false;
                
        $('#tbl-urgent-dispatchs-data').dataTable({
            'bProcessing': false,
            'bServerSide': true,
            'bLengthChange': false,
            'bPaginate': true,
            'bInfo': false,
            'bFilter': false,
            'bSort': true,
            'bDestroy': destroy,
            'sAjaxSource': '{base_url}services/dispatcherservices/source_urgent_dispatchs',
            'sServerMethod': 'POST',
            'aoColumnDefs': [
                { 'bSearchable': false, "bVisible": false, 'aTargets': [0] }
            ],
            "oLanguage": {
                "sUrl": "{base_url}assets/language/fr_FR.txt"
            }
        });
    }
    
    //Calls after document loading
    $(function() {
        loadUrgentDispatchs(); //Call
        
        $("#tbl-urgent-dispatchs-data tbody tr").live('click', function() {
            var tsel = $(this).attr('id');

            var rowid = parseInt(tsel.replace('row_', ''));
            $.ajax({
                url: '{base_url}services/dispatcherservices/fetch_pending_dispatch',
                type: 'post',
                datatype: 'json',
                async: false,
                data: { id: rowid },
                success: function(res) {
                    var json = JSON.parse(res);
                    showModalDialog('dialog-box', 'dialog-overlay'); //Call from pendingcotations scripts
                    setPendingDispatchForm(json, tsel); //Call
                }
            });
        });
        
        $('#btnClose').click(function() {
            hideModalDialog('dialog-box');
        });
    });
</script>