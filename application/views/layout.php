<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />    
	<title>{page_title}</title>
    <link rel="shortcut icon" href="{path_img}favicon.ico" />
    {css_resources}
    <link rel="stylesheet" type="text/css" href="{var_resource}" />
    {/css_resources}
</head>

<body>
    <div id="wrap">
        <div id="header">
            <h1 id="sitename">
                Wide CRM manager
                <img src="{path_img}beta_icon.png" alt="beta_logo">
                <span id="description">Manage your system information</span>
            </h1>
            {header_zone}
        </div>
        
        <div id="contents">
            {content_zone}
        </div>
        
        <div id="footer">
            {footer_zone}
        </div>
    </div>
    
    {js_resources}
    <script type="text/javascript" src="{var_resource}"></script>
    {/js_resources}
    {js_literals}
    {var_literal_js}
    {/js_literals}
</body>
</html>