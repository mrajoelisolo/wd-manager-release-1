<script type="text/javascript">
    function clearForm() {
        $('#txtLogin').val('');
        $('#txtPassword').val('');
        $('#txtFirstName').val('');
        $('#txtLastName').val('');
        $('#txtAcronym').val('');
    }

        function validateForm()
        {
            var res = true;
            var errorColor = '#ffc1c1';

            //Login field
            var idField = '#txtLogin';
            if( $(idField).val().trim() == '' )
            {
                $(idField).css('background-color', errorColor);
                res = false;
            }
            else { $(idField).removeAttr('style'); }
            
            //Password field
            var idField = '#txtPassword';
            if( $(idField).val().trim() == '' )
            {
                $(idField).css('background-color', errorColor);
                res = false;
            }
            else { $(idField).removeAttr('style'); }
            
            //First name field
            var idField = '#txtFirstName';
            if( $(idField).val().trim() == '' )
            {
                $(idField).css('background-color', errorColor);
                res = false;
            }
            else { $(idField).removeAttr('style'); }
            
            //Last name field
            var idField = '#txtLastName';
            if( $(idField).val().trim() == '' )
            {
                $(idField).css('background-color', errorColor);
                res = false;
            }
            else { $(idField).removeAttr('style'); }
            
            //Acronym field
            var idField = '#txtAcronym';
            if( $(idField).val().trim() == '' )
            {
                $(idField).css('background-color', errorColor);
                res = false;
            }
            else { $(idField).removeAttr('style'); }

            return res;
        }

    //UI Dialog
    $(function() {
        var idDialogAddCotation = "#dialog-confirm-add-user";                   
        $( idDialogAddCotation ).dialog( "destroy" );
            
            $( "#form-cotation" ).submit(function(e) {
                e.preventDefault();

                var bValidated = validateForm();
                if( !bValidated ) {
                    showConfirmation('Le formulaire est incomplet');
                    return false;
                }

                $( idDialogAddCotation ).dialog({
                    resizable: false,
                    height:140,
                    modal: true,
                    buttons: {
                        "Confirmer": function() {
                            $.ajax({
                                url: "{base_url}adminservices/accountservices/create",
                                type: "post",
                                dataType: "json",
                                data: $("#form-cotation").serialize(),
                                success: function(res) {
                                    $( idDialogAddCotation ).dialog( "close" );
            
                                        //Show notification
                                        showConfirmation();
                                        clearForm();
                                }
                            });
                        },
                        "Annuler": function() {
                            $( idDialogAddCotation ).dialog( "close" );
                        }
                    }
                });
            });
                    
            /***
             * Affiche message de confirmation succes/erreur
             */
            function showConfirmation(error) {
                var msg = 'Enregistr&eacute;e';
    
                if(error) {
                    $('#msgConfirmation').removeClass('toggleSuccess');
                    $('#msgConfirmation').addClass('toggleError');
            
                    msg = error;
                }
                else
                {
                    $('#msgConfirmation').removeClass('toggleError');
                    $('#msgConfirmation').addClass('toggleSuccess');
                }
            
                $('#msgConfirmation p').html(msg);
                $('#msgConfirmation').show('bounce', {}, callback);
            }
                    
            function callback() {
                setTimeout(function() {
                    $( "#msgConfirmation:visible" ).removeAttr( "style" ).fadeOut();
                }, 5000);
            }
        });
</script>