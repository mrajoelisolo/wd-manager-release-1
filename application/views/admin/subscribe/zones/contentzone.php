<div id="homepage">
            <div class="bodycontent">
                <h2 class="subhead">Nouvelle compte utilisateur</h2>
                <form id="form-cotation" action="#" method="post">
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <th colspan="2">G&eacute;n&eacute;ral</th>
                        <tr>
                            <td class="col1"><strong>Login</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtLogin" id="txtLogin" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Mot de passe</strong></td>
                            <td class="col2"><input class="input-txt" type="password" name="txtPassword" id="txtPassword" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Nom</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtLastName" id="txtLastName" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Prenom</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtFirstName" id="txtFirstName" /></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Type d'utilisateur</strong></td>
                            <td class="col2">
                                <select class="select-cmb" name="cmbUserType">
                                    <option value="DISPATCHER">Dispatcher</option>
                                    <option value="COMMERCIAL">Commercial</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Initiales (3-5 caract&egrave;res)</strong></td>
                            <td class="col2"><input class="input-txt" type="text" name="txtAcronym" id="txtAcronym" /></td>
                        </tr>
                    </table>
                    
                    <div class="div-btnbar">                    
                        <td><input type="submit" class="btnGradientBlue" value="Enregistrer" /></td>
                    </div>                    
                </form>
                <div id="msgConfirmation" class="toggleSuccess" style="display: none;">
                    <p>La cotation saisie a &eacute;t&eacute; enregistr&eacute;e</p>
                </div>
            </div>
        </div>