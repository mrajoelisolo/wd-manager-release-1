        <div id="homepage">
            <div class="bodycontent">
                <div class="onecol">
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="2">Stats</th>
                            </tr>
                        </thead>
                        <ul>
                            <tr>
                                <td style="width: 50%;">
                                    <li class="infoblue">Nombre d'utilisateurs inscrits</li>
                                </td>
                                <td><p class="overview_count" id="field_count_accounts">0</p></td>
                            </tr>
                        </ul>
                    </table>
                        
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="2">G&eacute;n&eacute;ral</th>
                            </tr>
                        </thead>
                        <tr>
                            <td style="width: 50%;">
                                <ul>
                                    <li class="users"><a href="{base_url}admin/subscribe">Gestion de compte utilisateurs</a></li>
                                    <li class="contacts"><a href="{base_url}admin/customers">Liste des contacts de clients</a></li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <!--
                                    <li class="users"><a href="{base_url}admin/listusers/page">Gérer la liste des chefs d'ateliers'</a></li>
                                    -->
                                </ul>
                            </td>
                        </tr>
                    </table>
                    
                    <!--
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="2">Administration</th>
                            </tr>
                        </thead>
                        <tr>
                            <td style="width: 50%;">
                                <ul>
                                    <li class="backup"><a href="#">Sauvegarder la base de donn&eacute;es</a></li>
                                    <li class="trash"><a href="#">Vider la base de donn&eacute;es</a></li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li class=""></li>
                                    <li class=""></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    -->
                </div>
            </div>
        </div>