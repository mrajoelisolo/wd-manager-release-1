    <!-- UI Dialogs -->
    {logout_dialog_content}
    <!-- End of Dialogs -->
    
        <div id="topnav">
            <ul>
                <li id="first" class="active"><a href="#">Accueil</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}shield_img.png" width="64px" height="64px" />
                Bienvenue dans le panneau admin
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" alt="logout_img" /></a>
            </div>
        </div>
        <div class="clear"></div>