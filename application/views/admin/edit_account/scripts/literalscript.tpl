<script type="text/javascript">
    function validateForm() {
        var res = true;
        var errorColor = '#ffc1c1';

        //Login field
        var idField = '#txtLogin';
        if( $(idField).val().trim() == '' ) {
            $(idField).css('background-color', errorColor);
            res = false;
        }
        else { $(idField).removeAttr('style'); }
    
        //First name field
        var idField = '#txtFirstName';
        if( $(idField).val().trim() == '' ) {
            $(idField).css('background-color', errorColor);
            res = false;
        }
        else { $(idField).removeAttr('style'); }
    
        //Last name field
        var idField = '#txtLastName';
        if( $(idField).val().trim() == '' ) {
            $(idField).css('background-color', errorColor);
            res = false;
        }
        else { $(idField).removeAttr('style'); }
    
        //Acronym field
        var idField = '#txtAcronym';
        if( $(idField).val().trim() == '' ) {
            $(idField).css('background-color', errorColor);
            res = false;
        }
        else { $(idField).removeAttr('style'); }

        return res;
    }
    
    $(function() {
        var idDialogUpdateUser = "#dialog-confirm-update-user";                   
        $( idDialogUpdateUser ).dialog( "destroy" );
        
        var idForm = '#form-user';
        $( idForm ).submit(function(e) {
            e.preventDefault();

            var bValidated = validateForm();
            if( !bValidated ) {
                showConfirmation('Le formulaire est incomplet');
                return false;
            }

            $( idDialogUpdateUser ).dialog({
                resizable: false,
                height:140,
                modal: true,
                buttons: {
                    "Confirmer": function() {
                        $.ajax({
                            url: "{base_url}adminservices/accountservices/update",
                            type: "post",
                            dataType: "json",
                            data: $(idForm).serialize(),
                            success: function(res) {
                                if( res.status == 1 ) {
                                    document.location.href = '{base_url}admin/accounts';
                                }
                                else
                                {
                                    showConfirmation('Erreur serveur');
                                }

                                $( idDialogUpdateUser ).dialog( "close" );
                            }
                        });
                    },
                    "Annuler": function() {
                        $( idDialogUpdateUser ).dialog( "close" );
                    }
                }
            });
        });
        
        /***
        * Affiche message de confirmation succes/erreur
        */
        function showConfirmation(error) {
            var msg = 'Enregistr&eacute;e';

            if(error) {
                $('#msgConfirmation').removeClass('toggleSuccess');
                $('#msgConfirmation').addClass('toggleError');

                msg = error;
            }
            else
            {
                $('#msgConfirmation').removeClass('toggleError');
                $('#msgConfirmation').addClass('toggleSuccess');
            }
            
            $('#msgConfirmation p').html(msg);
            $('#msgConfirmation').show('bounce', {}, callback);
        }
    
        function callback() {
            setTimeout(function() {
                $( "#msgConfirmation:visible" ).removeAttr( "style" ).fadeOut();
            }, 5000);
        }
    });
</script>