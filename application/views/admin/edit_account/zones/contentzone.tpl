<div id="homepage">
            <div class="bodycontent">
                <h2 class="subhead">Edition de compte utilisateur</h2>
                <form id="form-user" action="#" method="post">
                    <input type="hidden" name="txtAccountId" value="{var_account_id}">
                    <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="2">G&eacute;n&eacute;ral</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="col1"><strong>Login</strong></td>
                                <td class="col2"><input class="input-txt" type="text" name="txtLogin" id="txtLogin" value="{var_login}"></td>
                            </tr>
                            <tr>
                                <td class="col1"><strong>Nouveau mot de passe</strong></td>
                                <td class="col2"><input class="input-txt" type="password" name="txtPassword" id="txtPassword" value=""></td>
                            </tr>
                            <tr>
                                <td class="col1"><strong>Nom</strong></td>
                                <td class="col2"><input class="input-txt" type="text" name="txtLastName" id="txtLastName" value="{var_last_name}"></td>
                            </tr>
                            <tr>
                                <td class="col1"><strong>Prenom</strong></td>
                                <td class="col2"><input class="input-txt" type="text" name="txtFirstName" id="txtFirstName" value="{var_first_name}"></td>
                            </tr>
                            <tr>
                                <td class="col1"><strong>Type d'utilisateur</strong></td>
                                <td class="col2">
                                    <select class="select-cmb" name="cmbUserType">
                                        <option value="DISPATCHER" {is_dispatcher}>Dispatcher</option>
                                        <option value="COMMERCIAL" {is_commercial}>Commercial</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="col1"><strong>Initiales (3-5 caract&egrave;res)</strong></td>
                                <td class="col2"><input class="input-txt" type="text" name="txtAcronym" id="txtAcronym" value="{var_acronym}"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <strong>Note:</strong> Le chiffrement du mot de passe est a sens unique,
                                    remplir le champ <strong>Nouveau mot de passe</strong> pour réinitialiser le mot de passe
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="div-btnbar">                    
                        <td><input type="submit" class="btnGradientBlue" value="Modifier" /></td>
                    </div>                    
                </form>
                <div id="msgConfirmation" class="toggleSuccess" style="display: none;">
                    <p>La cotation saisie a &eacute;t&eacute; enregistr&eacute;e</p>
                </div>
            </div>
        </div>