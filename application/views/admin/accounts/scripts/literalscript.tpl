<script type="text/javascript">
    $(function() {
        $('#btnRechercher').click(function() {
            var f_nom       = $('#txtNom').val();
            var f_prenom    = $('#txtPrenom').val();
            
            var criteria = '';
            
            if( f_nom != '' ) {
                criteria += 'nom/' +  f_nom;
            }
            
            if( f_prenom != '' ) {
                if( criteria != '' )
                    criteria += '/';

                criteria += 'prenom/' +  f_prenom;
            }

            document.location.href = '{base_url}admin/accounts/page/' + criteria;
        });
        
        $(".uibtn").button();
    });
</script>