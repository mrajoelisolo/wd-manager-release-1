        <!-- UI dialogs -->
        {logout_dialog_content}

        <div id="dialog-confirm-add-user" title="Ajout de nouvelle compte utilisateur" style="display: none;">
	       <p><span class="ui-icon ui-icon-help" style="float:left; margin:0 7px 20px 0;"></span>Confirmez vous l'ajout ?</p>
        </div>
        <!-- UI dialogs -->
        
        <div id="topnav">
            <ul>
                <li id="first"><a href="{base_url}admin/home">Accueil</a></li>
                <li><a href="{base_url}admin/subscribe">Nouveau compte</a></li>
                <li class="active"><a href="{base_url}admin/accounts">Liste des comptes</a></li>
            </ul>
        </div>   
        <div class="clear"></div>
        
        <div id="headercolumn1">
            <p class="description">
                <img src="{path_img}edit_img.png" width="64px" height="64px" />
                Liste des comptes utilisateurs
            </p>
        </div> 
        
        <div id="headercolumn2">
            <div id="form_logout" align="right">
                <a id="btn_logout" href="javascript:void(0);"><img src="{path_img}logout.png" /></a>
            </div>
        </div>
        <div class="clear"></div>  