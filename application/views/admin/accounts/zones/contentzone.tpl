    <div id="homepage">
        <div class="bodycontent">
            <form id="form-search" action="#" method="post">
                <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="4">Filtre de recherche</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col1"><strong>Nom</strong></td>
                            <td><input class="input-txt" type="text" name="txtNom" id="txtNom"></td>
                        </tr>
                        <tr>
                            <td class="col1"><strong>Prénom</strong></td>
                            <td><input class="input-txt" type="text" name="txtPrenom" id="txtPrenom"></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <a href="{base_url}admin/accounts">Afficher tout</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <input type="button" value="Chercher" id="btnRechercher" class="uibtn">
                            </td>
                        </tr>
                    <tr>
                        <td colspan="2">
                            <table class="inner-tbl-edit" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Login</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {var_accounts}
                                    <tr id="account_row_{account_id}">
                                        <td>{last_name}</td>
                                        <td>{first_name}</td>
                                        <td>{login}</td>
                                        <td>
                                            <a id="afficher_{account_id}" href="#">Afficher</a> |
                                            <a id="editer_{account_id}" href="{base_url}admin/edit_account/by_id/{account_id}">Modifier</a> |
                                            <a id="supprimer_{account_id}" href="#" id="delete_account_{account_id}">Supprimer</a>
                                        </td>
                                    </tr>
                                    {/var_accounts}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {is_pagination}
                            <div class="wrap-pagination">
                                {var_pagination}
                            </div>
                            {/is_pagination}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>