    <div id="homepage">
        <div class="bodycontent">
            <form id="form-search" action="#" method="post">
                <table class="tbl-edit" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="4">Filtre de recherche</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col1"><strong>Nom</strong></td>
                            <td><input class="input-txt" type="text" name="txtNom" id="txtNom"></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <a href="{base_url}admin/accounts">Afficher tout</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <input type="button" value="Chercher" id="btnRechercher" class="uibtn">
                            </td>
                        </tr>
                    <tr>
                        <td colspan="2">
                            <table class="inner-tbl-edit" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Société</th>
                                        <th>Email</th>
                                        <th>Nom de purchaser</th>
                                        <th>Téléphone</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {var_customers}
                                    <tr id="customer_row_{customer_id}">
                                        <td>{enterprise_name}</td>
                                        <td>{customer_mail}</td>
                                        <td>{purchaser_name}</td>
                                        <td>{customer_phone}</td>
                                        <td>
                                            <a id="afficher_{customer_id}" href="#">Afficher</a> |
                                            <a id="editer_{customer_id}" href="{base_url}admin/edit_customer/by_id/{customer_id}">Modifier</a> |
                                            <a id="supprimer_{customer_id}" href="#" id="delete_customer_{customer_id}">Supprimer</a>
                                        </td>
                                    </tr>
                                    {/var_customers}
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {is_pagination}
                            <div class="wrap-pagination">
                                {var_pagination}
                            </div>
                            {/is_pagination}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>