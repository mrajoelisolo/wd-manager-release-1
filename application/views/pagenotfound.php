<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="{path_img}favicon.ico" />

    <style>
        body {background-color: #0055a6; font:11px Verdana, Arial, Helvetica, sans-serif; color:#333;}        
        h1 {display: block; padding-top: 35px; height: 73px; padding-left: 10px; font:bold italic 30px "Trebuchet MS", Arial, Helvetica, sans-serif; color:#fff;}
        a {color: #ffffff; text-decoration: none;}
        #maindiv {
            background: #00407d;
            text-align: center;
            width: 640px;
            height: auto;
            margin: auto;
            margin-top: 100px;
            
            -webkit-box-shadow: 5px 2px 10px rgba(0, 0, 0, 0.5);
        	-moz-box-shadow: 5px 2px 10px rgba(0, 0, 0, 0.5);
        	
        	-moz-border-radius: 5px;
        	-webkit-border-radius: 5px;
        }
    </style>

	<title>Wide manager - 404 page not found</title>
</head>

<body>
    <div id="maindiv">
        <h1>404 Page non trouv&eacute;</h1>
        <img src="{path_img}notification_img.png" alt="pagenotfound" />
        <h1>D&eacute;sol&eacute;, la page demand&eacute;e n'&eacute;xiste pas</h1>
        <a href="{base_url}homepage">Aller &agrave; la page d'accueil</a>
        <br />
        <br />
    </div>
</body>
</html>