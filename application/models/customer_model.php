<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class Customer_Model extends CI_Model
{
    const VIEW_NAME = 'wv_customer';
    const CUSTOMER_TABLE_NAME = 'w_customer';
    const ENTERPRISE_TABLE_NAME = 'w_enterprise';
    
    /**
     * Récupérer la liste des clients par leur nom
     */
    public function fetch_by_name($name, $limit = 20)
    {
        $res = $this->db->select('*')
                        ->from(self::VIEW_NAME)
                        ->like('enterprise_name', $name, 'after')
                        ->limit($limit)
                        ->get()
                        ->result();
        return $res;
    }
    
    /**
     * Enregistrer un client dans la base
     */
    public function save($enterprise_name, $enterprise_address, $customer_mail, $customer_phone, $purchaser_name)
    {
        $oEnterprise = $this->db->select('*')
                                ->from(self::ENTERPRISE_TABLE_NAME)
                                ->like('enterprise_name', $enterprise_name)
                                ->get()
                                ->row();

        if( !empty($oEnterprise) )
        {
            $enterprise_id = $oEnterprise->enterprise_id;
        }
        else
        {
            $this->db->set('enterprise_name', $enterprise_name);
            $this->db->set('enterprise_address', $enterprise_address);
            $this->db->insert(self::ENTERPRISE_TABLE_NAME);
            $enterprise_id = $this->db->insert_id();
        }
        
        $this->db->set('customer_mail', $customer_mail);
        $this->db->set('customer_phone', $customer_phone);
        $this->db->set('purchaser_name', $purchaser_name);
        $this->db->set('enterprise_fk', $enterprise_id);
        $this->db->insert(self::CUSTOMER_TABLE_NAME);
        
        return $this->db->insert_id();
    }
    
    /**
     * Obtenir un client à partir de son identifiant
     */
    public function get_by_id($id)
    {
        return $this->db->select('*')
                        ->from(self::VIEW_NAME)
                        ->where('customer_id', $id)
                        ->get()
                        ->row();
    }
    
    public function fetch_array_customers($customer_name = '', $customer_address = '', $purchaser_name = '', $start = 0, $count = 20)
    {
        $this->db->select('*')
                 ->from(self::VIEW_NAME);

        if( ! empty($customer_name) )
        {
            $this->db->where('enterprise_name', $customer_name);
        }
        
        if( ! empty($customer_address) )
        {
            $this->db->where('enterprise_address', $customer_address);
        }
        
        if( ! empty($purchaser_name) )
        {
            $this->db->where('purchaser_name', $purchaser_name);
        }
        
        $res = $this->db->limit($count, $start)
                        ->get()
                        ->result_array();
                        
        return $res;
    }
    
    public function count_customers($customer_name = '', $customer_address = '', $purchaser_name ='')
    {
        $this->db->select('COUNT(*) as count')
                 ->from(self::VIEW_NAME);

        if( ! empty($customer_name) )
        {
            $this->db->where('enterprise_name', $customer_name);
        }
        
        if( ! empty($customer_address) )
        {
            $this->db->where('enterprise_address', $customer_address);
        }
        
        if( ! empty($purchaser_name) )
        {
            $this->db->where('purchaser_name', $purchaser_name);
        }
        
        $res = $this->db->get()
                        ->row();
                        
        return $res->count;
    }
    
    public function getCustomers($customerName, $start, $count)
    {
        $this->db->select('*')
                 ->from(self::VIEW_NAME);

        if( ! empty($customerName) )
        {
            $this->db->like('enterprise_name', $customerName, 'after');
        }
        
        $res = $this->db->limit($count, $start)
                        ->get()
                        ->result();
        return $res;
    }
    
    public function countCustomers($customerName)
    {
        $this->db->select('COUNT(*) AS count')
                 ->from(self::VIEW_NAME);

        if( ! empty($customerName) )
        {
            $this->db->like('enterprise_name', $customerName, 'after');
        }
        
        $res = $this->db->get()
                        ->row();
        return $res->count;
    }
}