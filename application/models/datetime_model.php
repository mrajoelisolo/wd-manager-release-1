<?php

class DateTime_Model extends CI_Model
{
    /*    
    public function now()
    {   
        $format = 'Y-m-d H:i:m';
        $interval = 0;
                
        $hoursAfter = (int) 3;
        if( $hoursAfter != 0 )
        {   
        	$interval = $hoursAfter * 3600;
        }
                
        $datetime = date($format, time() + $interval);
                
        echo $datetime;
    }
    */
    
    public function getMySqlNowDate()
    {
        $hour_interval = 3;
        
        $format = 'Y-m-d';
        
        $interval = $hour_interval * 3600;
        
        $datetime = date($format, time() + $interval);
        
        return $datetime;
    }
    
    public function getMySqlNowDateTime($no_sec = false, $hour_interval = 3)
    {
        $format = 'Y-m-d H:i:m';
        if( $no_sec )
            $format = 'Y-m-d H:i:00';
        
       	$interval = $hour_interval * 3600;
        
        $datetime = date($format, time() + $interval);
        
        return $datetime;
    }                
    
    public function getMySqlDateTime($mysqldate, $no_sec = false, $hour_interval = 3)
    {   
        $format = 'H:i:m';
        if( $no_sec )
            $format = 'H:i:00';
        
       	$interval = $hour_interval * 3600;
        
        $datetime = date($format, time() + $interval);
        
        return $mysqldate . ' ' . $datetime;
    }
    
    public function getDateAfterInterval($date, $interval_jours = 0)
    {
        $strQuery = "SELECT DATE_ADD('" . $date . "', INTERVAL " . $interval_jours . " DAY) AS date;";

        $res = $this->db->query($strQuery)
                        ->row();
        
        return $res->date;
    }
    
    public function countDays($from_date, $to_date, $interval = 0)
    {
        $strQuery = "SELECT TO_DAYS('" . $to_date . "') - TO_DAYS('" . $from_date . "') + " . $interval . " AS days;";
        
        $res = $this->db->query($strQuery)
                        ->row();
        return $res->days;
    }

    public function getCurrentYear()
    {
        $format = 'y';        
        $date = date($format, time());
        
        return $date;
    }
}