<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proposition_Proforma_Model extends CI_Model
{
    const TABLE_NAME = 'w_proposition_proforma';
    const VIEW_NAME = 'wv_proposition_proforma';
    
    public function save($reference, $designation, $qty, $price, $cotation_fk)
    {
        $this->db->set('ref', $reference);
        $this->db->set('designation', $designation);
        $this->db->set('qty', intval($qty));
        $this->db->set('price_unit', intval($price));
        $this->db->set('cotation_fk', intval($cotation_fk));
        $this->db->insert(self::TABLE_NAME);
        
        return $this->db->insert_id();
    }
    
    public function fetch_array_by_cotation_id($id, $start = null, $count = null)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME)
                 ->where('cotation_fk', $id);
        
        if( isset($start) && isset($count) )
        {
            $this->db->limit($count, $start);
        }
        
        $res = $this->db->get()
                    ->result_array();
                        
        return $res;
    }

    public function fetch_array_view_by_cotation_id($id, $start = null, $count = null)
    {
        $this->db->select('*')
                 ->from(self::VIEW_NAME)
                 ->where('cotation_fk', $id);
        
        if( isset($start) && isset($count) )
        {
            $this->db->limit($count, $start);
        }
        
        $res = $this->db->get()
                    ->result_array();
                        
        return $res;
    }

    public function count_by_cotation_id($cotation_id, $cotation_number, $cotation_date, $commercial_fullname)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('cotation_fk', $cotation_id)            
                        ->count_all_results();
        
        return $res;        
    }
    
    public function get_total_amount($cotation_id)
    {
        $query = "SELECT SUM(w.price_unit * w.qty) as amount FROM " . self::TABLE_NAME . " w
                  WHERE w.cotation_fk = " . $cotation_id . ";";

        $res = $this->db->query($query)
                        ->row()
                        ->amount;
        
        return $res;
    }
}