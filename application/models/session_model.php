<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_Model extends CI_Model
{
    const TABLE_NAME_ACCOUNT = 'w_account';
    
    public function createUserSession($oAccount)
    {
        $this->session->set_userdata('account_id', $oAccount->account_id);
        $this->session->set_userdata('user_login', $oUser->user_login);
        $this->session->set_userdata('user_firstname', $oUser->user_firstname);
        $this->session->set_userdata('user_type', $oUser->user_type);
    }
    
    public function setUserData($key, $value)
    {
        $this->session->set_userdata($key, $value);
    }
    
    public function getUserData($key)
    {
        return $this->session->userdata($key);
    }
    
    public function checkIfSessionActive($userType = 'COMMERCIAL')
    {
        $login = $this->session->userdata('user_login');
        
        $oUser = $this->db->select('*')
                          ->from(self::TABLE_NAME_ACCOUNT)
                          ->where('login', $login)
                          ->get()
                          ->row();
        $strUserType = $oUser->user_type;
        
        $res = ($this->session->userdata('user_login') && $strUserType == $userType);
        
        return $res;
    }
    
    public function destroySession()
    {
        $this->session->sess_destroy();
    }
    
    public function getUserSessionData($key)
    {
        return $this->session->userdata($key);
    }
}