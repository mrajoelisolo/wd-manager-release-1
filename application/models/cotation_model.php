<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class Cotation_Model extends CI_Model
{
    const TABLE_NAME = 'w_cotation';
    const VIEW_RELANCE_COTATION = 'wv_relance_cotation';
    const REQUEST_PROFORMA_TABLE = 'w_request_proforma';
    const TABLE_DISPATCH_QUEUE = 'w_dispatch_queue';
    
    /**
     * Enregistrer une cotation
     * 
     * @author Mitanjo
     */
    public function save($cotation_source, $reception_date, $limit_send_date, $validity_start, 
        $validity_end, $cotation_status, $cotation_number_customer, $customer_fk, $dispatcher_fk, $commercial_fk)
    {
        $this->db->set('cotation_source', $cotation_source);
        $this->db->set('reception_date', $reception_date);
        $this->db->set('limit_send_date', $limit_send_date);
        $this->db->set('validity_start', $validity_start);
        $this->db->set('validity_end', $validity_end);
        $this->db->set('cotation_status', $cotation_status);
        $this->db->set('cotation_number_customer', $cotation_number_customer);
        $this->db->set('customer_fk', $customer_fk);
        $this->db->set('dispatcher_fk', $dispatcher_fk);
        $this->db->set('commercial_fk', $commercial_fk);
        $this->db->insert(self::TABLE_NAME);
        
        $cotation_id = $this->db->insert_id();
        
        //Adding the cotation into the dispatch queue
        $this->db->set('account_id', $commercial_fk);
        $this->db->set('cotation_id', $cotation_id);
        $this->db->insert(self::TABLE_DISPATCH_QUEUE);
        
        return $cotation_id;
    }
    
    /**
     * Obtenir le nombre de dispatchs a envoyer à n jours avant la date limite d'envoi
     * 
     * @author Mitanjo
     */
    public function count_dispatchs_to_send_before_by_commercial_id($account_id, $days_before = 3)
    {   
        $strQuery = 'SELECT COUNT(*) as count FROM ' . self::VIEW_RELANCE_COTATION . ' ct
                    WHERE TO_DAYS(ct.limit_send_date) - TO_DAYS(CURDATE()) <= ' . intval($days_before) . '
                    AND ct.cotation_status = \'PENDING\'
                    AND ct.commercial_fk = ' . $account_id ;
        
        $query = $this->db->query($strQuery); 
        $res = $query->row();
        
        return $res->count;
    }    
    
    /**
     * Obtenir les dispatchs a traiter avant les n jours avant la date limite d'envoi
     * 
     * @author Mitanjo
     * @param Id du compte utilisateur
     * @param L'index de début des enregitrements
     * @param Le nombre maximal d'enregistrements a retourner
     * @param Nombre de jours avant la date limite d'envoi
     */
    public function get_dispatchs_to_send_before_by_commercial_id($account_id, $start = 0, $count = 20, $days_before = 3)
    {
        $strQuery = 'SELECT * FROM ' . self::VIEW_RELANCE_COTATION . ' ct
                    WHERE TO_DAYS(ct.limit_send_date) - TO_DAYS(CURDATE()) <= ' . intval($days_before) . '
                    AND ct.cotation_status = \'PENDING\'
                    AND ct.commercial_fk = ' . $account_id . '
                    ORDER BY ct.limit_send_date DESC LIMIT ' . $start . ', ' . $count . ';';
        
        $res = $this->db->query($strQuery);
        
        return $res->result();
    }
    
    /**
     * Obtenir le nombre de dispatchs a envoyer à n jours avant la date limite d'envoi (coté dispatcher)
     * 
     * @author Mitanjo
     */
    public function count_dispatchs_to_send_before_by_dispatcher_id($account_id, $days_before = 3)
    {   
        $strQuery = 'SELECT COUNT(*) as count FROM ' . self::VIEW_RELANCE_COTATION . ' ct
                    WHERE TO_DAYS(ct.limit_send_date) - TO_DAYS(CURDATE()) <= ' . intval($days_before) . '
                    AND ct.cotation_status = \'PENDING\'
                    AND ct.dispatcher_fk = ' . $account_id ;
        
        $query = $this->db->query($strQuery); 
        $res = $query->row();
        
        return $res->count;
    }
    
    /**
     * Obtenir les dispatchs a traiter avant les n jours avant la date limite d'envoi (coté dispatcher)
     * 
     * @author Mitanjo
     * @param Id du compte utilisateur
     * @param L'index de début des enregitrements
     * @param Le nombre maximal d'enregistrements a retourner
     * @param Nombre de jours avant la date limite d'envoi
     */
    public function get_dispatchs_to_send_before_by_dispatcher_id($account_id, $start = 0, $count = 20, $days_before = 3)
    {
        $strQuery = 'SELECT * FROM ' . self::VIEW_RELANCE_COTATION . ' ct
                    WHERE TO_DAYS(ct.limit_send_date) - TO_DAYS(CURDATE()) <= ' . intval($days_before) . '
                    AND ct.cotation_status = \'PENDING\'
                    AND ct.dispatcher_fk = ' . $account_id . '
                    ORDER BY ct.limit_send_date DESC LIMIT ' . $start . ', ' . $count . ';';
        
        $res = $this->db->query($strQuery);
        
        return $res->result();
    }
    
    /**
     * Obenir les relances a partir du compte du commercial
     * 
     * @author Mitanjo
     */
    public function get_sended_cotations_to_check_by_commercial($commercial_id, $start = 0, $count = 20)
    {
        $strQuery = "SELECT *
                    FROM " . self::VIEW_RELANCE_COTATION . " as ct
                    WHERE CURDATE() BETWEEN ct.validity_start AND ct.validity_end
                    AND ct.commercial_fk = $commercial_id
                    AND ct.cotation_status = 'SENDED'
                    AND 
                    (
                        (
                            DATE_FORMAT(CURDATE(), '%w') BETWEEN 1 AND 5 AND 
                            CURDATE() IN (ct.date1, ct.date2, ct.date3)
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date1 AND DATE_ADD(ct.date1, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date2 AND DATE_ADD(ct.date2, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date3 AND DATE_ADD(ct.date3, INTERVAL 1 DAY)
                            )
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date1, INTERVAL 1 DAY) AND ct.date1
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date2, INTERVAL 1 DAY) AND ct.date2
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date3, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date3, INTERVAL 1 DAY) AND ct.date3
                            )
                        )
                        OR ct.duration < 14
                    ) LIMIT $start, $count;";
                    
        $res = $this->db->query($strQuery);
        
        return $res->result();
    }
    
    /**
     * Obtenir le nombre de cotations pour la relance à partir de l'id du commercial
     * 
     * @author Mitanjo
     */
    public function count_sended_cotations_to_check_by_commercial($commercial_id)
    {
        $strQuery = "SELECT COUNT(*) as count
                    FROM " . self::VIEW_RELANCE_COTATION . " as ct
                    WHERE CURDATE() BETWEEN ct.validity_start AND ct.validity_end
                    AND ct.commercial_fk = $commercial_id
                    AND ct.cotation_status = 'SENDED'
                    AND 
                    (
                        (
                            DATE_FORMAT(CURDATE(), '%w') BETWEEN 1 AND 5 AND 
                            CURDATE() IN (ct.date1, ct.date2, ct.date3)
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date1 AND DATE_ADD(ct.date1, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date2 AND DATE_ADD(ct.date2, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date3 AND DATE_ADD(ct.date3, INTERVAL 1 DAY)
                            )
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date1, INTERVAL 1 DAY) AND ct.date1
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date2, INTERVAL 1 DAY) AND ct.date2
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date3, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date3, INTERVAL 1 DAY) AND ct.date3
                            )
                        )
                        OR ct.duration < 14
                    );";
        
        $query = $this->db->query($strQuery); 
        $res = $query->row();
        
        return $res->count;
    }
    
    /**
     * Compter le nombre de dispatchs en attente à partir de l'id du commerciel
     * 
     * @author Mitanjo
     */
    public function count_pending_dispatchs_by_commercial_id($id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->where('commercial_fk', $id)
                        ->where('cotation_status', 'PENDING')
                        ->from(self::VIEW_RELANCE_COTATION)
                        ->get()
                        ->row();

        return $res->count;
    }
    
    /**
     * Obtenir les dispatchs en attente à partir de l'id du commercial
     * 
     * @author Mitanjo
     */
    public function get_pending_dispatchs_by_commercial_id($commercial_id, $sOrder = '', $start = 0, $count = 10)
    {
        $res = $this->db->select('*')
                    ->from(self::VIEW_RELANCE_COTATION)
                    ->where('commercial_fk', $commercial_id)
                    ->where('cotation_status', 'PENDING')
                    ->order_by($sOrder)
                    ->limit($count, $start)
                    ->get()
                    ->result();
                    
        return $res;
    }
    
    /**
     * Compter le nombre de dispatchs en attente à partir de l'id du dispatcher
     * 
     * @author Mitanjo
     */
    public function count_pending_dispatchs_by_dispatcher_id($id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->where('dispatcher_fk', $id)
                        ->where('cotation_status', 'PENDING')
                        ->from(self::VIEW_RELANCE_COTATION)
                        ->get()
                        ->row();

        return $res->count;
    }
    
    /**
     * Obtenir les dispatchs en attente à partir de l'id du commercial
     * 
     * @author Mitanjo
     */
    public function get_pending_dispatchs_by_dispatcher_id($dispatcher_id, $sOrder = '', $start = 0, $count = 10)
    {
        $res = $this->db->select('*')
                    ->from(self::VIEW_RELANCE_COTATION)
                    ->where('dispatcher_fk', $dispatcher_id)
                    ->where('cotation_status', 'PENDING')
                    ->order_by($sOrder)
                    ->limit($count, $start)
                    ->get()
                    ->result();
                    
        return $res;
    }
    ///
    
    /**
     * Obtenir les designations demandés à partir de l'id de la cotation
     * 
     * @author Mitanjo 
     */
    public function get_request_proforma_by_cotation_id($id, $count = 3)
    {
        $strQuery = 'SELECT * FROM ' . self::REQUEST_PROFORMA_TABLE . ' rq WHERE EXISTS
                    (SELECT ct.cotation_id FROM ' . self::TABLE_NAME . ' ct 
                    WHERE ct.cotation_id = rq.cotation_fk
                    AND ct.cotation_id = ' . $id . ') 
                    LIMIT ' . $count . ';';
        
        $query = $this->db->query($strQuery);
        
        return $query->result();
    }
    
    /**
     * Obtenir les designations proposés à partir de l'id de la cotation
     * 
     * @author Mitanjo 
     */
    public function get_proposition_proforma_by_cotation_id($id, $count = 3)
    {
        $strQuery = 'SELECT * FROM w_proposition_proforma rq WHERE EXISTS
                    (SELECT ct.cotation_id FROM ' . self::TABLE_NAME . ' ct 
                    WHERE ct.cotation_id = rq.cotation_fk
                    AND ct.cotation_id = ' . $id . ') 
                    LIMIT ' . $count . ';';
        
        $query = $this->db->query($strQuery);
        
        return $query->result();
    }
    
    /**
     * Récupérer une cotation à partir de son identification
     * 
     * @author Mitanjo
     */
    public function get_by_id($id)
    {
        $res = $this->db->select('*')
            ->from(self::TABLE_NAME)
            ->where('cotation_id', $id)
            ->get()
            ->row();

        return $res;
    }
    
    public function get_view_id($id)
    {
        $res = $this->db->select('*')
            ->from(self::VIEW_RELANCE_COTATION)
            ->where('cotation_id', $id)
            ->get()
            ->row();

        return $res;
    }
    
    /**
     * Mettre a jour le statut d'une cotation
     * 
     * @author Mitanjo
     */
    public function update_cotation_status($cotation_id, $status, $cotation_number = '')
    {
        $data = array(
            'cotation_status' => $status,
        );
        
        if( $cotation_number != '' )
            $data['cotation_number'] = $cotation_number;
        
        $this->db->where('cotation_id', $cotation_id);
        $this->db->update(self::TABLE_NAME, $data);
        
        //Remove the cotation from the queue
        $this->db->where('cotation_id', $cotation_id);
        $this->db->delete(self::TABLE_DISPATCH_QUEUE);
    }
    
    /**
     * Obtenir les dispatchs traités par le commercial (cotations dont le statut est SENDED) correspondant a un compte (account)
     * 
     * @author Mitanjo
     */
    public function get_sended_cotations_by_commercial_id($account_id, $sOrder = '', $start = 0, $count = 10)
    {
        $res = $this->db->select('*')
                    ->from(self::VIEW_RELANCE_COTATION)
                    ->where('commercial_fk', $account_id)
                    ->where('cotation_status', 'SENDED')
                    ->where('validity_end >= CURDATE()')
                    ->order_by($sOrder)
                    ->limit($count, $start)
                    ->get()
                    ->result();
                    
        return $res;
    }
    
    /**
     * Obtenir le nombre de dispatchs traités par le commercial (cotations dont le statut est PENDING) et associés a un compte
     * 
     * @author Mitanjo
     */
    public function count_sended_cotations_by_commercial_id($account_id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->where('commercial_fk', $account_id)
                        ->where('cotation_status', 'SENDED')
                        ->from(self::VIEW_RELANCE_COTATION)
                        ->get()
                        ->row();

        return $res->count;
    }

    /**
     * Obtenir les dispatchs traités par le commercial (cotations dont le statut est SENDED) correspondans a un compte (account)
     * a partir de l'id du dispatcher
     * 
     * @author Mitanjo
     */
    public function get_sended_cotations_by_dispatcher_id($account_id, $start = 0, $count = 10)
    {
        $res = $this->db->select('*')
                    ->from(self::VIEW_RELANCE_COTATION)
                    ->where('dispatcher_fk', $account_id)
                    ->where('cotation_status', 'SENDED')
                    ->where('validity_end >= CURDATE()')
                    ->limit($count, $start)
                    ->get()
                    ->result();
                    
        return $res;
    }

    /**
     * Compter le nombre de cotations envoyés a partir de l'id du dispatcher
     * 
     * @author Mitanjo
     */
    public function count_sended_cotations_by_dispatcher_id($account_id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->where('dispatcher_fk', $account_id)
                        ->where('cotation_status', 'SENDED')
                        ->from(self::VIEW_RELANCE_COTATION)
                        ->get()
                        ->row();

        return $res->count;
    }
    
    /**
     * Obenir les relances a partir du compte du commercial
     * 
     * @author Mitanjo
     */
    public function get_sended_cotations_to_check_by_dispatcher($dispatcher_id, $start = 0, $count = 20)
    {
        $strQuery = "SELECT *
                    FROM " . self::VIEW_RELANCE_COTATION . " as ct
                    WHERE CURDATE() BETWEEN ct.validity_start AND ct.validity_end
                    AND ct.dispatcher_fk = $dispatcher_id
                    AND ct.cotation_status = 'SENDED'
                    AND 
                    (
                        (
                            DATE_FORMAT(CURDATE(), '%w') BETWEEN 1 AND 5 AND 
                            CURDATE() IN (ct.date1, ct.date2, ct.date3)
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date1 AND DATE_ADD(ct.date1, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date2 AND DATE_ADD(ct.date2, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date3 AND DATE_ADD(ct.date3, INTERVAL 1 DAY)
                            )
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date1, INTERVAL 1 DAY) AND ct.date1
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date2, INTERVAL 1 DAY) AND ct.date2
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date3, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date3, INTERVAL 1 DAY) AND ct.date3
                            )
                        )
                        OR ct.duration < 14
                    ) LIMIT $start, $count;";
                    
        $res = $this->db->query($strQuery);
        
        return $res->result();
    }
    
    /**
     * Obtenir le nombre de cotations pour la relance à partir de l'id du commercial
     * 
     * @author Mitanjo
     */
    public function count_sended_cotations_to_check_by_dispatcher($dispatcher_id)
    {
        $strQuery = "SELECT COUNT(*) as count
                    FROM " . self::VIEW_RELANCE_COTATION . " as ct
                    WHERE CURDATE() BETWEEN ct.validity_start AND ct.validity_end
                    AND ct.dispatcher_fk = $dispatcher_id
                    AND ct.cotation_status = 'SENDED'
                    AND 
                    (
                        (
                            DATE_FORMAT(CURDATE(), '%w') BETWEEN 1 AND 5 AND 
                            CURDATE() IN (ct.date1, ct.date2, ct.date3)
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date1 AND DATE_ADD(ct.date1, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date2 AND DATE_ADD(ct.date2, INTERVAL 1 DAY)
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 0 AND
                                CURDATE() BETWEEN ct.date3 AND DATE_ADD(ct.date3, INTERVAL 1 DAY)
                            )
                        )
                        OR
                        (
                            (
                                DATE_FORMAT(ct.date1, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date1, INTERVAL 1 DAY) AND ct.date1
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date2, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date2, INTERVAL 1 DAY) AND ct.date2
                            )
                            OR
                            (
                                DATE_FORMAT(ct.date3, '%w') = 6 AND
                                CURDATE() BETWEEN DATE_SUB(ct.date3, INTERVAL 1 DAY) AND ct.date3
                            )
                        )
                        OR ct.duration < 14
                    );";
        
        $query = $this->db->query($strQuery); 
        $res = $query->row();
        
        return $res->count;
    }
    
    //Temporaires codes
    public function get_array_cotations_by($cotation_number = '', $cotation_date = '', $commercial_id = '', $cotation_status = '', $start = 0, $count = 20)
    {
        $this->db->select('*')
                 ->from(self::VIEW_RELANCE_COTATION);

        if( $cotation_number != '' )
        {
            $this->db->where('cotation_number', $cotation_number);
        }
        
        if( $cotation_date != '' )
        {
            $this->db->where('reception_date', $cotation_date);
        }
        
        if( $commercial_id != '' )
        {
            $this->db->where('commercial_fk', intval($commercial_id));
        }
        
        if( $cotation_status != '' )
        {
            $this->db->where('cotation_status', $cotation_status);
        }

        $res = $this->db->order_by('reception_date', 'desc')
                        ->limit($count, $start)
                        ->get()
                        ->result_array();
        
        return $res;
    }
    
    public function count_cotations_by($cotation_number = '', $cotation_date = '', $commercial_id = '', $cotation_status = '')
    {
        $this->db->select('*')
                 ->from(self::VIEW_RELANCE_COTATION);

        if( $cotation_number != '' )
        {
            $this->db->where('cotation_number', $cotation_number);
        }
        
        if( $cotation_date != '' )
        {
            $this->db->where('reception_date', $cotation_date);
        }
        
        if( $commercial_id != '' )
        {
            $this->db->where('commercial_fk', intval($commercial_id));
        }
        
        if( $cotation_status != '' )
        {
            $this->db->where('cotation_status', $cotation_status);
        }

        $res = $this->db->count_all_results();
        
        return $res;
    }
    
    //NEW SINCE 15/06/2013
    public function getSendedCotationsByCommercialId($accountId, $customer, $receptionDate, $start, $count)
    {
        $this->db->select('*')
                 ->from(self::VIEW_RELANCE_COTATION);

        if( ! empty($customer) )
        {
            $this->db->like('customer_name', $customer, 'after');
        }
        
        if( ! empty($receptionDate) )
        {
            $this->db->like('reception_date', $receptionDate, 'after');
        }

        $this->db->where('commercial_fk', $accountId);
        $this->db->where('cotation_status', 'ACCEPTED');

        $res = $this->db->limit($count, $start)
                        ->get()
                        ->result_array();
                    
        return $res;
    }
    
    public function countSendedCotationsByCommercialId($accountId, $customer, $receptionDate)
    {
        $this->db->select('COUNT(*) as count')
                 ->from(self::VIEW_RELANCE_COTATION);

        if( ! empty($customer) )
        {
            $this->db->like('customer_name', $customer, 'after');
        }
        
        if( ! empty($receptionDate) )
        {
            $this->db->like('reception_date', $receptionDate, 'after');
        }

        $this->db->where('commercial_fk', $accountId);
        $this->db->where('cotation_status', 'ACCEPTED');
        
        $res = $this->db->get()
                        ->row();

        return $res->count;
    }
    
    /**
     * Compter le nombre de nouveaux dispatchs à partir de l'id du commerciel
     * 
     * @author Mitanjo
     */
    public function count_new_dispatchs_by_commercial_id($id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->where('account_id', $id)
                        ->from(self::TABLE_DISPATCH_QUEUE)
                        ->get()
                        ->row();

        return $res->count;
    }

    /**
     * Obtenir les nouveaux dispatchs à partir de l'id du commercial
     * 
     * @author Mitanjo
     */
    public function get_new_dispatchs_by_commercial_id($commercial_id, $sOrder = '', $start = 0, $count = 10)
    {
        $queue = $this->db->select('*')
                          ->from(self::TABLE_DISPATCH_QUEUE)
                          ->where('account_id', $commercial_id)
                          ->get()
                          ->result();

        $newDispatchs = array(); 
        foreach( $queue as $q )
        {
            $newDispatchs[] = $this->db->select('*')
                                     ->from(self::VIEW_RELANCE_COTATION)
                                     ->where('cotation_id', $q->cotation_id)
                                     ->where('commercial_fk', $commercial_id)
                                     ->where('cotation_status', 'PENDING')
                                     ->get()
                                     ->row();
        }
        
        return $newDispatchs;
    }
}