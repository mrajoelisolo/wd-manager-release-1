<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_Proforma_Model extends CI_Model
{
    const TABLE_NAME = 'w_request_proforma';
    
    public function save($reference, $designation, $qty, $cotation_fk)
    {
        $this->db->set('ref', $reference);
        $this->db->set('designation', $designation);
        $this->db->set('qty', intval($qty));
        $this->db->set('cotation_fk', intval($cotation_fk));
        $this->db->insert(self::TABLE_NAME);
        
        return $this->db->insert_id();
    }
    
    public function fetch_array_by_cotation_id($id)
    {
        $res = $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('cotation_fk', $id)
                        ->get()
                        ->result_array();
                        
        return $res;
    }
}