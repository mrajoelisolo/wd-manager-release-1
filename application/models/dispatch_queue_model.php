<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dispatch_Queue_Model extends CI_Model
{
    const TABLE_NAME = 'w_dispatch_queue';
    const COTATION_TABLE_NAME = 'w_cotation';

    public function count_dispatchs_queue($account_id)
    {
        $strQuery = 'SELECT COUNT(*) AS count FROM ' . self::COTATION_TABLE_NAME . ' ct
                     WHERE ct.cotation_id IN
                    (SELECT q.cotation_id FROM ' . self::TABLE_NAME . ' q WHERE q.account_id = ' . $account_id . ');';
        $res = $this->db->query($strQuery)
                        ->row();
        
        return $res->count;
    }
    
    public function flush_dispatchs_queue($account_id)
    {
        $this->db->where('account_id', $account_id);
        $this->db->delete(self::TABLE_NAME);
    }
}