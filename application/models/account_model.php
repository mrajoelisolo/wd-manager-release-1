<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class Account_Model extends CI_Model {
    const TABLE_NAME = 'w_account';
    
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Uset to authentify wide users
     */
    public function authentify($login, $pwd) {
        $res = $this->db->select('*')
                ->from(self::TABLE_NAME)
                ->where('login', $login)
                ->where('pwd', sha1($pwd))
                ->count_all_results();

		return ($res > 0);
    }
    
    /**
     * Used to subscribe users to wide
     */
    public function subscribe($login, $pwd, $first_name, $last_name, $user_type, $acronym) {
        $this->db->set('login', $login)
                 ->set('pwd', sha1($pwd))
                 ->set('first_name', $first_name)
                 ->set('last_name', $last_name)
                 ->set('user_type', $user_type)
                 ->set('acronym', $acronym);
        
        $this->db->insert(self::TABLE_NAME);
    }
    
    /**
     * Get user details from his login
     */
    public function get_by_login($login, $pwd) {
        return $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('login', $login)
                        ->where('pwd', sha1($pwd))
                        ->get()
                        ->row();
    }
    
    /**
     * Get user account by his id
     */
    public function get_by_id($account_id)
    {
        return $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('account_id', $account_id)
                        ->get()
                        ->row();
    }
    
    /**
     * Not used
     */
    public function query($where, $order, $limit) {
        $this->db->query("SELECT SQL_CALC_FOUND_ROWS ");
    }
    
    /**
     * Get a list of object of commercials (Account)
     */
    public function getCommercials($start = 0, $count = 10)
    {
        return $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('user_type', 'COMMERCIAL')
                        ->get()
                        ->result();
    }
    
    /**
     * Count commercial count
     */
    public function countCommercials()
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->from(self::TABLE_NAME)
                        ->where('user_type', 'COMMERCIAL')
                        ->get()
                        ->row();
        
        return $res->count;
    }
    
    /**
     * Get the list of the commercials except one
     */
    public function getCommercialsExcept($id, $start = 0, $count = 10)
    {
        return $this->db->select('*')
                        ->from(self::TABLE_NAME)
                        ->where('user_type', 'COMMERCIAL')
                        ->where('account_id <>', $id)
                        ->limit($count, $start)
                        ->get()
                        ->result();
    }
    
    /**
     * Count the number of the commercials - 1 TO DO can be optimized
     */
    public function countCommercialsExcept($id)
    {
        $res = $this->db->select('COUNT(*) as count')
                        ->from(self::TABLE_NAME)
                        ->where('user_type', 'COMMERCIAL')
                        ->where('account_id <>', $id)
                        ->get()
                        ->row();

        return $res->count;
    }
    
    //Since 17-06-2013
    public function getAccounts($firstName, $lastName, $start, $count)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME);
        
        if( ! empty($firstName) )
        {
            $this->db->like('first_name', $firstName, 'after');
        }
        
        if( ! empty($lastName) )
        {
            $this->db->like('last_name', $lastName, 'after');
        }
        
        $this->db->not_like('user_type', 'ADMIN');
        
        $res = $this->db->limit($count, $start)
                    ->get()
                    ->result();
        return $res;
    }
    
    public function countAccounts($firstName, $lastName)
    {
        $this->db->select('*')
                 ->from(self::TABLE_NAME);
        
        if( ! empty($firstName) )
        {
            $this->db->like('first_name', $firstName, 'after');
        }
        
        if( ! empty($lastName) )
        {
            $this->db->like('last_name', $lastName, 'after');
        }

        $this->db->not_like('user_type', 'ADMIN');

        $res = $this->db->count_all_results();
        
        return $res;
    }
    
    public function updateAccount($accountId, $login, $pwd, $firstName, $lastName, $userType, $acronym)
    {
        $data = array(
            'login' => $login,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'user_type' => $userType,
            'acronym' => $acronym,
        );

        if( ! empty($pwd) )
        {
            $data['pwd'] = sha1($pwd);
        }

        $this->db->where('account_id', $accountId);
        $this->db->update(self::TABLE_NAME, $data);
    }
}