<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('css_url'))
{
	function css_url($filename)
	{	
		return path_css().$filename.'.css';
	}
}

if (! function_exists('js_url'))
{
	function js_url($filename)
	{
		return path_js() . $filename . '.js';
	}
}

if (! function_exists('img_url'))
{
	function img_url($filename)
	{
		return path_img() . $filename;
	}
}

if (! function_exists('sound_url'))
{
	function sound_url($filename)
	{
		return path_sound() . $filename;
	}
}

/* Path functions */

if (! function_exists('path_css') )
{
    function path_css()
    {
        return base_url() . 'assets/css/';
    }
}

if (! function_exists('path_js') )
{
    function path_js()
    {
        return base_url() . 'assets/script/';
    }
}

if (! function_exists('path_img') )
{
    function path_img()
    {
        return base_url() . 'assets/images/';
    }
}

if (! function_exists('path_sound') )
{
    function path_sound()
    {
        return base_url() . 'assets/sounds/';
    }
}

if (! function_exists('assets_paths') )
{
    function assets_paths()
    {
        $res = array(
            'base_url' => base_url() ,
            'path_js' => path_js(),
            'path_img' => path_img(),
            'path_css' => path_css(),
            'path_sound' => path_sound(),
        );
        
        return $res;
    }
}

if (! function_exists('copyright')  )
{
    function copyright()
    {
        return 'Wide manager - Copyright &#169; 2013 - Myrmidon Software Engineering';
    }
}