<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mysqldate_to_str'))
{
	function mysqldate_to_str($strDate)
	{	
        if( empty($strDate) ) return '';
        
		$arr = explode('-', $strDate);
        
        return $arr[2] . '/' . $arr[1] . '/' . $arr[0]; 
	}
}

if( ! function_exists('str_to_mysqldate') )
{
    function str_to_mysqldate($strDate, $separator = '/')
    {
        if( empty($strDate) ) return '';
        
        $arr = explode($separator, $strDate);
        
        $res = $arr[2] . '-' . $arr[1] . '-' . $arr[0]; 
        
        return $res;
    }
}

if( ! function_exists('mysqldatetime_to_str') )
{
    function mysqldatetime_to_str($mysqlDate)
    {
        if( empty($mysqlDate) ) return '';
        
        $arr = explode(' ', $mysqlDate);
        $time = $arr[1];
        
        $arr = explode(':', $time);
        $h = $arr[0];
        $m = $arr[1];
        
        return $h . ':' . $m;
    }
}