<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('leading_format'))
{
    function leading_format($value)
    {
    	$l = strlen("" . $value);
    
    	$strLead = '';
    	$leadCount = 3;
    	
    	for($i = 0; $i < $leadCount - $l; $i++)
    	{
    		$strLead .= '0';
    	}
    	
    	$res = $strLead . $value;
    	
    	return $res;
    }
}