<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('fpdf.php');

class WideProformaReport extends FPDF
{   
    const ROWS_PER_PAGE = 21;
    const CURRENCY_LANG = 'fr';
    const CURRENCY = 'ARIARY';
    
	public function Header()
	{
	   //src, x, y, wdt
		$this->Image('./assets/images/reporting/wide_header.png', 5, 5, 90);
        $this->Image('./assets/images/reporting/wide_topframe.png', 0, 0, 210);
        $this->Image('./assets/images/reporting/wide_bottomframe.png', 0, 219, 210);
	}
	
    private function fetchPropositions($params, $totalRows, $start, $count)
    {
        $res = array();

        //var_dump($params);
        //var_dump($start);

        if( $count > $totalRows ) $count = $totalRows;

        for( $i = 0; $i < $count; $i++ )
        {
            $j = $i + $start;
            
            if( $j < $totalRows )
            {
                $res[] = array(
                    $params['arr_ref'][$j],
                    $params['arr_designation'][$j],
                    $params['arr_qty'][$j],
                    $params['arr_pu'][$j],
                    $params['arr_qty'][$j] * $params['arr_pu'][$j],
                );
            }
        }
        
        return $res;
    }
    
    public function initContent($params)
    {
        $digitToLetterConvertor = $params['lib_digitToLetterConvertor'];
        
        $totalRows = count($params['arr_designation']);
        
        //var_dump($params['arr_designation']);
        
        if( $totalRows <= self::ROWS_PER_PAGE )
        {
            $pageCount = 1;
        }
        else
        {
            $pageCount = intval($totalRows / self::ROWS_PER_PAGE);
            
            if( $totalRows % self::ROWS_PER_PAGE > 0 )
            {
                $pageCount += 1;
            }
        }
        
        for( $iPage = 0; $iPage < $pageCount; $iPage++ )
        {
            $this->AddPage();
            
            $startRow = $iPage * self::ROWS_PER_PAGE;
            $currentPage = $iPage + 1;
            $propositions = $this->fetchPropositions($params, $totalRows, $startRow, self::ROWS_PER_PAGE);
            //var_dump($propositions); 
            
            $this->createCell(140, 5, 30, 9, 'QUOTE N°', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
                'fontColor' => array(
                    'red' => 0,
                    'green' => 0,
                    'blue' => 0,
                ),
            ));
            
            $this->createCell(170, 5, 37, 9, $params['var_quot_number'], array( //$proforma->cotation_number
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), 'C');
            
            $this->createCell(140, 15, 30, 9, 'Date', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 10,
            ));
            
            $this->createCell(140, 25, 30, 9, $params['var_proforma_date'], array( //$params['var_proforma_date']
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ));
            
            $this->createCell(170, 15, 37, 9, 'Page', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 10,
            ),
            'C');
            
            $this->createCell(170, 25, 37, 9, 'Page ' . $currentPage . '/' . $pageCount, array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ),
            'C');
            
            $this->createCell(0, 54, 30, 4, 'Reqst for Quotation', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            /* Cotation source field */
            $cotation_source = $params['var_cotation_source'];
            $this->createCell(30, 54, 60, 4, '           -              -        -', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            $isFromPhone = '';
            if( strtoupper($cotation_source) == 'PHONE' )
            {
                $isFromPhone = 'U';
            }
            $this->createCell(30, 54, 15, 4, 'Phone', array(
                'fontName' => 'Arial',
                'fontStyle' => $isFromPhone,
                'fontSize' => 8,
            ), '', 0);
            
            $isFromPhysical = '';
            if( strtoupper($cotation_source) == 'PHYSICAL' )
            {
                $isFromPhysical = 'U';
            }
            $this->createCell(40, 54, 15, 4, 'Physical', array(
                'fontName' => 'Arial',
                'fontStyle' => $isFromPhysical,
                'fontSize' => 8,
            ), '', 0);
            
            $isFromMail = '';
            if( strtoupper($cotation_source) == 'MAIL' )
            {
                $isFromMail = 'U';
            }
            $this->createCell(52, 54, 15, 4, 'Mail', array(
                'fontName' => 'Arial',
                'fontStyle' => $isFromMail,
                'fontSize' => 8,
            ), '', 0);
            
            $isFromOther = '';
            if( strtoupper($cotation_source) == 'OTHER' )
            {
                $isFromOther = 'U';
            }
            $this->createCell(59, 54, 15, 4, 'Other', array(
                'fontName' => 'Arial',
                'fontStyle' => $isFromOther,
                'fontSize' => 8,
            ), '', 0);
            /* End of cotation source field */
            
            $this->createCell(0, 58, 30, 4, 'Rsble Quotation', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            $this->createCell(30, 58, 60, 4, $params['var_customer_fullname'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            $this->createCell(140, 58, 67, 4, $params['var_customer'], array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 12,
            ), 'C', 0);
            
            $this->createCell(140, 67, 67, 4, 'Madagascar', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 12,
            ), 'C', 0);
            
            //Setting the table
            $tableX = 0;
            $tableY = 80;
            $this->SetFont('Arial', '', 8);
            
            $this->SetXY($tableX, $tableY);
            $header = array(
                'Reference',
                'Designation',
                utf8_decode('Qty'), 
                'P.U. H.T. in ' . $params['var_short_unit'],
                'Amount H.T en ' . $params['var_short_unit'],
            );
            $w = array(
                30,
                98,
                12,
                30,
                37
            );
            
            for( $i = 0; $i < count($header); $i++ )
            {
                $this->SetFillColor(224, 224, 224);
                $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
            }
            
            $this->Ln();
            $this->SetX($tableX);
    
            $data = array();
            $totalAmount = $params['var_net_paid'];
            
            foreach( $propositions as $proposition )
            {
                $data[] = array(
                    $proposition[0],
                    $proposition[1],
                    $proposition[2],
                    $proposition[3],
                    $proposition[4], 
                );
            }
            
            $strTotalAmount = strtoupper($digitToLetterConvertor->toWords($totalAmount, self::CURRENCY_LANG));
    
            foreach( $data as $row )
            {
                $this->Cell($w[0], 6, utf8_decode($row[0]), 'LR');
                $this->Cell($w[1], 6, utf8_decode($row[1]), 'LR');
                $this->Cell($w[2], 6, number_format($row[2], 0, ',', ' '), 'LR', 0, 'R');
                $this->Cell($w[3], 6, number_format($row[3], 0, ',', ' '), 'LR', 0, 'R');
                $this->Cell($w[4], 6, number_format($row[4], 2, ',', ' '), 'LR', 0, 'R');
                $this->Ln();
                $this->SetX($tableX);
            }
            
            //Definit la hauteur minimum de la table
            $rowCount = count($data);
            $emtpyRowCount = 20 - $rowCount;
            for($i = 0; $i < $emtpyRowCount; $i++)
            {
                $this->Cell($w[0], 6, '', 'LR');
                $this->Cell($w[1], 6, '', 'LR');
                $this->Cell($w[2], 6, '', 'LR');
                $this->Cell($w[3], 6, '', 'LR');
                $this->Cell($w[4], 6, '', 'LR');
                $this->Ln();
                $this->SetX($tableX);
            }
    
            $this->Cell(array_sum($w), 0, '', 'T');
            
            $this->createCell(0, 220, 90, 4, 'CONDITIONS OF PAYMENT', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), 'C', 0);
    
            $this->createCell(0, 224, 30, 6, 'MODE', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(30, 224, 60, 6, $params['var_payment_mode'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(0, 230, 30, 8, 'DELAI', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(30, 230, 60, 8, $params['var_payment_delay'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(0, 238, 30, 4, 'VTY OF QTAT', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            $this->createCell(30, 238, 60, 4, $params['var_cotation_validity'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(0, 242, 30, 4, 'DELIVERY', array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(30, 242, 60, 4, $params['var_cotation_delivery'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
    
            $this->createCell(90, 220, 50, 4, 'BASES EXLUDE VAT', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), 'C', 0);
            
            $this->createNumericCell(90, 224, 50, 6, $params['var_base_exclude_vat'], array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
                'decimalCount' => 2,
            ), 'C', 0);
            
            $this->createCell(140, 220, 30, 4, 'VAT 20%', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), 'C', 0);
            
            $this->createNumericCell(140, 224, 30, 6, $params['var_vat'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
                'decimalCount' => 2,
            ), 'C', 0);
            
            $this->createCell(170, 220, 40, 4, 'MTTC', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), 'C', 0);
    
            $this->createNumericCell(170, 224, 40, 6, $params['var_mttc'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
                'decimalCount' => 2,
            ), 'C', 0);
            
            $this->createCell(90, 238, 50, 8, 'NET PAID:', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), 'C', 0);
            
            $this->createNumericCell(140, 238, 30, 8, $totalAmount, array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
                'decimalCount' => 2,
            ), 'C', 0);
            
            $this->createCell(170, 238, 40, 8, $params['var_short_unit'], array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            $this->createCell(0, 250, 210, 4, 'Arrétée la presente facture à la somme de: ' . $strTotalAmount . ' ' . self::CURRENCY, array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 8,
            ), '', 0);
            
            /*
            $this->createCell(0, 254, 90, 4, 'ARIARY', array(
                'fontName' => 'Arial',
                'fontStyle' => 'B',
                'fontSize' => 8,
            ), '', 0);
            */
            
            $this->createMultiCell(0, 258, 210, 4, "Clauses de réserve de propriété: Le vendeur conserve la propriété pleine et entière des marchandises vendues jusqu'au paiement complet du prix, en application de la loi du 12 Mai 1980. Tout différend quel qu'il soit et quelles que soient les conditions de ventes, d'exécution des commandes ou les modes de paiement acceptés est de la compétence exclusive du Tribunal de commerce du Siège Social de Madagascar. En cas de retard de paiement, les pénalités doivent être calculées au taux d'intérêt légal majoré de 1,5% au prorata du nombre de jours de retard par rapport à l'échéance de la facture.", array(
                'fontName' => 'Arial',
                'fontStyle' => '',
                'fontSize' => 7,
                'fontColor' => array(
                    'red' => 122,
                    'green' => 122,
                    'blue' => 122,
                ),
            ), 'C', 0);
        }
    }
    
	public function Footer()
	{
	   $this->Image('./assets/images/reporting/wide_footer.png', 0, 280, 210);
	}
    
    private function createCell($x, $y, $wdt, $hgt, $txt, $options = array(), $justify = '', $border = 0, $fill = false, $r = 255, $g = 255, $b = 255)
    {
        $fontName  = (isset($options['fontName'])) ? $options['fontName'] : 'Arial';
        $fontStyle = (isset($options['fontStyle'])) ? $options['fontStyle'] : '';
        $fontSize  = (isset($options['fontSize'])) ? $options['fontSize'] : 8;
        $fontColor = (isset($options['fontColor'])) ? $options['fontColor'] : array(
            'red' => 0,
            'green' => 0,
            'blue' => 0,
        );
        
        $this->SetFont($fontName, $fontStyle, $fontSize);
        $this->SetTextColor($fontColor['red'], $fontColor['green'], $fontColor['blue']);
        $this->SetXY($x, $y);
        //$this->SetDrawColor(0, 0, 0);
        $this->SetFillColor($r, $g, $b);
        $this->Cell($wdt, $hgt, utf8_decode($txt), $border, 1, $justify, $fill);
    }
    
    private function createNumericCell($x, $y, $wdt, $hgt, $value, $options = array(), $justify = '', $border = 0)
    {
        $fontName  = (isset($options['fontName'])) ? $options['fontName'] : 'Arial';
        $fontStyle = (isset($options['fontStyle'])) ? $options['fontStyle'] : '';
        $fontSize  = (isset($options['fontSize'])) ? $options['fontSize'] : 8;
        $decimalCount      = (isset($options['decimalCount'])) ? $options['decimalCount'] : 2;
        $decimalSeparator  = (isset($options['decimalSeparator'])) ? $options['decimalSeparator'] : ',';
        $fontColor = (isset($options['fontColor'])) ? $options['fontColor'] : array(
            'red' => 0,
            'green' => 0,
            'blue' => 0,
        );
        
        $this->SetFont($fontName, $fontStyle, $fontSize);
        $this->SetTextColor($fontColor['red'], $fontColor['green'], $fontColor['blue']);
        $this->SetXY($x, $y);
        $this->Cell($wdt, $hgt, number_format($value, $decimalCount, $decimalSeparator, ' '), $border, 1, $justify);
    }
    
    private function createMultiCell($x, $y, $wdt, $hgt, $txt, $options = array(), $justify = '', $border = 0)
    {
        $fontName  = (isset($options['fontName'])) ? $options['fontName'] : 'Arial';
        $fontStyle = (isset($options['fontStyle'])) ? $options['fontStyle'] : '';
        $fontSize  = (isset($options['fontSize'])) ? $options['fontSize'] : 8;
        $fontColor = (isset($options['fontColor'])) ? $options['fontColor'] : array(
            'red' => 0,
            'green' => 0,
            'blue' => 0,
        );
        
        $this->SetFont($fontName, $fontStyle, $fontSize);
        $this->SetTextColor($fontColor['red'], $fontColor['green'], $fontColor['blue']);
        $this->SetXY($x, $y);
        $this->MultiCell($wdt, $hgt, utf8_decode($txt), $border, $justify);
    }
}