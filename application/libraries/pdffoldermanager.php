<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PdfFolderManager
{
    private $months = array(
        'Janvier',
        'Fevrier',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Aout',
        'Septembre',
        'Octobre',
        'Novembre',
        'Decembre',
    );
    
    private $basePath = '';
    private $curDate = '';
    
    public function initialize($value)
    {
        $this->curDate = $value['cur_date'];
        $this->basePath = $value['base_path'];
    }
    
    public function initFolderPath()
    {
        $arrDateElt = explode('/', $this->curDate);
        $day = $arrDateElt[0];
        $month = $arrDateElt[1];
        $year = $arrDateElt[2];
        
        $path = $this->basePath. '/' .$year . '/';
        if( ! file_exists($path) )
        {
            mkdir($path);
        }
        
        $path = $this->basePath. '/' .$year . '/' . $this->months[$month - 1];
        if( ! file_exists($path) )
        {
            mkdir($path);
        }
        
        return $path . '/';
    }
}

/* End of file PdfFolderManager.php */