<?php

/**
 * @author Mitanjo
 * @copyright 2012
 */

class PageNotFound extends CI_Controller {
    public function index()
    {
        $data = assets_paths();
        
        $this->parser->parse('pagenotfound', $data);
    }
}

?>