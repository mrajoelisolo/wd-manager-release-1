<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscribe extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('account_model');
    }
    
    public function index()
    {
        //Setting the main folder
        $folderName = 'admin/subscribe/';
        
        //Resources
        $cssResources = array(
            array('var_resource' => css_url('style')),
            array('var_resource' => css_url('modal')),
            array('var_resource' => css_url('datatable')),
            array('var_resource' => css_url('ui/jquery.ui.all')),
        );
            
        $jsResources = array(
            array('var_resource' => js_url('jquery.min')),
            array('var_resource' => js_url('ui/jquery-ui')),
            array('var_resource' => js_url('ui/jquery.ui.core')),
            array('var_resource' => js_url('ui/jquery.ui.widget')),
            array('var_resource' => js_url('ui/jquery.ui.button')),
            array('var_resource' => js_url('ui/jquery.ui.mouse')),
            array('var_resource' => js_url('ui/jquery.ui.draggable')),
            array('var_resource' => js_url('ui/jquery.ui.position')),
            array('var_resource' => js_url('ui/jquery.ui.dialog')),
            array('var_resource' => js_url('ui/jquery.ui.datepicker')),
            array('var_resource' => js_url('jquery.simplemodal-1.4.3.min')),
            array('var_resource' => js_url('jquery.dataTables.min')),
            array('var_resource' => js_url('wide-script')),
        );
        
        //Literal js, a literal javascript content
        $literalJs = array(
            array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
            array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript', assets_paths(), TRUE)),
        );
        
        $hData = assets_paths();
        $cData = assets_paths();
        $fData = assets_paths();
        
        $fData = array_merge($fData, array(
            'copyright' => copyright(),
        ));
        
        $headerZone  = $this->parser->parse($folderName . 'zones/headerzone', $hData, TRUE);
        $contentZone = $this->parser->parse($folderName . 'zones/contentzone', $cData, TRUE);
        $footerZone  = $this->parser->parse('footerzone', $fData, TRUE);
        
        $data = assets_paths();
        $data = array_merge($data, array(
            'page_title'      => 'Wide manager - Inscription d\'une nouvelle compte utilisateur',
            'css_resources'   => $cssResources,
            'js_resources'    => $jsResources,
            'js_literals'     => $literalJs,
            'header_zone'     => $headerZone,
            'content_zone'    => $contentZone,
            'footer_zone'     => $footerZone,
        ));
            
        $this->parser->parse('layout_admin.tpl', $data);
    }
}