<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller
{
    const ITEMS_PER_PAGE = 10;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('account_model');
    }

    public function index()
    {
        $this->page();
    }

    public function page()
    {
        if( $this->session_model->checkIfSessionActive('ADMIN') )
        {
            /* Setting URI segments */
            $params = $this->uri->uri_to_assoc(4);

            $f_last_name  = ( isset($params['nom'])) ? $params['nom'] : '';
            $f_first_name = ( isset($params['prenom'])) ? $params['prenom'] : '';
            
            $start = ( isset($params['start'])) ? $params['start'] : 0;
            if( ! $start )
                $start = 0;
    
            $segments = '';
            if( trim($f_last_name) != '' )
            {
                $segments .= 'nom/' . $f_last_name;
            }
            
            if( trim($f_first_name) != '' )
            {
                $segments .= 'prenom/' . $f_first_name;
                
                if( $segments != '' ) $segments .= '/';
            }
            /* Endof setting URI segments */
        
            //Setting the main folder
            $pageTitle = 'Wide manager - Inscription d\'une nouvelle compte utilisateur';
            $folderName = 'admin/accounts/';
            
            //Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('modal')),
                array('var_resource' => css_url('datatable')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
            );
                
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('jquery.simplemodal-1.4.3.min')),
                array('var_resource' => js_url('jquery.dataTables.min')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal js, a literal javascript content
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript.tpl', assets_paths(), TRUE)),
            );
    
            /* Setting data */
            $var_accounts = $this->account_model->getAccounts($f_first_name, $f_last_name, $start, self::ITEMS_PER_PAGE);
            $totalRows = $this->account_model->countAccounts($f_first_name, $f_last_name);
            
            $is_pagination = array();
            if( $totalRows > self::ITEMS_PER_PAGE )
            {
                $is_pagination[] = array(1);
            }
        
            $config = array(
                'base_url' => base_url() . 'admin/accounts/page/' . $segments . 'start/',
                'total_rows' => $totalRows,
                'per_page' => self::ITEMS_PER_PAGE,
                'first_link' => 'D&eacute;but',
                'last_link' => 'Fin',
                'next_link' => '&gt;',
                'prev_link' => '&lt;',
                'cur_tag_open' => '&nbsp;<strong class="inactive_anchor">',
                'cur_tag_close' => '</strong>',
                'cur_page' => $start,
            );
            $this->pagination->initialize($config);
            $var_pagination = $this->pagination->create_links();
            /* End setting data */
    
            $hData = array_merge(assets_paths(), array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            $cData = array_merge(assets_paths(), array(
                'var_accounts' => $var_accounts,
                'var_pagination' => $var_pagination,
                'is_pagination' => $is_pagination,
            ));
            
            $fData = array_merge(assets_paths(), array(
                'copyright' => copyright(),
            ));
            
            $headerZone  = $this->parser->parse($folderName . 'zones/headerzone.tpl', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone.tpl', $cData, TRUE);
            $footerZone  = $this->parser->parse('footerzone', $fData, TRUE);
            
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title'      => $pageTitle,
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
                
            $this->parser->parse('layout_admin.tpl', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
}