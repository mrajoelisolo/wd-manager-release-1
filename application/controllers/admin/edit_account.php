<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_Account extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('account_model');
    }

    public function by_id($accountId)
    {
        if( $this->session_model->checkIfSessionActive('ADMIN') )
        {
            //Setting the main folder
            $pageTitle = 'Wide manager - Edition de compte utilisateur';
            $folderName = 'admin/edit_account/';
            
            //Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('modal')),
                array('var_resource' => css_url('datatable')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
            );
                
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('jquery.simplemodal-1.4.3.min')),
                array('var_resource' => js_url('jquery.dataTables.min')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal js, a literal javascript content
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript.tpl', assets_paths(), TRUE)),
            );

            // Setting data
            $oAccount = $this->account_model->get_by_id($accountId);
            
            $var_account_id = $oAccount->account_id;
            $var_login = $oAccount->login;
            $var_pwd = $oAccount->pwd;
            $var_last_name = $oAccount->last_name;
            $var_first_name = $oAccount->first_name;
            $var_acronym = $oAccount->acronym;
            
            $is_dispatcher = 'selected="selected"';
            $is_commercial = '';
            
            if( $oAccount->user_type == 'COMMERCIAL' )
            {
                $is_dispatcher = '';
                $is_commercial = 'selected="selected"';
            }
            // End setting data

            $hData = array_merge(assets_paths(), array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            $cData = array_merge(assets_paths(), array(
                'var_account_id' => $var_account_id,
                'var_login'      => $var_login,
                'var_last_name'  => $var_last_name,
                'var_first_name' => $var_first_name,
                'var_acronym'    => $var_acronym,
                'is_dispatcher'  => $is_dispatcher,
                'is_commercial'  => $is_commercial,
            ));
            
            $fData = array_merge(assets_paths(), array(
                'copyright' => copyright(),
            ));
            
            $headerZone  = $this->parser->parse($folderName . 'zones/headerzone.tpl', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone.tpl', $cData, TRUE);
            $footerZone  = $this->parser->parse('footerzone', $fData, TRUE);
            
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title'      => $pageTitle,
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
                
            $this->parser->parse('layout_admin.tpl', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
}