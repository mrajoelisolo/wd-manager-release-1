<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AccountServices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('account_model');
    }

    public function create()
    {
        $login = $this->input->post('txtLogin');
        $pwd = $this->input->post('txtPassword');
        $firstName = $this->input->post('txtFirstName');
        $lastName = $this->input->post('txtLastName');
        $userType = $this->input->post('cmbUserType');
        $acronym = $this->input->post('txtAcronym');
        
        $this->account_model->subscribe($login, $pwd, $firstName, $lastName, $userType, $acronym);
        
        $arr = array(
            'status' => 1,
        );
        
        echo json_encode($arr);
    }
    
    public function update()
    {
        $accountId = $this->input->post('txtAccountId');
        $login = $this->input->post('txtLogin');
        $pwd = $this->input->post('txtPassword');
        $firstName = $this->input->post('txtFirstName');
        $lastName = $this->input->post('txtLastName');
        $userType = $this->input->post('cmbUserType');
        $acronym = $this->input->post('txtAcronym');

        $this->account_model->updateAccount($accountId, $login, $pwd, $firstName, $lastName, $userType, $acronym);

        $arr = array(
            'status' => 1,
        );

        echo json_encode($arr);
    }
}