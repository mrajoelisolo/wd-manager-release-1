<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */
 
class CotationReporting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('cotation_model');
        $this->load->model('proposition_proforma_model');
        
        $this->load->library('numbers_words');
        $this->load->library('pagination');
        
		$this->load->model('cotation_model');
        $this->load->model('proposition_proforma_model');
        $this->load->model('account_model');
        
        $this->load->library('wideproformareport', array(
			'orientation' => 'P',
			'unit' => 'mm',
			'size' => 'A4',
		));
    }
    
    public function edit($proforma_id)
    {
        if( $this->session_model->checkIfSessionActive('COMMERCIAL') )
        {
            $folderName = 'commercial/cotationreporting/';
            
            //CSS Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
            );
            
            //JS resources
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('autocomplete.jquery')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal JS
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript', assets_paths(), TRUE)),
            );
            
            $hData = assets_paths();
            $cData = assets_paths();
            $fData = assets_paths();
            
            $hData = array_merge($hData, array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            //Setting content data
            $cotation = $this->cotation_model->get_view_id($proforma_id);
            $var_designations = $this->proposition_proforma_model->fetch_array_view_by_cotation_id($proforma_id);
            $totalAmount = $this->proposition_proforma_model->get_total_amount($proforma_id);
            $vat = 0.2;
            
            $cData = array_merge($cData, array(
                'var_cotation_number' => $cotation->cotation_number,
                'var_customer' => $cotation->customer_name,
                'var_cotation_source' => $cotation->cotation_source,
                'var_commercial_fullname' => $cotation->commercial_lastname . ' ' . $cotation->commercial_firstname,
                'var_designations' => $var_designations,
                
                'var_bases_exclude_vat' => $totalAmount,
                'var_vat_20p' => $vat * $totalAmount,
                'var_mttc' => $totalAmount + $vat * $totalAmount,
                'var_net_paid' => $totalAmount + $vat * $totalAmount,
                
                'var_payment_mode' => 'BANK TRANSFER/ CHEQUE',
                'var_payment_delai' => '50 % to order / 50 % to delivery',
                'var_cotation_validity' => 'fifteen (15) days',
                'var_cotation_delivery' => 'five (5) weeks after order',
            ));
            
            //Setting footer data
            $fData = array_merge($fData, array(
                'copyright' => copyright(),
            ));
            //End of setting footer data
            
            $headerZone = $this->parser->parse($folderName . 'zones/headerzone', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone', $cData, TRUE);
            $footerZone = $this->parser->parse('footerzone', $fData, TRUE);
            
            //Setting the layout
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title' => 'Wide manager - Reporting de proforma',
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
            
            $this->parser->parse('layout', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
    
    public function topdf()
    {
        $post = $this->input->post();
        
        $account_id = $this->session->userdata('account_id');
        $oAccount = $this->account_model->get_by_id($account_id);
        
        $cotationNumber = $this->input->post('txtCotationNumber');
        $strdate = str_replace('/', '_', $post['txtCotationDate']);
        $outputFile = 'prf_' . $cotationNumber . '_' . $post['txtCustomer'] . '_' . $strdate;
        $outputFile = underscore($outputFile) . '.pdf';
        
        $pdf = $this->wideproformareport;
        $params = array(
            'lib_digitToLetterConvertor' => $this->numbers_words,
            
            'var_quot_number'       => $post['txtCotationNumber'],
            'var_proforma_date'     => $post['txtCotationDate'],
            'var_customer'          => $post['txtCustomer'],
            'var_cotation_source'   => $post['txtCotationSource'],
            'var_customer_fullname' => $post['txtCommercialFullname'],
            
            'arr_ref'               => $post['txtRef'],
            'arr_designation'       => $post['txtDesignation'],
            'arr_qty'               => $post['txtQty'],
            'arr_pu'                => $post['txtPU'],

            'var_base_exclude_vat'  => $post['txtBaseExcludeVat'],
            'var_vat'               => $post['txtVat'],
            'var_mttc'              => $post['txtMttc'],
            'var_net_paid'          => $post['txtNetPaid'],
            
            'var_payment_mode'      => $post['txtPaymentMode'],
            'var_payment_delay'     => $post['txtPaymentDelay'],
            'var_cotation_validity' => $post['txtCotationValidity'],
            'var_cotation_delivery' => $post['txtCotationDelivery'],
            
            'var_short_unit'        => 'Ar',
        );
        
        $pdf->initContent($params);
        
        $pdf->Output($outputFile, 'D');
        
        $res = array(
            'output_file' => $outputFile,
        );
        
        echo json_encode($res);
    }
}