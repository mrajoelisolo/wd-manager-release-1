<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class Portal extends CI_Controller {
    public function __construct() {
        parent::__construct();
        
        $this->load->model('account_model');
    }
    
    public function index() {
        $this->load->view('portal/portal');
    }
    
    public function xlogin() {
        $login = $this->input->post('login');
        $pwd = $this->input->post('pwd');
        
        $res = $this->account_model->authentify($login, $pwd);
        
        $res_status = -1;
        $res_parameter = 'nil';
        
        if( $res ) {
            //If login is success
            $oUser      = $this->account_model->get_by_login($login, $pwd);
            $first_name = $oUser->first_name;
            $user_type  = $oUser->user_type;
            $account_id = $oUser->account_id;
            
            $this->session->set_userdata('user_login', $login);
            $this->session->set_userdata('user_firstname', $first_name);
            $this->session->set_userdata('user_type', $user_type);
            $this->session->set_userdata('account_id', $account_id);
            
            $res_status = 1;
            $res_parameter = 'homepage';
        }else {
            $res_parameter = 'Login failed';
        }
        
        $arr = array(
            'status' => $res_status,
            'parameter' => $res_parameter
        );
        
        echo json_encode($arr);
    }
    
    public function xlogout() {
        $this->session->sess_destroy();
        
        echo base_url().'portal';
    }
}

?>