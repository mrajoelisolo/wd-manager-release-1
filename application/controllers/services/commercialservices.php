<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class CommercialServices extends CI_Controller
{
    const COTATION_SENDED_STATUS = 'SENDED';
    const COTATION_ACCEPTED_STATUS = 'ACCEPTED';
    const COTATION_REJECTED_STATUS = 'REJECTED';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('cotation_model');
        $this->load->model('account_model');
        $this->load->model('customer_model');
        $this->load->model('request_proforma_model');
        $this->load->model('proposition_proforma_model');
        $this->load->model('datetime_model');
        $this->load->model('dispatch_queue_model');
    }
    
    /**
     * Récupérer la liste des dispatchs en attente reçu par le commercial
     * 
     * @author Mitanjo
     */
    public function source_pending_dispatchs()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_pending_dispatchs_by_commercial_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'limit_send_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_pending_dispatchs_by_commercial_id($account_id, $sOrder, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            
            $aOutput['aaData'][] = $row;
        }
        
        //On flushe la liste des nouveaux dispatchs
        $this->dispatch_queue_model->flush_dispatchs_queue($account_id);
        
        echo json_encode($aOutput);
    }
    
    /**
     * Récupère la liste des cotations envoyés
     * 
     * @author Mitanjo
     */
    function source_sended_cotations()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_sended_cotations_by_commercial_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'validity_start',
            'validity_end',
        );
        
        $sOrder = $aColumns[4] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_sended_cotations_by_commercial_id($account_id, $sOrder, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            $oPropositionDesignations = $this->cotation_model->get_proposition_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = $this->parse_designation_cotation($oPropositionDesignations);
            $row[] = mysqldate_to_str($oRow->validity_start);
            $row[] = mysqldate_to_str($oRow->validity_end);
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    /**
     * Retourne la liste des cotations a relancer par le commercial
     * 
     * @author Mitanjo
     */
    function source_relance_cotations()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_sended_cotations_to_check_by_commercial($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'validity_start',
            'validity_end',
        );
        
        $sOrder = $aColumns[4] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_sended_cotations_to_check_by_commercial($account_id, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            $oPropositionDesignations = $this->cotation_model->get_proposition_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = $this->parse_designation_cotation($oPropositionDesignations);
            $row[] = mysqldate_to_str($oRow->validity_start);
            $row[] = mysqldate_to_str($oRow->validity_end);
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    public function source_urgent_dispatchs()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_dispatchs_to_send_before_by_commercial_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'reception_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_dispatchs_to_send_before_by_commercial_id($account_id, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    /**
     * Regrouper les designation de propositions en une seule chaine
     * 
     * @author Mitanjo
     */
    private function parse_designation_cotation($oCotationDesignations)
    {
        $strDesignation = '';
        foreach( $oCotationDesignations as $oCotationDesignation )
        {
            if( $strDesignation != '' )
                $strDesignation .= ', ';
            
            $strDesignation .= $oCotationDesignation->designation;
        }
            
        return character_limiter($strDesignation, 70);
    }
    
    /**
     * Récupère l'enregistrement d'une cotation à partir de son id
     * 
     * @author Mitanjo
     */
    public function fetch_pending_dispatch()
    {
        $cotationid = $this->input->post('id');
        
        $aCotation = $this->cotation_model->get_by_id($cotationid);

        $customer_fk = $aCotation->customer_fk;
        $oCustomer = $this->customer_model->get_by_id($customer_fk);
        
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $res['cotation_id']               = $aCotation->cotation_id;
        $res['cotation_source']           = $aCotation->cotation_source;
        $res['limit_send_date']           = mysqldate_to_str($aCotation->limit_send_date);
        $res['reception_date']            = mysqldate_to_str($aCotation->reception_date);
        $res['validity_end']              = mysqldate_to_str($aCotation->validity_end);
        $res['validity_start']            = mysqldate_to_str($aCotation->validity_start);
        $res['cotation_number_customer']  = $aCotation->cotation_number_customer;
        
        $res['customer_name']             = $oCustomer->enterprise_name;
        $res['customer_address']          = $oCustomer->enterprise_address;
        $res['customer_mail']             = $oCustomer->customer_mail;
        $res['customer_phone']            = $oCustomer->customer_phone;
        $res['purchaser_name']            = $oCustomer->purchaser_name;
        
        $res['commercial_fullname']       = $oUser->last_name . ' ' . $oUser->first_name;
        
        $res['proformas']                 = $this->request_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        //Génération du numéro de proforma
        $cotationNumber = leading_format($cotationid);
        $numProforma = $cotationNumber . '/' . $oUser->acronym . '/WD/' . $this->datetime_model->getCurrentYear();
        $res['cotation_number'] = $numProforma; 
        
        echo json_encode($res);
    }
    
    /**
     * Récupérer une cotation déja envoyée
     * 
     * @author Mitanjo
     */
    public function fetch_sended_cotation()
    {
        $cotationid = $this->input->post('id');
        
        $aCotation = $this->cotation_model->get_by_id($cotationid);

        $customer_fk = $aCotation->customer_fk;
        $oCustomer = $this->customer_model->get_by_id($customer_fk);
        
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $res['cotation_id']               = $aCotation->cotation_id;
        $res['cotation_source']           = $aCotation->cotation_source;
        $res['limit_send_date']           = mysqldate_to_str($aCotation->limit_send_date);
        $res['reception_date']            = mysqldate_to_str($aCotation->reception_date);
        $res['validity_end']              = mysqldate_to_str($aCotation->validity_end);
        $res['validity_start']            = mysqldate_to_str($aCotation->validity_start);
        $res['cotation_number_customer']  = $aCotation->cotation_number_customer;
        $res['cotation_number']           = $aCotation->cotation_number;
        
        $res['customer_name']             = $oCustomer->enterprise_name;
        $res['customer_address']          = $oCustomer->enterprise_address;
        $res['customer_mail']             = $oCustomer->customer_mail;
        $res['customer_phone']            = $oCustomer->customer_phone;
        $res['purchaser_name']            = $oCustomer->purchaser_name;
        
        $res['commercial_fullname']       = $oUser->last_name . ' ' . $oUser->first_name;
        
        $res['request_proformas']         = $this->request_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        $res['proposition_proformas']     = $this->proposition_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        echo json_encode($res);
    }
    
    /**
     * Change le statut d'une cotation en 'SENDED' et passe à l'état des cotations envoyées
     * 
     * @author Mitanjo
     */
    public function send_dispatch()
    {
        $cotation = $this->input->post();
        
        $cotation_id                = intval($this->input->post('txtCotationId'));
        $cotation_number            = $this->input->post('txtCotationNumber');
        $id_selected_row            = $this->input->post('idSelectedRow');
        
        $this->cotation_model->update_cotation_status($cotation_id, self::COTATION_SENDED_STATUS, $cotation_number);
        
        $arrPropositionReferences   = $this->input->post('arrReferences2');
        $arrPropositionDesignations = $this->input->post('arrDesignations2');
        $arrPropositionQtys         = $this->input->post('arrQuantitys2');
        $arrPropositionPrices       = $this->input->post('arrPricesUnit2');
        
        $i = 0;
        foreach( $arrPropositionDesignations as $propositionDesignation )
        {
            $propositionReference = $arrPropositionReferences[$i];
            $propositionQty       = $arrPropositionQtys[$i];
            $propositionPrice     = $arrPropositionPrices[$i];
            
            $this->proposition_proforma_model->save($propositionReference, $propositionDesignation, $propositionQty, $propositionPrice, $cotation_id);
            
            $i++;
        }
        
        $res = array(
            'action' => 'validation',
            'id_selected_row' => $cotation_id,
            'redirection' => base_url() . 'commercial/cotationreporting/edit/' . $cotation_id,
        );
        echo json_encode($res);
    }
    
    /**
     * Change le statut d'une cotation en ACCEPTED
     * 
     * @author Mitanjo
     */
    public function accept_sended_cotation()
    {
        $cotation_id     = $this->input->post('txtCotationId');
        $id_selected_row = $this->input->post('idSelectedRow');
        
        $this->cotation_model->update_cotation_status($cotation_id, self::COTATION_ACCEPTED_STATUS);
        
        $res = array(
            'action' => 'accept_cotation',
            'id_selected_row' => $id_selected_row,
        );
        echo json_encode($res);
    }
    
    /**
     * Change le statut d'une cotation en REJECTED
     * 
     * @author Mitanjo
     */
    public function reject_sended_cotation()
    {
        $cotation_id = $this->input->post('txtCotationId');
        $id_selected_row = $this->input->post('idSelectedRow');
        
        $this->cotation_model->update_cotation_status($cotation_id, self::COTATION_REJECTED_STATUS);
        
        $res = array(
            'action' => 'reject_cotation',
            'id_selected_row' => $id_selected_row,
        );
        echo json_encode($res);
    }
    
    /**
     * Prolonge le délai d'expiration d'une cotation et
     * change le statut d'une cotation en SENDEDE
     * 
     * @author Mitanjo
     */
    public function prolongate_sended_cotation()
    {
        
    }

    public function check_dispatch_notification()
    {
        $res_status = 0;

        $account_id = $this->session_model->getUserData('account_id');
        
        $urgentDispatchCount = $this->cotation_model->count_dispatchs_to_send_before_by_commercial_id($account_id);

        if( $urgentDispatchCount > 0 )
        {
            $res_status = 1;
        }

        $res = array(
            'status' => $res_status,
        );
        
        echo json_encode($res);
    }
    
    public function check_new_dispatch()
    {
        $res_status = 0;

        $account_id = $this->session_model->getUserData('account_id');

        $count = $this->dispatch_queue_model->count_dispatchs_queue($account_id);
        
        if($count > 0)
        {
            $res_status = 1;
        }

        $res = array(
            'status' => $res_status,
        );
        
        echo json_encode($res);
    }
    
    public function check_relance()
    {
        $res_status = 0;

        $account_id = $this->session_model->getUserData('account_id');

        $count = $this->cotation_model->count_sended_cotations_to_check_by_commercial($account_id);

        if($count > 0)
        {
            $res_status = 1;
        }

        $res = array(
            'status' => $res_status,
        );
        
        echo json_encode($res);
    }
    
    /**
     * Récupérer la liste des dispatchs dans la file d'attente
     * 
     * @author Mitanjo
     */
    public function source_new_dispatchs()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_new_dispatchs_by_commercial_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'limit_send_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_new_dispatchs_by_commercial_id($account_id, $sOrder, $iDisplayStart, $iDisplayLength);

        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();

            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            
            $aOutput['aaData'][] = $row;
        }

        //On flushe la liste des nouveaux dispatchs
        $this->dispatch_queue_model->flush_dispatchs_queue($account_id);

        echo json_encode($aOutput);
    }
}