<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DateServices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function today()
    {
        $format = 'd/m/Y';
        $interval = 0;
        
        $daysAfter = (int) $this->input->post('daysafter');
        if( $daysAfter != 0 )
        {   
            $interval = $daysAfter * 3600 * 24;
        }
        
        $date = date($format, time() + $interval);
        
        echo $date;
    } 
}