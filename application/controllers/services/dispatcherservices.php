<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class DispatcherServices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('account_model');
        $this->load->model('customer_model');
        $this->load->model('cotation_model');
        $this->load->model('request_proforma_model');
        $this->load->model('proposition_proforma_model');
    }
    
    /**
     * Recuperer la liste des clients, utilise par l'autocompltete du newdispatch
     * 
     * @author Mitanjo
     */
    public function source_customers($input, $limit)
    {
        $len = strlen($input);
        
        $aResults = array();
        $len = strlen($input);
        
        if( $len )
        {
            $oCustomers = $this->customer_model->fetch_by_name($input, $limit);
            foreach( $oCustomers as $oCustomer )
            {
                $aResults[] = array(
                    "id" => $oCustomer->customer_id,
                    "value" => $oCustomer->enterprise_name . ' - ' . $oCustomer->purchaser_name,
                    "info" => $oCustomer->enterprise_address,
                    "customer_name"    => $oCustomer->enterprise_name,
                    "customer_address" => $oCustomer->enterprise_address,
                    "customer_mail"    => $oCustomer->customer_mail,
                    "customer_phone" => $oCustomer->customer_phone,
                    "purchaser_name" => $oCustomer->purchaser_name,
                );
            }
        }
        
        $json_result = '{"results": ' . json_encode($aResults) . '}';
        echo $json_result;
    }
    
    /**
     * Enregistre un dispatch et l'envoi au commercial concerné
     * 
     * @author Mitanjo
     */
    public function save_dispatch()
    {
        $account_id = $this->session->userdata('account_id');
        //$oUser      = $this->account_model->get_by_id($account_id);
        
        $cotation_source          = strtoupper($this->input->post('cmbCotationSource'));
        $reception_date           = str_to_mysqldate($this->input->post('txtReceptionDate'));
        $limit_send_date          = str_to_mysqldate($this->input->post('txtLimitSendDate'));
        $commercial_id            = intval($this->input->post('cmbCommercial'));
        $validity_start           = str_to_mysqldate($this->input->post('txtValidityStart'));
        $validity_end             = str_to_mysqldate($this->input->post('txtValidityEnd'));
        $cotation_number_customer = $this->input->post('txtCotationNumberCustomer');
        $cotation_status = 'PENDING';
        
        $customer_id     = intval($this->input->post('txtCustomerId'));
        
        //Saving customer
        $customer_fk = 0;
        if( $customer_id == -1 )
        {
            $customer_name    = $this->input->post('txtCustomerName');
            $customer_address = $this->input->post('txtCustomerAddress');
            $customer_mail    = $this->input->post('txtCustomerMail');
            $customer_phone   = $this->input->post('txtCustomerPhone');
            $purchaser_name    = $this->input->post('txtPurchaseName');
            
            $customer_fk = $this->customer_model->save($customer_name, $customer_address, $customer_mail, $customer_phone, $purchaser_name, $account_id);
        }
        else
        {
            $customer_fk = $customer_id;
        }
        
        //Saving cotation
        $cotation_fk = $this->cotation_model->save(
            $cotation_source,
            $reception_date,
            $limit_send_date,
            $validity_start, 
            $validity_end,
            $cotation_status,
            $cotation_number_customer,
            $customer_fk,
            $account_id,
            $commercial_id
        );

        //Saving requests
        $i = 0;
        $arrReferences   = $this->input->post('arrReferences');
        $arrQuantitys    = $this->input->post('arrQuantitys');
        $arrDesignations = $this->input->post('arrDesignations');
        foreach( $arrDesignations as $strDesignationRequest )
        {
            $designationReference = $arrReferences[$i];
            $designationQty = $arrQuantitys[$i];
            
            $this->request_proforma_model->save($designationReference, $strDesignationRequest, $designationQty, $cotation_fk);
            
            $i++;
        }
        
        $arr = array(
            'status' => 1,
        );
        
        echo json_encode($arr);
    }
    
    /**
     * Retourne la liste des cotations a relancer par le commerciaux
     * 
     * @author Mitanjo
     */
    public function source_relance_cotations()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_sended_cotations_to_check_by_dispatcher($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'validity_start',
            'validity_end',
        );
        
        $sOrder = $aColumns[4] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_sended_cotations_to_check_by_dispatcher($account_id, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            $oPropositionDesignations = $this->cotation_model->get_proposition_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = $this->parse_designation_cotation($oPropositionDesignations);
            $row[] = mysqldate_to_str($oRow->validity_start);
            $row[] = mysqldate_to_str($oRow->validity_end);
            $row[] = $oRow->commercial_lastname . ' ' . $oRow->commercial_firstname;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    /**
     * Retourne la liste des dispatchs urgents qui doivent être traités par les commerciaux
     * 
     * @author Mitanjo
     */
    public function source_urgent_dispatchs()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_dispatchs_to_send_before_by_dispatcher_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'reception_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_dispatchs_to_send_before_by_dispatcher_id($account_id, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            $row[] = $oRow->commercial_lastname . ' ' . $oRow->commercial_firstname;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    /**
     * Retourne la liste des cotations envoyés
     * 
     * @author Mitanjo
     */
    public function source_sended_cotations()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_sended_cotations_by_dispatcher_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'validity_start',
            'validity_end',
        );
        
        $sOrder = $aColumns[4] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_sended_cotations_by_dispatcher_id($account_id, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            $oPropositionDesignations = $this->cotation_model->get_proposition_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = $this->parse_designation_cotation($oPropositionDesignations);
            $row[] = mysqldate_to_str($oRow->validity_start);
            $row[] = mysqldate_to_str($oRow->validity_end);
            $row[] = $oRow->commercial_lastname . ' ' . $oRow->commercial_firstname;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    /**
     * Regrouper les designation de propositions en une seule chaine
     * 
     * @author Mitanjo
     */
    private function parse_designation_cotation($oCotationDesignations)
    {
        $strDesignation = '';
        foreach( $oCotationDesignations as $oCotationDesignation )
        {
            if( $strDesignation != '' )
                $strDesignation .= ', ';
            
            $strDesignation .= $oCotationDesignation->designation;
        }
            
        return character_limiter($strDesignation, 70);
    }

/**
     * Récupérer la liste des dispatchs en attente reçu par le commercial
     * 
     * @author Mitanjo
     */
    public function source_pending_dispatchs()
    {
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_pending_dispatchs_by_dispatcher_id($account_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'limit_send_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_pending_dispatchs_by_dispatcher_id($account_id, $sOrder, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            $row[] = $oRow->commercial_lastname . ' ' . $oRow->commercial_firstname;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }

    /**
     * Récupérer une cotation déja envoyée
     * 
     * @author Mitanjo
     */
    public function fetch_sended_cotation()
    {
        $cotationid = $this->input->post('id');
        
        $aCotation = $this->cotation_model->get_by_id($cotationid);

        $customer_fk = $aCotation->customer_fk;
        $oCustomer = $this->customer_model->get_by_id($customer_fk);
        
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $res['cotation_id']               = $aCotation->cotation_id;
        $res['cotation_source']           = $aCotation->cotation_source;
        $res['limit_send_date']           = mysqldate_to_str($aCotation->limit_send_date);
        $res['reception_date']            = mysqldate_to_str($aCotation->reception_date);
        $res['validity_end']              = mysqldate_to_str($aCotation->validity_end);
        $res['validity_start']            = mysqldate_to_str($aCotation->validity_start);
        $res['cotation_number_customer']  = $aCotation->cotation_number_customer;
        $res['cotation_number']           = $aCotation->cotation_number;
        
        $res['customer_name']             = $oCustomer->customer_name;
        $res['customer_address']          = $oCustomer->customer_address;
        $res['customer_mail']             = $oCustomer->customer_mail;
        $res['customer_phone']            = $oCustomer->customer_phone;
        $res['purchaser_name']            = $oCustomer->purchaser_name;
        
        $res['commercial_fullname']       = $oUser->last_name . ' ' . $oUser->first_name;
        
        $res['request_proformas']         = $this->request_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        $res['proposition_proformas']     = $this->proposition_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        echo json_encode($res);
    }
    
    /**
     * Récupère l'enregistrement d'une cotation à partir de son id
     * 
     * @author Mitanjo
     */
    public function fetch_pending_dispatch()
    {
        $cotationid = $this->input->post('id');
        
        $aCotation = $this->cotation_model->get_by_id($cotationid);

        $customer_fk = $aCotation->customer_fk;
        $oCustomer = $this->customer_model->get_by_id($customer_fk);
        
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $res['cotation_id']               = $aCotation->cotation_id;
        $res['cotation_source']           = $aCotation->cotation_source;
        $res['limit_send_date']           = mysqldate_to_str($aCotation->limit_send_date);
        $res['reception_date']            = mysqldate_to_str($aCotation->reception_date);
        $res['validity_end']              = mysqldate_to_str($aCotation->validity_end);
        $res['validity_start']            = mysqldate_to_str($aCotation->validity_start);
        $res['cotation_number_customer']  = $aCotation->cotation_number_customer;
        
        $res['customer_name']             = $oCustomer->enterprise_name;
        $res['customer_address']          = $oCustomer->enterprise_address;
        $res['customer_mail']             = $oCustomer->customer_mail;
        $res['customer_phone']            = $oCustomer->customer_phone;
        $res['purchaser_name']            = $oCustomer->purchaser_name;
        
        $res['commercial_fullname']       = $oUser->last_name . ' ' . $oUser->first_name;
        
        $res['proformas']                 = $this->request_proforma_model->fetch_array_by_cotation_id($cotationid);
        
        echo json_encode($res);
    }
    
    public function source_customers_except_one()
    {
        $res = array();
        
        $oCommercials = $this->account_model->getCommercialsExcept(10);
        
        foreach( $oCommercials as $oCommercial )
        {
            $res['commercials'][] = array(
                array(
                    'commercial_id' => $oCommercial->account_id,
                    'first_name' => $oCommercial->first_name,
                    'last_name' => $oCommercial->last_name
                ),
            );   
        } 
        
        echo json_encode($res);
    }
    
    public function source_commercials()
    {
        //$account_id = $this->session->userdata('account_id');
        //$oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->account_model->countCommercials();
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'account_id',
            'first_name',
            'last_name',
            'acronym',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->account_model->getCommercials($iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->account_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->account_id;
            
            $row[] = $oRow->account_id;
            $row[] = $oRow->last_name;
            $row[] = $oRow->first_name;
            $row[] = $oRow->acronym;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
    
    //Methocs for reaffected dispatchs
    public function source_dispatchs_by_commercial_id($commercialId)
    {
        $commercial_id = intval($commercialId);
        
        $account_id = $this->session->userdata('account_id');
        $oUser      = $this->account_model->get_by_id($account_id);
        
        $sEcho = $this->input->post('sEcho');
        $nTotal = $this->cotation_model->count_pending_dispatchs_by_commercial_id($commercial_id);
        $nFilteredTotal = $nTotal; //If no filter
        
        $iDisplayStart = intval($this->input->post('iDisplayStart'));
        $iDisplayLength = intval($this->input->post('iDisplayLength'));
        
        $aColumns = array(
            'cotation_id',
            'cotation_number_customer',
            'customer_name',
            'limit_send_date',
        );
        
        $sOrder = $aColumns[0] . ' asc';
        $sortCol = intval($this->input->post('iSortCol_0'));
        if( $sortCol > 0 )
        {
            $sOrder = '';
            $nColumns = intval($this->input->post('iSortingCols'));
            
            for( $i = 0; $i < $nColumns; $i++ )
            {
                $iSortCol = $this->input->post('iSortCol_' . $i);
                $bSortable = $this->input->post('bSortable_' . $iSortCol);
                if( $bSortable == 'true' )
                {
                    if( $sOrder != '' )
                        $sOrder .= ', ';
                    
                    $sortDir = $this->input->post('sSortDir_' . $i);
                    $sOrder .= $aColumns[$iSortCol] . ' ' . $sortDir;
                }
            }
        }
        
        $oResults = $this->cotation_model->get_pending_dispatchs_by_commercial_id($commercial_id, $sOrder, $iDisplayStart, $iDisplayLength);
        
        $aOutput = array(
            'sEcho' => $sEcho,
            'iTotalRecords' => $nFilteredTotal,
            'iTotalDisplayRecords' => $nTotal,
            'aaData' => array(),
            'debug' => $sOrder,
        );
        
        foreach( $oResults as $oRow )
        {
            $row = array();
            
            $row['DT_RowId'] = 'row_'.$oRow->cotation_id;
            $row['DT_RowClass'] = 'gradeC';
            
            $cotation_id = $oRow->cotation_id;
            
            $oRequestDesignations = $this->cotation_model->get_request_proforma_by_cotation_id($cotation_id);
            
            $row[] = $oRow->cotation_id;
            $row[] = $oRow->cotation_number_customer;
            $row[] = $oRow->customer_name;
            $row[] = $this->parse_designation_cotation($oRequestDesignations);
            $row[] = mysqldate_to_str($oRow->limit_send_date);
            $row[] = $oRow->commercial_lastname . ' ' . $oRow->commercial_firstname;
            
            $aOutput['aaData'][] = $row;
        }
        
        echo json_encode($aOutput);
    }
}

?>