<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class NewDispatch extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('account_model');
    }
    
    public function index()
    {
        if( $this->session_model->checkIfSessionActive('DISPATCHER') )
        {
            $folderName = 'dispatcher/newdispatch/';
            
            //CSS Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
                array('var_resource' => css_url('jqautocomplete')),
            );
            
            //JS resources
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('autocomplete.jquery')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal JS
            //Literal JS
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript', assets_paths(), TRUE)),
            );
            
            $hData = assets_paths();
            $cData = assets_paths();
            $fData = assets_paths();
            
            $hData = array_merge($hData, array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            //Setting content data
            $oCommercials = $this->account_model->getCommercials();
            $varCommercials = array();
            foreach( $oCommercials as $oCommercial )
            {
                $varCommercials[] = array(
                    'id'       => $oCommercial->account_id,
                    'fullname' => $oCommercial->first_name . ' ' . $oCommercial->last_name,
                );
            }
            
            $cData = array_merge($cData, array(
                'var_commercials' => $varCommercials, 
            ));
            //End of setting content data
            
            //Setting footer data
            $fData = array_merge($fData, array(
                'copyright' => copyright(),
            ));
            //End of setting footer data
            
            $headerZone = $this->parser->parse($folderName . 'zones/headerzone', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone', $cData, TRUE);
            $footerZone = $this->parser->parse('footerzone', $fData, TRUE);
            
            //Setting the layout
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title' => 'Wide manager - Nouveau dispatch',
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
            
            $this->parser->parse('layout', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
}

?>