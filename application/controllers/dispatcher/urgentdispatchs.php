<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */

class UrgentDispatchs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
    }
    
    public function index()
    {
        if( $this->session_model->checkIfSessionActive('DISPATCHER') )
        {
            $folderName = 'dispatcher/urgentdispatchs/';
            
            //CSS Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('modal')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
                array('var_resource' => css_url('datatable')),
            );
            
            //JS resources
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('jquery.simplemodal-1.4.3.min')),
                array('var_resource' => js_url('jquery.dataTables.min')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal js, a literal javascript content
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse('commercial/pendingdispatchs/scripts/pendingdispatchs', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript', assets_paths(), TRUE)),
            );
            
            $hData = assets_paths();
            $cData = assets_paths();
            $fData = assets_paths();
            
            $hData = array_merge($hData, array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            $cData = array_merge($cData, array(
                'content_pending_dispatch' => $this->parser->parse('commercial/pendingdispatchs/contents/pending_dispatch_content_readonly', assets_paths(), TRUE),
            ));
            $fData = array_merge($fData, array(
                'copyright' => copyright(),
            ));
            
            $headerZone  = $this->parser->parse($folderName . 'zones/headerzone', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone', $cData, TRUE);
            $footerZone  = $this->parser->parse('footerzone', $fData, TRUE);
            
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title'      => 'Wide manager - Dispatchs urgents',
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
            
            $this->parser->parse('layout', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
}