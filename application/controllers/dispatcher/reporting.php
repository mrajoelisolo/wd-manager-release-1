<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Mitanjo
 * @copyright 2012
 */
 
class Reporting extends CI_Controller
{
    const COUNT_PER_PAGE = 5;
    
    public function __construct()
    {
        parent::__construct();
		
        $this->load->model('session_model');
        $this->load->library('numbers_words');
        $this->load->library('pagination');
        
		$this->load->model('cotation_model');
        $this->load->model('proposition_proforma_model');
        $this->load->model('account_model');
        
        $this->load->library('wideproformareport', array(
			'orientation' => 'P',
			'unit' => 'mm',
			'size' => 'A4',
		));
    }
    
    public function show()
    {        
        if( $this->session_model->checkIfSessionActive('DISPATCHER') )
        {
            /* Setting URI segments */
            $params = $this->uri->uri_to_assoc(4);

            $f_cotation_num  = ( isset($params['cnum'])) ? $params['cnum'] : '';
            $f_cotation_date = ( isset($params['cdate'])) ? $params['cdate'] : '';
            $f_commercial_id = ( isset($params['com'])) ? $params['com'] : '';
            $start = ( isset($params['start'])) ? $params['start'] : 0;

            $segments = '';
            if( trim($f_cotation_num) != '' )
            {
                $segments .= 'cnum/' . $f_cotation_num;
            }
            
            if( trim($f_cotation_date) != '' )
            {
                if( $segments != '' ) $segments .= '/';
                $segments .= 'cdate/' . $f_cotation_date;
            }
            
            if( trim($f_commercial_id) != '' )
            {
                if( $segments != '' ) $segments .= '/';
                $segments .= 'com/' . $f_commercial_id;
            }
            
            if( $segments != '' )
            {
                $segments = $segments . '/';
            }
            /* Endof setting URI segments */
            
            $folderName = 'dispatcher/reporting/';
            
            //CSS Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
            );
            
            //JS resources
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('autocomplete.jquery')),
                array('var_resource' => js_url('wide-script')),
            );
            
            //Literal JS
            //Literal JS
            $literalJs = array(
                array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                array('var_literal_js' => $this->parser->parse($folderName . 'scripts/literalscript', assets_paths(), TRUE)),
            );
            
            $hData = assets_paths();
            $cData = assets_paths();
            $fData = assets_paths();
            
            $hData = array_merge($hData, array(
                'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
            ));
            
            //Setting content data
            $varCommercials = $this->account_model->getCommercials();
            
            if( $f_cotation_date != '' )
            {
                $arr_cotation_date = explode('-', $f_cotation_date);
                $f_cotation_date = $arr_cotation_date[2] . '-' . $arr_cotation_date[1] . '-' . $arr_cotation_date[0];
            } 

            $cotations = $this->cotation_model->get_array_cotations_by($f_cotation_num, $f_cotation_date, $f_commercial_id, 'ACCEPTED', $start, self::COUNT_PER_PAGE);
            $totalRows = $this->cotation_model->count_cotations_by($f_cotation_num, $f_cotation_date, $f_commercial_id, 'ACCEPTED');
            
            $config = array(
                'base_url' => base_url() . 'dispatcher/reporting/show/' . $segments . 'start/',
                'total_rows' => $totalRows,
                'per_page' => self::COUNT_PER_PAGE,
                'first_link' => 'D&eacute;but',
                'last_link' => 'Fin',
                'next_link' => '&gt;',
                'prev_link' => '&lt;',
                'cur_tag_open' => '&nbsp;<strong class="inactive_anchor">',
                'cur_tag_close' => '</strong>',
                'cur_page' => $start,
            );
            $this->pagination->initialize($config);
            $var_pagination = $this->pagination->create_links();
            
            $cData = array_merge($cData, array(
                'var_cotations' => $cotations,
                'var_commercials' => $varCommercials,
                'var_pagination' => $var_pagination,
            ));
            
            //Setting footer data
            $fData = array_merge($fData, array(
                'copyright' => copyright(),
            ));
            //End of setting footer data
            
            $headerZone = $this->parser->parse($folderName . 'zones/headerzone', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'zones/contentzone', $cData, TRUE);
            $footerZone = $this->parser->parse('footerzone', $fData, TRUE);
            
            //Setting the layout
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title' => 'Wide manager - Reporting',
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
            
            $this->parser->parse('layout', $data);
        }
        else
        {
            redirect('/portal');
        }
    }

    public function topdf()
    {
        $post = $this->input->post();
        
        $pdf = $this->wideproformareport;
        $params = array(
            'lib_digitToLetterConvertor' => $this->numbers_words,
            
            'var_quot_number'       => $post['txtCotationNumber'],
            'var_proforma_date'     => $post['txtCotationDate'],
            'var_customer'          => $post['txtCustomer'],
            'var_cotation_source'   => $post['txtCotationSource'],
            'var_customer_fullname' => $post['txtCommercialFullname'],
            
            'arr_ref'               => $post['txtRef'],
            'arr_designation'       => $post['txtDesignation'],
            'arr_qty'               => $post['txtQty'],
            'arr_pu'                => $post['txtPU'],

            'var_base_exclude_vat'  => $post['txtBaseExcludeVat'],
            'var_vat'               => $post['txtVat'],
            'var_mttc'              => $post['txtMttc'],
            'var_net_paid'          => $post['txtNetPaid'],
            
            'var_payment_mode'      => $post['txtPaymentMode'],
            'var_payment_delay'     => $post['txtPaymentDelay'],
            'var_cotation_validity' => $post['txtCotationValidity'],
            'var_cotation_delivery' => $post['txtCotationDelivery'],
            
            'var_short_unit'        => 'Ar',
        );
        
        $pdf->initContent($params);
        
        $pdf->Output();
    }
}