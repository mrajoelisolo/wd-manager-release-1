<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Homepage extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('session_model');
        $this->load->model('account_model');
        $this->load->model('cotation_model');
    }
    
    public function index()
    {
        if( $this->session_model->getUserData('user_login') )
        {
            if( $this->getFolderNameByUserType() == 'admin' )
            {
                redirect('/admin/home');
                
                return;
            }
            
            $account_id = $this->session->userdata('account_id');
            $oUser      = $this->account_model->get_by_id($account_id);
            $folderName = $this->getFolderNameByUserType() . '/';
            
            //CSS Resources
            $cssResources = array(
                array('var_resource' => css_url('style')),
                array('var_resource' => css_url('ui/jquery.ui.all')),
                array('var_resource' => css_url('modal')),
            );
            
            //JS Resources
            $jsResources = array(
                array('var_resource' => js_url('jquery.min')),
                array('var_resource' => js_url('ui/jquery-ui')),
                array('var_resource' => js_url('ui/jquery.ui.core')),
                array('var_resource' => js_url('ui/jquery.ui.widget')),
                array('var_resource' => js_url('ui/jquery.ui.button')),
                array('var_resource' => js_url('ui/jquery.ui.mouse')),
                array('var_resource' => js_url('ui/jquery.ui.draggable')),
                array('var_resource' => js_url('ui/jquery.ui.position')),
                array('var_resource' => js_url('ui/jquery.ui.dialog')),
                array('var_resource' => js_url('ui/jquery.ui.datepicker')),
                array('var_resource' => js_url('jquery.simplemodal-1.4.3.min')),
                array('var_resource' => js_url('jquery.timer')),
                array('var_resource' => js_url('wide-script')),
            );
            
            $hData = assets_paths();
            $cData = assets_paths();
            $fData = assets_paths();
            
            if( $this->getFolderNameByUserType() == 'commercial' )
            {
                $cssResources[] = array('var_resource' => css_url('datatable'));
                $jsResources[]  = array('var_resource' => js_url('jquery.dataTables.min'));
                
                //Literal JS
                $literalJs = array(
                    array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                    array('var_literal_js' => $this->parser->parse($folderName . 'index/scripts/literalscript.tpl', assets_paths(), TRUE)),
                );
                
                $var_count_pending_dispatchs = $this->cotation_model->count_pending_dispatchs_by_commercial_id($account_id);
                $var_count_sended_cotations  = $this->cotation_model->count_sended_cotations_by_commercial_id($account_id);
                $var_count_urgent_proformas  = $this->cotation_model->count_dispatchs_to_send_before_by_commercial_id($account_id);
                $var_count_relance_cotations = $this->cotation_model->count_sended_cotations_to_check_by_commercial($account_id);
                
                $var_account_state = 'UNLOCKED';
                if( $var_count_urgent_proformas > 0 )
                {
                    $var_account_state = 'LOCKED';
                }
                
                $var_relance_state = 'NO_RELANCE';
                if( $var_count_relance_cotations > 0 )
                {
                    $var_relance_state = 'RELANCE';
                }
                
                $hData = array_merge($hData, array(
                    'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
                    'var_firstname' => $oUser->first_name,
                    'var_account_state' => $var_account_state,
                    'var_relance_state' => $var_relance_state,
                ));
                
                $cData = array_merge($cData, array(
                    'var_count_pending_dispatchs' => $var_count_pending_dispatchs,
                    'var_count_sended_cotations' => $var_count_sended_cotations,
                    'var_count_urgent_proformas' => $var_count_urgent_proformas,
                    'var_count_relance_cotations' => $var_count_relance_cotations,
                    'content_sended_cotation' => $this->parser->parse($folderName . 'index/zones/content/content_pending_dispatch', assets_paths(), TRUE)
                ));
            }
            else if( $this->getFolderNameByUserType() == 'dispatcher' )
            {
                $cssResources[] = array('var_resource' => css_url('datatable'));
                $jsResources[]  = array('var_resource' => js_url('jquery.dataTables.min'));
                
                //Literal JS
                $literalJs = array(
                    array('var_literal_js' => $this->parser->parse('portal/scripts/login', assets_paths(), TRUE)),
                    array('var_literal_js' => $this->parser->parse($folderName . 'index/scripts/literalscript', assets_paths(), TRUE)),
                );
                
                $var_count_pending_dispatchs = $this->cotation_model->count_pending_dispatchs_by_dispatcher_id($account_id);
                $var_count_sended_dispatchs  = $this->cotation_model->count_sended_cotations_by_dispatcher_id($account_id);
                $var_count_urgent_dispatchs  = $this->cotation_model->count_dispatchs_to_send_before_by_dispatcher_id($account_id);
                $var_count_relance_cotations = $this->cotation_model->count_sended_cotations_to_check_by_dispatcher($account_id);
                
                $var_account_state = 'NO_URGENT_DISPATCHS';
                if( $var_count_urgent_dispatchs > 0 )
                {
                    $var_account_state = 'URGENT_DISPATCHS';
                }
                
                $var_relance_state = 'NO_RELANCE';
                if( $var_count_relance_cotations > 0 )
                {
                    $var_relance_state = 'RELANCE';
                }
                
                $hData = array_merge($hData, array(
                    'logout_dialog_content' => $this->parser->parse('portal/contents/logout_dialog_content', assets_paths(), TRUE),
                    'var_firstname' => $oUser->first_name,
                    'var_account_state' => $var_account_state,
                    'var_relance_state' => $var_relance_state,
                ));
                
                $cData = array_merge($cData, array(
                    'var_count_pending_dispatchs' => $var_count_pending_dispatchs,
                    'var_count_sended_dispatchs'  => $var_count_sended_dispatchs,
                    'var_count_urgent_dispatchs'  => $var_count_urgent_dispatchs,
                    'var_count_relance_cotations' => $var_count_relance_cotations,
                ));
            }
            
            $fData = array_merge($fData, array(
                'copyright' => copyright(),
            ));
            
            $headerZone = $this->parser->parse($folderName . 'index/zones/headerzone', $hData, TRUE);
            $contentZone = $this->parser->parse($folderName . 'index/zones/contentzone', $cData, TRUE);
            $footerZone = $this->parser->parse('footerzone', $fData, TRUE);
            
            //Setting the layout
            $data = assets_paths();
            $data = array_merge($data, array(
                'page_title' => 'Wide manager - Homepage',
                'css_resources'   => $cssResources,
                'js_resources'    => $jsResources,
                'js_literals'     => $literalJs,
                'header_zone'     => $headerZone,
                'content_zone'    => $contentZone,
                'footer_zone'     => $footerZone,
            ));
            
            $this->parser->parse('layout', $data);
        }
        else
        {
            redirect('/portal');
        }
    }
    
    private function getFolderNameByUserType()
    {
        $user_type = $this->session->userdata('user_type');
        
        if( $user_type == 'DISPATCHER' )
        {
            return 'dispatcher';
        }
        else if( $user_type == 'COMMERCIAL' )
        {
            return 'commercial';
        }
        else if( $user_type == 'ADMIN' )
        {
            return 'admin';   
        }
        
        return 'nil';
    }
}