-- phpMyAdmin SQL Dump
-- version 3.3.7deb5
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 20 Juin 2013 à 10:45
-- Version du serveur: 5.1.49
-- Version de PHP: 5.3.3-7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `wide_manager`
--

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `wv_proposition_proforma`
--
CREATE TABLE IF NOT EXISTS `wv_proposition_proforma` (
`proposition_proforma_id` bigint(20) unsigned
,`ref` varchar(45)
,`designation` varchar(2048)
,`price_unit` double
,`qty` float
,`cotation_fk` int(11)
,`amount` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `wv_relance_cotation`
--
CREATE TABLE IF NOT EXISTS `wv_relance_cotation` (
`cotation_id` bigint(20) unsigned
,`customer_name` varchar(45)
,`customer_address` varchar(255)
,`customer_mail` varchar(45)
,`customer_phone` varchar(45)
,`purchaser_name` varchar(45)
,`commercial_firstname` varchar(45)
,`commercial_lastname` varchar(45)
,`commercial_acronym` varchar(5)
,`cotation_source` enum('MAIL','PHYSICAL','PHONE')
,`reception_date` date
,`limit_send_date` date
,`validity_start` date
,`validity_end` date
,`cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED')
,`cotation_number_customer` varchar(45)
,`cotation_number` varchar(45)
,`customer_fk` int(11)
,`dispatcher_fk` int(11)
,`commercial_fk` int(11)
,`duration` int(7)
,`date1` date
,`date2` date
,`date3` date
);
-- --------------------------------------------------------

--
-- Structure de la table `w_account`
--

CREATE TABLE IF NOT EXISTS `w_account` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_type` enum('ADMIN','COMMERCIAL','DISPATCHER') NOT NULL,
  `acronym` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `w_account`
--

INSERT INTO `w_account` (`account_id`, `login`, `pwd`, `first_name`, `last_name`, `user_type`, `acronym`) VALUES
(1, 'superadmin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'John', 'Doe', 'ADMIN', 'JDOE'),
(2, 'commercial1', '3cec6cf175c2438291042a7a392a0c71090d209b', 'Jeanne', 'RASOA', 'COMMERCIAL', 'RAS'),
(3, 'commercial2', 'e3581bfd855e281d7d258a8cea9a921699a20ed9', 'Marc', 'RAJAO', 'COMMERCIAL', 'RAJ'),
(4, 'dispatcher', 'bdf70eff0e4d79093bd5f318014dd13348b89cdb', 'Julien', 'RABE', 'DISPATCHER', 'AND');

-- --------------------------------------------------------

--
-- Structure de la table `w_cotation`
--

CREATE TABLE IF NOT EXISTS `w_cotation` (
  `cotation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHYSICAL','PHONE') NOT NULL,
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') NOT NULL,
  `cotation_number_customer` varchar(45) DEFAULT NULL,
  `cotation_number` varchar(45) NOT NULL,
  `customer_fk` int(11) NOT NULL,
  `dispatcher_fk` int(11) NOT NULL,
  `commercial_fk` int(11) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `w_cotation`
--

INSERT INTO `w_cotation` (`cotation_id`, `cotation_source`, `reception_date`, `limit_send_date`, `validity_start`, `validity_end`, `cotation_status`, `cotation_number_customer`, `cotation_number`, `customer_fk`, `dispatcher_fk`, `commercial_fk`) VALUES
(1, 'PHONE', '2013-06-20', '2013-06-20', '2013-06-20', '2013-07-05', 'SENDED', '', '1/RAS/WD/13', 2, 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `w_customer`
--

CREATE TABLE IF NOT EXISTS `w_customer` (
  `customer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_mail` varchar(45) NOT NULL,
  `customer_phone` varchar(45) NOT NULL,
  `purchaser_name` varchar(45) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `w_customer`
--

INSERT INTO `w_customer` (`customer_id`, `customer_name`, `customer_address`, `customer_mail`, `customer_phone`, `purchaser_name`) VALUES
(1, 'ADEMA', 'Ivato Aeroport', 'sales@adema.mg', '22457636', 'RANDRIA Nary'),
(2, 'EDELEC', 'Andraharo', 'sales@edelec.com', '2235468', 'RAJAO'),
(3, 'BFV-SG', 'Antaninarenina', 'sales@bfv.mg', '2265487', 'RAKOTO Nirina'),
(4, 'AQUAMAD', 'Anosivavaka', 'aquamad@sales.mg', '022 224 16', 'RAKOTO Jaona'),
(5, 'Etech Consulting', 'Anosivavaka', 'etech.consulting@ovh.com', '22 435 34', 'Rakoto');

-- --------------------------------------------------------

--
-- Structure de la table `w_proposition_proforma`
--

CREATE TABLE IF NOT EXISTS `w_proposition_proforma` (
  `proposition_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `price_unit` double NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `w_proposition_proforma`
--

INSERT INTO `w_proposition_proforma` (`proposition_proforma_id`, `ref`, `designation`, `price_unit`, `qty`, `cotation_fk`) VALUES
(1, '', 'TOSHIBA SATELLITE 1250', 1200000, 1, 1),
(2, '', 'ACER ASPIRE 1640Z', 1150000, 1, 1),
(3, '', 'HP 5250', 1600000, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `w_request_proforma`
--

CREATE TABLE IF NOT EXISTS `w_request_proforma` (
  `request_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `w_request_proforma`
--

INSERT INTO `w_request_proforma` (`request_proforma_id`, `ref`, `designation`, `qty`, `cotation_fk`) VALUES
(1, '', 'TOSHIBA SATELLITE 1250', 1, 1),
(2, '', 'ACER ASPIRE 1640Z', 1, 1),
(3, '', 'HP 5250', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `w_sessions`
--

CREATE TABLE IF NOT EXISTS `w_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `w_sessions`
--

INSERT INTO `w_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('38abe74d4791a16391d28ae8a37ba4c1', '192.168.0.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0', 1371717930, 'a:5:{s:9:"user_data";s:0:"";s:10:"user_login";s:11:"commercial1";s:14:"user_firstname";s:6:"Jeanne";s:9:"user_type";s:10:"COMMERCIAL";s:10:"account_id";s:1:"2";}'),
('510c003ff4137f35ad05156e874aeeea', '192.168.0.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36', 1371717507, 'a:5:{s:9:"user_data";s:0:"";s:10:"user_login";s:10:"dispatcher";s:14:"user_firstname";s:6:"Julien";s:9:"user_type";s:10:"DISPATCHER";s:10:"account_id";s:1:"4";}');

-- --------------------------------------------------------

--
-- Structure de la vue `wv_proposition_proforma`
--
DROP TABLE IF EXISTS `wv_proposition_proforma`;

CREATE ALGORITHM=UNDEFINED DEFINER=`wdmanager`@`localhost` SQL SECURITY DEFINER VIEW `wv_proposition_proforma` AS select `pr`.`proposition_proforma_id` AS `proposition_proforma_id`,`pr`.`ref` AS `ref`,`pr`.`designation` AS `designation`,`pr`.`price_unit` AS `price_unit`,`pr`.`qty` AS `qty`,`pr`.`cotation_fk` AS `cotation_fk`,(`pr`.`price_unit` * `pr`.`qty`) AS `amount` from `w_proposition_proforma` `pr`;

-- --------------------------------------------------------

--
-- Structure de la vue `wv_relance_cotation`
--
DROP TABLE IF EXISTS `wv_relance_cotation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`wdmanager`@`localhost` SQL SECURITY DEFINER VIEW `wv_relance_cotation` AS select `ct`.`cotation_id` AS `cotation_id`,`cs`.`customer_name` AS `customer_name`,`cs`.`customer_address` AS `customer_address`,`cs`.`customer_mail` AS `customer_mail`,`cs`.`customer_phone` AS `customer_phone`,`cs`.`purchaser_name` AS `purchaser_name`,`cm`.`first_name` AS `commercial_firstname`,`cm`.`last_name` AS `commercial_lastname`,`cm`.`acronym` AS `commercial_acronym`,`ct`.`cotation_source` AS `cotation_source`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`customer_fk` AS `customer_fk`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`commercial_fk` AS `commercial_fk`,(to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) AS `duration`,(`ct`.`validity_start` + interval ((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) / 3) day) AS `date1`,(`ct`.`validity_start` + interval (((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) * 2) / 3) day) AS `date2`,(`ct`.`validity_start` + interval (to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) day) AS `date3` from ((`w_cotation` `ct` join `w_customer` `cs`) join `w_account` `cm`) where ((`ct`.`customer_fk` = `cs`.`customer_id`) and (`ct`.`commercial_fk` = `cm`.`account_id`));
