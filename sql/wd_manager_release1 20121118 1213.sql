-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.20-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema wd_manager
--

CREATE DATABASE IF NOT EXISTS wd_manager;
USE wd_manager;

--
-- Temporary table structure for view `wv_cotations`
--
DROP TABLE IF EXISTS `wv_cotations`;
DROP VIEW IF EXISTS `wv_cotations`;
CREATE TABLE `wv_cotations` (
  `account_id` int(11),
  `cotation_id` int(10) unsigned,
  `cotation_number` varchar(45),
  `cotation_number_customer` varchar(45),
  `customer_name` varchar(45),
  `commercial_firstname` varchar(45),
  `commercial_lastname` varchar(45),
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED'),
  `reception_date` date,
  `limit_send_date` date,
  `validity_start` date,
  `validity_end` date
);

--
-- Definition of table `w_account`
--

DROP TABLE IF EXISTS `w_account`;
CREATE TABLE `w_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) CHARACTER SET latin1 NOT NULL,
  `pwd` varchar(40) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_type` enum('ADMIN','DISPATCHER','COMMERCIAL') CHARACTER SET latin1 NOT NULL DEFAULT 'COMMERCIAL',
  `account_state` enum('ACTIVATED','DISABLED') CHARACTER SET latin1 NOT NULL DEFAULT 'DISABLED',
  `acronym` varchar(5) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_account`
--

/*!40000 ALTER TABLE `w_account` DISABLE KEYS */;
INSERT INTO `w_account` (`account_id`,`login`,`pwd`,`first_name`,`last_name`,`user_type`,`account_state`,`acronym`) VALUES 
 (5,'mitanjo','f178a3bcfe745f528d2fdf1df35baa86f9e8dd3a','Mitanjo','RAJOELISOLO','DISPATCHER','ACTIVATED','MIT'),
 (6,'rasoa','f178a3bcfe745f528d2fdf1df35baa86f9e8dd3a','Rasoa','RAKOTOMANGA','COMMERCIAL','ACTIVATED','RAS'),
 (7,'jean','f178a3bcfe745f528d2fdf1df35baa86f9e8dd3a','Jean','RAKOTO','COMMERCIAL','DISABLED','RAK');
/*!40000 ALTER TABLE `w_account` ENABLE KEYS */;


--
-- Definition of table `w_cotation`
--

DROP TABLE IF EXISTS `w_cotation`;
CREATE TABLE `w_cotation` (
  `cotation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHONE','PHYSICAL') CHARACTER SET latin1 NOT NULL DEFAULT 'MAIL',
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') CHARACTER SET latin1 NOT NULL DEFAULT 'PENDING',
  `customer_fk` int(10) unsigned NOT NULL,
  `cotation_number_customer` varchar(45) NOT NULL,
  `cotation_number` varchar(45) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_cotation`
--

/*!40000 ALTER TABLE `w_cotation` DISABLE KEYS */;
INSERT INTO `w_cotation` (`cotation_id`,`cotation_source`,`reception_date`,`limit_send_date`,`validity_start`,`validity_end`,`cotation_status`,`customer_fk`,`cotation_number_customer`,`cotation_number`) VALUES 
 (1,'MAIL','2012-11-15','2012-11-20','2012-11-20','2012-11-30','REJECTED',2,'COT_ARO_001','RAS/001/WD/12'),
 (2,'MAIL','2012-11-15','2012-11-17','2012-10-17','2012-11-22','REJECTED',1,'COT_ADE_001','RAS/002/WD/12'),
 (3,'MAIL','2012-11-16','2012-11-19','2012-11-19','2012-11-30','ACCEPTED',1,'COT_ADE_003','JN/004/WD/12'),
 (4,'MAIL','2012-11-16','2012-11-20','2012-10-21','2012-11-30','REJECTED',1,'COT_ARO_004','JN/004/WD/12'),
 (5,'MAIL','2012-10-19','2012-11-20','2012-10-21','2012-11-30','ACCEPTED',1,'ADE_COT_005','JN/006/WD/12'),
 (6,'MAIL','2012-10-19','2012-11-19','2012-10-19','2012-11-30','ACCEPTED',2,'COT_ARO_005','JN/05/WD/12'),
 (7,'PHONE','2012-11-16','2012-11-19','2012-11-16','2012-11-26','PENDING',1,'COT_ADE_006',''),
 (8,'MAIL','2012-11-16','2012-11-20','2012-11-20','2012-11-30','PENDING',1,'COT_007_ADE',''),
 (9,'MAIL','2012-11-16','2012-11-20','2012-11-20','2012-11-30','SENDED',2,'COT_ARO_008','RAS/008/WD/12'),
 (10,'PHONE','2012-11-16','2012-11-22','2012-10-22','2012-11-30','SENDED',2,'COT_ARO_008','JN/008/WD/12');
/*!40000 ALTER TABLE `w_cotation` ENABLE KEYS */;


--
-- Definition of table `w_currency`
--

DROP TABLE IF EXISTS `w_currency`;
CREATE TABLE `w_currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_acronym` varchar(5) CHARACTER SET latin1 NOT NULL,
  `currency_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_currency`
--

/*!40000 ALTER TABLE `w_currency` DISABLE KEYS */;
INSERT INTO `w_currency` (`currency_id`,`currency_acronym`,`currency_name`) VALUES 
 (1,'MGA','Malagasy Ariary');
/*!40000 ALTER TABLE `w_currency` ENABLE KEYS */;


--
-- Definition of table `w_customer`
--

DROP TABLE IF EXISTS `w_customer`;
CREATE TABLE `w_customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `customer_mail` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_phone` varchar(45) CHARACTER SET latin1 NOT NULL,
  `purchaser_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_customer`
--

/*!40000 ALTER TABLE `w_customer` DISABLE KEYS */;
INSERT INTO `w_customer` (`customer_id`,`customer_name`,`customer_address`,`customer_mail`,`customer_phone`,`purchaser_name`) VALUES 
 (1,'ADEMA','Ivato aeroport','sales@adema.mg','0343234053','ADEMA SALES'),
 (2,'ASSURANCE ARO','Village des Jeux Ankorondrano','sales@aro.mg','2255793','ARO SALES');
/*!40000 ALTER TABLE `w_customer` ENABLE KEYS */;


--
-- Definition of table `w_proposition_proforma`
--

DROP TABLE IF EXISTS `w_proposition_proforma`;
CREATE TABLE `w_proposition_proforma` (
  `proposition_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `price_unit` float NOT NULL DEFAULT '0',
  `qty` float NOT NULL DEFAULT '0',
  `currency_fk` int(10) unsigned NOT NULL DEFAULT '1',
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_proposition_proforma`
--

/*!40000 ALTER TABLE `w_proposition_proforma` DISABLE KEYS */;
INSERT INTO `w_proposition_proforma` (`proposition_proforma_id`,`designation`,`price_unit`,`qty`,`currency_fk`,`cotation_fk`) VALUES 
 (1,'Acer Aspire Core i3',1000000,5,1,1),
 (2,'Table ordi IKEA',500000,5,1,1),
 (5,'Connecteur RJ45',500,10,1,2),
 (6,'Cable cat 6 (metres)',1000,300,1,2),
 (7,'Routeur Cisco Prolink',260000,5,1,3),
 (8,'Switch 16 ports Base 100 D-Link',230000,8,1,3),
 (9,'Prise parafoudre 5 ports',10000,10,1,4),
 (10,'Rupteur 2A Tichka',50000,5,1,4),
 (11,'Ecran LCD 22\"',230000,10,1,6),
 (12,'UC Core i3 Ram 2Go HDD 250Go',300000,10,1,6),
 (13,'Clavier FR',10000,10,1,6),
 (14,'Souris optique 120 dpi',5000,10,1,6),
 (15,'Connecteur RJ45 blindé',1000,50,1,5),
 (16,'Cable cat 6 (metres)',200000,300,1,5),
 (17,'Switch D-Link Base1000',300000,8,1,5),
 (18,'Camera IP',100000,2,1,9),
 (19,'Cable RJ45 Cat 6 (rouleau de 100m)',100000,2,1,9),
 (20,'Connecteurs RJ45 blindé',1000,10,1,9),
 (21,'Carte mère socket LGA775 Gigabyte',150000,10,1,10),
 (22,'RAM DDR3 2Go PC2700 Corsair',50000,20,1,10),
 (23,'HDD 1Tera SATA2 Seagate Barracuda',150000,10,1,10),
 (24,'Alimentation UC 500W',230000,10,1,10),
 (25,'Boitier Grand Tour',60000,10,1,10);
/*!40000 ALTER TABLE `w_proposition_proforma` ENABLE KEYS */;


--
-- Definition of table `w_rel_account_cotation`
--

DROP TABLE IF EXISTS `w_rel_account_cotation`;
CREATE TABLE `w_rel_account_cotation` (
  `account_id` int(10) unsigned NOT NULL,
  `cotation_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`account_id`,`cotation_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_rel_account_cotation`
--

/*!40000 ALTER TABLE `w_rel_account_cotation` DISABLE KEYS */;
INSERT INTO `w_rel_account_cotation` (`account_id`,`cotation_id`) VALUES 
 (5,1),
 (5,2),
 (5,3),
 (5,4),
 (5,5),
 (5,6),
 (5,7),
 (5,8),
 (5,9),
 (5,10),
 (6,1),
 (6,2),
 (6,7),
 (6,8),
 (6,9),
 (7,3),
 (7,4),
 (7,5),
 (7,6),
 (7,10);
/*!40000 ALTER TABLE `w_rel_account_cotation` ENABLE KEYS */;


--
-- Definition of table `w_request_proforma`
--

DROP TABLE IF EXISTS `w_request_proforma`;
CREATE TABLE `w_request_proforma` (
  `request_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_request_proforma`
--

/*!40000 ALTER TABLE `w_request_proforma` DISABLE KEYS */;
INSERT INTO `w_request_proforma` (`request_proforma_id`,`designation`,`qty`,`cotation_fk`) VALUES 
 (1,'Ordinateur portable Core i3',5,1),
 (2,'Table ordinateur',5,1),
 (3,'Connecteur RJ45',10,2),
 (4,'Cable cat 6 (metres)',300,2),
 (5,'Routeur Cisco',5,3),
 (6,'Switch 16 ports Base 100',8,3),
 (7,'Prise parafoudre 5 ports',10,4),
 (8,'Rupteur 2A',5,4),
 (9,'Connecteur RJ45 blindé',50,5),
 (10,'Cable cat 6 (metres)',300,5),
 (11,'Switch D-Link Base1000',8,5),
 (12,'Ecran LCD 22\"',10,6),
 (13,'UC Core i3 Ram 2Go HDD 250Go',10,6),
 (14,'Clavier FR',10,6),
 (15,'Souris optique 120 dpi',10,6),
 (16,'Modem routeur 6 ports',5,7),
 (17,'Pinces a sertir RJ45',5,7),
 (18,'Cable RJ45 Cat6 (metres)',300,7),
 (19,'Connecteur RJ45',100,7),
 (20,'Switch base 100',10,7),
 (21,'Camera IP',5,8),
 (22,'UC Serveur 8Go RAM HDD 2Tera',2,8),
 (23,'Camera IP',2,9),
 (24,'Cable RJ45 Cat 6 (rouleau de 100m)',2,9),
 (25,'Connecteurs RJ45 blindé',10,9),
 (26,'Carte mère socket LGA775',10,10),
 (27,'RAM DDR3 2Go PC3200',20,10),
 (28,'HDD 1Tera SATA2',10,10),
 (29,'Alimentation UC 500W',10,10),
 (30,'Boitier Grand Tour',10,10);
/*!40000 ALTER TABLE `w_request_proforma` ENABLE KEYS */;


--
-- Definition of table `w_sessions`
--

DROP TABLE IF EXISTS `w_sessions`;
CREATE TABLE `w_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_sessions`
--

/*!40000 ALTER TABLE `w_sessions` DISABLE KEYS */;
INSERT INTO `w_sessions` (`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) VALUES 
 ('06d8ed9f7d65b61278e27036de550682','127.0.0.1','Mozilla/5.0 (Windows NT 5.1; rv:12.0) Gecko/20100101 Firefox/12.0',1353229734,'a:4:{s:10:\"user_login\";s:5:\"rasoa\";s:14:\"user_firstname\";s:5:\"Rasoa\";s:9:\"user_type\";s:10:\"COMMERCIAL\";s:10:\"account_id\";s:1:\"6\";}');
/*!40000 ALTER TABLE `w_sessions` ENABLE KEYS */;


--
-- Definition of view `wv_cotations`
--

DROP TABLE IF EXISTS `wv_cotations`;
DROP VIEW IF EXISTS `wv_cotations`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_cotations` AS select `ac`.`account_id` AS `account_id`,`ct`.`cotation_id` AS `cotation_id`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`cs`.`customer_name` AS `customer_name`,`ac`.`first_name` AS `commercial_firstname`,`ac`.`last_name` AS `commercial_lastname`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end` from (((`w_cotation` `ct` join `w_account` `ac`) join `w_rel_account_cotation` `rl`) join `w_customer` `cs`) where ((`ct`.`cotation_id` = `rl`.`cotation_id`) and (`rl`.`account_id` = `ac`.`account_id`) and (`ct`.`customer_fk` = `cs`.`customer_id`));



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
