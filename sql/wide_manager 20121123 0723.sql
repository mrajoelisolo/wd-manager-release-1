-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.20-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema wd_manager
--

CREATE DATABASE IF NOT EXISTS wd_manager;
USE wd_manager;

--
-- Temporary table structure for view `wv_cotations`
--
DROP TABLE IF EXISTS `wv_cotations`;
DROP VIEW IF EXISTS `wv_cotations`;
CREATE TABLE `wv_cotations` (
  `account_id` int(11),
  `cotation_id` int(10) unsigned,
  `cotation_number` varchar(45),
  `cotation_number_customer` varchar(45),
  `customer_name` varchar(45),
  `commercial_firstname` varchar(45),
  `commercial_lastname` varchar(45),
  `user_type` enum('ADMIN','DISPATCHER','COMMERCIAL'),
  `dispatcher_fk` int(11),
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED'),
  `reception_date` date,
  `limit_send_date` date,
  `validity_start` date,
  `validity_end` date
);

--
-- Temporary table structure for view `wv_relance`
--
DROP TABLE IF EXISTS `wv_relance`;
DROP VIEW IF EXISTS `wv_relance`;
CREATE TABLE `wv_relance` (
  `account_id` int(11),
  `cotation_id` int(10) unsigned,
  `cotation_number` varchar(45),
  `cotation_number_customer` varchar(45),
  `customer_name` varchar(45),
  `commercial_firstname` varchar(45),
  `commercial_lastname` varchar(45),
  `user_type` enum('ADMIN','DISPATCHER','COMMERCIAL'),
  `dispatcher_fk` int(11),
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED'),
  `reception_date` date,
  `limit_send_date` date,
  `validity_start` date,
  `validity_end` date,
  `duration` int(7),
  `date1` date,
  `date2` date,
  `date3` date
);

--
-- Definition of table `w_account`
--

DROP TABLE IF EXISTS `w_account`;
CREATE TABLE `w_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) CHARACTER SET latin1 NOT NULL,
  `pwd` varchar(40) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_type` enum('ADMIN','DISPATCHER','COMMERCIAL') CHARACTER SET latin1 NOT NULL DEFAULT 'COMMERCIAL',
  `account_state` enum('ACTIVATED','DISABLED') CHARACTER SET latin1 NOT NULL DEFAULT 'DISABLED',
  `acronym` varchar(5) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_account`
--

/*!40000 ALTER TABLE `w_account` DISABLE KEYS */;
INSERT INTO `w_account` (`account_id`,`login`,`pwd`,`first_name`,`last_name`,`user_type`,`account_state`,`acronym`) VALUES 
 (1,'dispatcher','bdf70eff0e4d79093bd5f318014dd13348b89cdb','RAKOTO','A','DISPATCHER','ACTIVATED','RAK'),
 (2,'commercial1 ','3cec6cf175c2438291042a7a392a0c71090d209b','RASOA','A','COMMERCIAL','ACTIVATED','RAS'),
 (3,'commercial2','e3581bfd855e281d7d258a8cea9a921699a20ed9','RAJEAN','C','COMMERCIAL','ACTIVATED','JN');
/*!40000 ALTER TABLE `w_account` ENABLE KEYS */;


--
-- Definition of table `w_cotation`
--

DROP TABLE IF EXISTS `w_cotation`;
CREATE TABLE `w_cotation` (
  `cotation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHONE','PHYSICAL') CHARACTER SET latin1 NOT NULL DEFAULT 'MAIL',
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') CHARACTER SET latin1 NOT NULL DEFAULT 'PENDING',
  `customer_fk` int(10) unsigned NOT NULL,
  `cotation_number_customer` varchar(45) NOT NULL,
  `cotation_number` varchar(45) NOT NULL,
  `dispatcher_fk` int(11) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_cotation`
--

/*!40000 ALTER TABLE `w_cotation` DISABLE KEYS */;
INSERT INTO `w_cotation` (`cotation_id`,`cotation_source`,`reception_date`,`limit_send_date`,`validity_start`,`validity_end`,`cotation_status`,`customer_fk`,`cotation_number_customer`,`cotation_number`,`dispatcher_fk`) VALUES 
 (1,'MAIL','2012-11-21','2012-11-23','2012-11-19','2012-12-03','SENDED',1,'COT_001_A','PRF_001_A',1),
 (2,'MAIL','2012-10-19','2012-11-23','2012-11-21','2012-12-21','SENDED',2,'COT_002_A','PRF_002_A',1);
/*!40000 ALTER TABLE `w_cotation` ENABLE KEYS */;


--
-- Definition of table `w_currency`
--

DROP TABLE IF EXISTS `w_currency`;
CREATE TABLE `w_currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_acronym` varchar(5) CHARACTER SET latin1 NOT NULL,
  `currency_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_currency`
--

/*!40000 ALTER TABLE `w_currency` DISABLE KEYS */;
INSERT INTO `w_currency` (`currency_id`,`currency_acronym`,`currency_name`) VALUES 
 (1,'MGA','Malagasy Ariary');
/*!40000 ALTER TABLE `w_currency` ENABLE KEYS */;


--
-- Definition of table `w_customer`
--

DROP TABLE IF EXISTS `w_customer`;
CREATE TABLE `w_customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `customer_mail` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_phone` varchar(45) CHARACTER SET latin1 NOT NULL,
  `purchaser_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_customer`
--

/*!40000 ALTER TABLE `w_customer` DISABLE KEYS */;
INSERT INTO `w_customer` (`customer_id`,`customer_name`,`customer_address`,`customer_mail`,`customer_phone`,`purchaser_name`) VALUES 
 (1,'ECRS','Ambohimanarina Anosivavaka Route du Pape','sales@ecrs.mg','2261958','Eddie Sam Lock'),
 (2,'ADEMA','Ivato aéroport','sales@adema.mg','2256879','Henri');
/*!40000 ALTER TABLE `w_customer` ENABLE KEYS */;


--
-- Definition of table `w_proposition_proforma`
--

DROP TABLE IF EXISTS `w_proposition_proforma`;
CREATE TABLE `w_proposition_proforma` (
  `proposition_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `price_unit` float NOT NULL DEFAULT '0',
  `qty` float NOT NULL DEFAULT '0',
  `currency_fk` int(10) unsigned NOT NULL DEFAULT '1',
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_proposition_proforma`
--

/*!40000 ALTER TABLE `w_proposition_proforma` DISABLE KEYS */;
INSERT INTO `w_proposition_proforma` (`proposition_proforma_id`,`designation`,`price_unit`,`qty`,`currency_fk`,`cotation_fk`) VALUES 
 (1,'Camera IP',100000,5,1,1),
 (2,'Cable RJ45 Cat 6 (rouleau de 100m)',20000,2,1,1),
 (3,'Portable Core i3 Toshiba Satellite',1500000,2,1,2),
 (4,'Ecran LCD 22\" LG',200000,2,1,2);
/*!40000 ALTER TABLE `w_proposition_proforma` ENABLE KEYS */;


--
-- Definition of table `w_rel_account_cotation`
--

DROP TABLE IF EXISTS `w_rel_account_cotation`;
CREATE TABLE `w_rel_account_cotation` (
  `account_id` int(10) unsigned NOT NULL,
  `cotation_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`account_id`,`cotation_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_rel_account_cotation`
--

/*!40000 ALTER TABLE `w_rel_account_cotation` DISABLE KEYS */;
INSERT INTO `w_rel_account_cotation` (`account_id`,`cotation_id`) VALUES 
 (2,1),
 (2,2);
/*!40000 ALTER TABLE `w_rel_account_cotation` ENABLE KEYS */;


--
-- Definition of table `w_request_proforma`
--

DROP TABLE IF EXISTS `w_request_proforma`;
CREATE TABLE `w_request_proforma` (
  `request_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_request_proforma`
--

/*!40000 ALTER TABLE `w_request_proforma` DISABLE KEYS */;
INSERT INTO `w_request_proforma` (`request_proforma_id`,`designation`,`qty`,`cotation_fk`) VALUES 
 (1,'Camera IP',5,1),
 (2,'Cable RJ45 Cat 6 (rouleau de 100m)',2,1),
 (3,'Portable Core i3',2,2),
 (4,'Ecran LCD 21\"0',2,2);
/*!40000 ALTER TABLE `w_request_proforma` ENABLE KEYS */;


--
-- Definition of table `w_sessions`
--

DROP TABLE IF EXISTS `w_sessions`;
CREATE TABLE `w_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_sessions`
--

/*!40000 ALTER TABLE `w_sessions` DISABLE KEYS */;
INSERT INTO `w_sessions` (`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) VALUES 
 ('06ac2a50881890c76c806b878b0d90e8','127.0.0.1','Mozilla/5.0 (Windows NT 5.1; rv:12.0) Gecko/20100101 Firefox/12.0',1353644347,'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"user_login\";s:11:\"commercial1\";s:14:\"user_firstname\";s:5:\"RASOA\";s:9:\"user_type\";s:10:\"COMMERCIAL\";s:10:\"account_id\";s:1:\"2\";}'),
 ('27ae0f62cd0906a7e97fee9994054f87','127.0.0.1','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4',1353644413,''),
 ('a04427c601e725ba5404b7496e1fd98b','127.0.0.1','Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4',1353639493,'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"user_login\";s:10:\"dispatcher\";s:14:\"user_firstname\";s:6:\"RAKOTO\";s:9:\"user_type\";s:10:\"DISPATCHER\";s:10:\"account_id\";s:1:\"1\";}');
/*!40000 ALTER TABLE `w_sessions` ENABLE KEYS */;


--
-- Definition of view `wv_cotations`
--

DROP TABLE IF EXISTS `wv_cotations`;
DROP VIEW IF EXISTS `wv_cotations`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_cotations` AS select `ac`.`account_id` AS `account_id`,`ct`.`cotation_id` AS `cotation_id`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`cs`.`customer_name` AS `customer_name`,`ac`.`first_name` AS `commercial_firstname`,`ac`.`last_name` AS `commercial_lastname`,`ac`.`user_type` AS `user_type`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end` from (((`w_cotation` `ct` join `w_account` `ac`) join `w_rel_account_cotation` `rl`) join `w_customer` `cs`) where ((`ct`.`cotation_id` = `rl`.`cotation_id`) and (`rl`.`account_id` = `ac`.`account_id`) and (`ct`.`customer_fk` = `cs`.`customer_id`));

--
-- Definition of view `wv_relance`
--

DROP TABLE IF EXISTS `wv_relance`;
DROP VIEW IF EXISTS `wv_relance`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_relance` AS select `ac`.`account_id` AS `account_id`,`ct`.`cotation_id` AS `cotation_id`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`cs`.`customer_name` AS `customer_name`,`ac`.`first_name` AS `commercial_firstname`,`ac`.`last_name` AS `commercial_lastname`,`ac`.`user_type` AS `user_type`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end`,(to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) AS `duration`,(`ct`.`validity_start` + interval ((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) / 3) day) AS `date1`,(`ct`.`validity_start` + interval (((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) * 2) / 3) day) AS `date2`,(`ct`.`validity_start` + interval (to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) day) AS `date3` from (((`w_cotation` `ct` join `w_account` `ac`) join `w_rel_account_cotation` `rl`) join `w_customer` `cs`) where ((`ct`.`cotation_id` = `rl`.`cotation_id`) and (`rl`.`account_id` = `ac`.`account_id`) and (`ct`.`customer_fk` = `cs`.`customer_id`));



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
