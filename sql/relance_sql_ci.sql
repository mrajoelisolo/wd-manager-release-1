SELECT *,
DATE_FORMAT(ct.date1, '%w') as wday1,
DATE_FORMAT(ct.date2, '%w') as wday2,
DATE_FORMAT(ct.date3, '%w') as wday3
FROM `wd_manager`.`wv_relance` as ct
WHERE CURDATE() BETWEEN ct.validity_start AND ct.validity_end
AND ct.dispatcher_fk = 1
/*AND ct.cotation_status = 'SENDED'*/
AND 
(
    (
        DATE_FORMAT(CURDATE(), '%w') BETWEEN 1 AND 5 AND 
        CURDATE() IN (ct.date1, ct.date2, ct.date3)
    )
    OR
    (
        (
            DATE_FORMAT(ct.date1, '%w') = 0 AND
            CURDATE() BETWEEN ct.date1 AND DATE_ADD(ct.date1, INTERVAL 1 DAY)
        )
        OR
        (
            DATE_FORMAT(ct.date2, '%w') = 0 AND
            CURDATE() BETWEEN ct.date2 AND DATE_ADD(ct.date2, INTERVAL 1 DAY)
        )
        OR
        (
            DATE_FORMAT(ct.date2, '%w') = 0 AND
            CURDATE() BETWEEN ct.date3 AND DATE_ADD(ct.date3, INTERVAL 1 DAY)
        )
    )
    OR
    (
        (
            DATE_FORMAT(ct.date1, '%w') = 6 AND
            CURDATE() BETWEEN DATE_SUB(ct.date1, INTERVAL 1 DAY) AND ct.date1
        )
        OR
        (
            DATE_FORMAT(ct.date2, '%w') = 6 AND
            CURDATE() BETWEEN DATE_SUB(ct.date2, INTERVAL 1 DAY) AND ct.date2
        )
        OR
        (
            DATE_FORMAT(ct.date3, '%w') = 6 AND
            CURDATE() BETWEEN DATE_SUB(ct.date3, INTERVAL 1 DAY) AND ct.date3
        )
    )
    OR ct.duration < 14
);