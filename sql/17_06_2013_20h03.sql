/*
SQLyog Ultimate v9.10 
MySQL - 5.5.20-log : Database - wide_manager
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wide_manager` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `wide_manager`;

/*Table structure for table `w_account` */

DROP TABLE IF EXISTS `w_account`;

CREATE TABLE `w_account` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_type` enum('ADMIN','COMMERCIAL','DISPATCHER') NOT NULL,
  `acronym` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `w_account` */

insert  into `w_account`(`account_id`,`login`,`pwd`,`first_name`,`last_name`,`user_type`,`acronym`) values (4,'superadmin','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','Super','Admin','ADMIN','ADMN'),(8,'dispatcher','bdf70eff0e4d79093bd5f318014dd13348b89cdb','D','Dispatcher','DISPATCHER','DS'),(9,'commercial1','3cec6cf175c2438291042a7a392a0c71090d209b','C1','Commercial','COMMERCIAL','C1'),(10,'commercial2','e3581bfd855e281d7d258a8cea9a921699a20ed9','C2','Commercial','COMMERCIAL','C2');

/*Table structure for table `w_cotation` */

DROP TABLE IF EXISTS `w_cotation`;

CREATE TABLE `w_cotation` (
  `cotation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHYSICAL','PHONE') NOT NULL,
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') NOT NULL,
  `cotation_number_customer` varchar(45) DEFAULT NULL,
  `cotation_number` varchar(45) NOT NULL,
  `customer_fk` int(11) NOT NULL,
  `dispatcher_fk` int(11) NOT NULL,
  `commercial_fk` int(11) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `w_cotation` */

insert  into `w_cotation`(`cotation_id`,`cotation_source`,`reception_date`,`limit_send_date`,`validity_start`,`validity_end`,`cotation_status`,`cotation_number_customer`,`cotation_number`,`customer_fk`,`dispatcher_fk`,`commercial_fk`) values (5,'MAIL','2013-06-17','2013-06-19','2013-06-17','2013-07-02','SENDED','','5/C1/WD/13',4,8,9);

/*Table structure for table `w_customer` */

DROP TABLE IF EXISTS `w_customer`;

CREATE TABLE `w_customer` (
  `customer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_mail` varchar(45) NOT NULL,
  `customer_phone` varchar(45) NOT NULL,
  `purchaser_name` varchar(45) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `w_customer` */

insert  into `w_customer`(`customer_id`,`customer_name`,`customer_address`,`customer_mail`,`customer_phone`,`purchaser_name`) values (1,'ADEMA','Ivato Aeroport','sales@adema.mg','22457636','RANDRIA Nary'),(2,'EDELEC','Andraharo','sales@edelec.com','2235468','RAJAO'),(3,'BFV-SG','Antaninarenina','sales@bfv.mg','2265487','RAKOTO Nirina'),(4,'AQUAMAD','Anosivavaka','aquamad@sales.mg','022 224 16','RAKOTO Jaona');

/*Table structure for table `w_proposition_proforma` */

DROP TABLE IF EXISTS `w_proposition_proforma`;

CREATE TABLE `w_proposition_proforma` (
  `proposition_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `price_unit` float NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `w_proposition_proforma` */

insert  into `w_proposition_proforma`(`proposition_proforma_id`,`ref`,`designation`,`price_unit`,`qty`,`cotation_fk`) values (13,'','Toshiba Satellite C855-27G',1690000,1,5),(14,'','Dell Vostro 2520',1390000,1,5),(15,'','Lenovo Essentiel B590',1060000,1,5),(16,'','HP 620 - XX842EA',1650000,1,5),(17,'','Acer Aspire V5-571G',2150000,1,5),(18,'','Gateway NS40LX',1700000,1,5);

/*Table structure for table `w_request_proforma` */

DROP TABLE IF EXISTS `w_request_proforma`;

CREATE TABLE `w_request_proforma` (
  `request_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `w_request_proforma` */

insert  into `w_request_proforma`(`request_proforma_id`,`ref`,`designation`,`qty`,`cotation_fk`) values (13,'','Toshiba Satellite C855-27G',1,5),(14,'','Dell Vostro 2520',1,5),(15,'','Lenovo Essentiel B590',1,5),(16,'','HP 620 - XX842EA',1,5),(17,'','Acer Aspire V5-571G',1,5),(18,'','Gateway NS40LX',1,5);

/*Table structure for table `w_sessions` */

DROP TABLE IF EXISTS `w_sessions`;

CREATE TABLE `w_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `w_sessions` */

insert  into `w_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('2a19bd3b02f0ec5a7ac5b4c84684cded','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0',1371488539,'');

/*Table structure for table `wv_proposition_proforma` */

DROP TABLE IF EXISTS `wv_proposition_proforma`;

/*!50001 DROP VIEW IF EXISTS `wv_proposition_proforma` */;
/*!50001 DROP TABLE IF EXISTS `wv_proposition_proforma` */;

/*!50001 CREATE TABLE  `wv_proposition_proforma`(
 `proposition_proforma_id` bigint(20) unsigned ,
 `ref` varchar(45) ,
 `designation` varchar(2048) ,
 `price_unit` float ,
 `qty` float ,
 `cotation_fk` int(11) ,
 `amount` double 
)*/;

/*Table structure for table `wv_relance_cotation` */

DROP TABLE IF EXISTS `wv_relance_cotation`;

/*!50001 DROP VIEW IF EXISTS `wv_relance_cotation` */;
/*!50001 DROP TABLE IF EXISTS `wv_relance_cotation` */;

/*!50001 CREATE TABLE  `wv_relance_cotation`(
 `cotation_id` bigint(20) unsigned ,
 `customer_name` varchar(45) ,
 `customer_address` varchar(255) ,
 `customer_mail` varchar(45) ,
 `customer_phone` varchar(45) ,
 `purchaser_name` varchar(45) ,
 `commercial_firstname` varchar(45) ,
 `commercial_lastname` varchar(45) ,
 `commercial_acronym` varchar(5) ,
 `cotation_source` enum('MAIL','PHYSICAL','PHONE') ,
 `reception_date` date ,
 `limit_send_date` date ,
 `validity_start` date ,
 `validity_end` date ,
 `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') ,
 `cotation_number_customer` varchar(45) ,
 `cotation_number` varchar(45) ,
 `customer_fk` int(11) ,
 `dispatcher_fk` int(11) ,
 `commercial_fk` int(11) ,
 `duration` int(7) ,
 `date1` date ,
 `date2` date ,
 `date3` date 
)*/;

/*View structure for view wv_proposition_proforma */

/*!50001 DROP TABLE IF EXISTS `wv_proposition_proforma` */;
/*!50001 DROP VIEW IF EXISTS `wv_proposition_proforma` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_proposition_proforma` AS select `pr`.`proposition_proforma_id` AS `proposition_proforma_id`,`pr`.`ref` AS `ref`,`pr`.`designation` AS `designation`,`pr`.`price_unit` AS `price_unit`,`pr`.`qty` AS `qty`,`pr`.`cotation_fk` AS `cotation_fk`,(`pr`.`price_unit` * `pr`.`qty`) AS `amount` from `w_proposition_proforma` `pr` */;

/*View structure for view wv_relance_cotation */

/*!50001 DROP TABLE IF EXISTS `wv_relance_cotation` */;
/*!50001 DROP VIEW IF EXISTS `wv_relance_cotation` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_relance_cotation` AS select `ct`.`cotation_id` AS `cotation_id`,`cs`.`customer_name` AS `customer_name`,`cs`.`customer_address` AS `customer_address`,`cs`.`customer_mail` AS `customer_mail`,`cs`.`customer_phone` AS `customer_phone`,`cs`.`purchaser_name` AS `purchaser_name`,`cm`.`first_name` AS `commercial_firstname`,`cm`.`last_name` AS `commercial_lastname`,`cm`.`acronym` AS `commercial_acronym`,`ct`.`cotation_source` AS `cotation_source`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`customer_fk` AS `customer_fk`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`commercial_fk` AS `commercial_fk`,(to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) AS `duration`,(`ct`.`validity_start` + interval ((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) / 3) day) AS `date1`,(`ct`.`validity_start` + interval (((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) * 2) / 3) day) AS `date2`,(`ct`.`validity_start` + interval (to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) day) AS `date3` from ((`w_cotation` `ct` join `w_customer` `cs`) join `w_account` `cm`) where ((`ct`.`customer_fk` = `cs`.`customer_id`) and (`ct`.`commercial_fk` = `cm`.`account_id`)) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
