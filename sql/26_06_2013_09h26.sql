-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.20-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema wide_manager
--

CREATE DATABASE IF NOT EXISTS wide_manager;
USE wide_manager;

--
-- Temporary table structure for view `wv_customer`
--
DROP TABLE IF EXISTS `wv_customer`;
DROP VIEW IF EXISTS `wv_customer`;
CREATE TABLE `wv_customer` (
  `customer_id` bigint(20) unsigned,
  `customer_mail` varchar(45),
  `customer_phone` varchar(45),
  `purchaser_name` varchar(45),
  `enterprise_fk` bigint(20),
  `enterprise_id` bigint(20),
  `enterprise_name` varchar(255),
  `enterprise_address` varchar(255)
);

--
-- Temporary table structure for view `wv_proposition_proforma`
--
DROP TABLE IF EXISTS `wv_proposition_proforma`;
DROP VIEW IF EXISTS `wv_proposition_proforma`;
CREATE TABLE `wv_proposition_proforma` (
  `proposition_proforma_id` bigint(20) unsigned,
  `ref` varchar(45),
  `designation` varchar(2048),
  `price_unit` float,
  `qty` float,
  `cotation_fk` int(11),
  `amount` double
);

--
-- Temporary table structure for view `wv_relance_cotation`
--
DROP TABLE IF EXISTS `wv_relance_cotation`;
DROP VIEW IF EXISTS `wv_relance_cotation`;
CREATE TABLE `wv_relance_cotation` (
  `cotation_id` bigint(20) unsigned,
  `customer_name` varchar(255),
  `customer_address` varchar(255),
  `customer_mail` varchar(45),
  `customer_phone` varchar(45),
  `purchaser_name` varchar(45),
  `commercial_firstname` varchar(45),
  `commercial_lastname` varchar(45),
  `commercial_acronym` varchar(5),
  `cotation_source` enum('MAIL','PHYSICAL','PHONE'),
  `reception_date` date,
  `limit_send_date` date,
  `validity_start` date,
  `validity_end` date,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED'),
  `cotation_number_customer` varchar(45),
  `cotation_number` varchar(45),
  `customer_fk` int(11),
  `dispatcher_fk` int(11),
  `commercial_fk` int(11),
  `duration` int(7),
  `date1` date,
  `date2` date,
  `date3` date
);

--
-- Definition of table `w_account`
--

DROP TABLE IF EXISTS `w_account`;
CREATE TABLE `w_account` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_type` enum('ADMIN','COMMERCIAL','DISPATCHER') NOT NULL,
  `acronym` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_account`
--

/*!40000 ALTER TABLE `w_account` DISABLE KEYS */;
INSERT INTO `w_account` (`account_id`,`login`,`pwd`,`first_name`,`last_name`,`user_type`,`acronym`) VALUES 
 (4,'superadmin','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','Super','Admin','ADMIN','ADMN'),
 (8,'dispatcher','bdf70eff0e4d79093bd5f318014dd13348b89cdb','Jean','RABE','DISPATCHER','JN'),
 (9,'commercial1','3cec6cf175c2438291042a7a392a0c71090d209b','Jeanne','RASOA','COMMERCIAL','RAS'),
 (10,'commercial2','e3581bfd855e281d7d258a8cea9a921699a20ed9','Andre','RAJAO','COMMERCIAL','RAJ'),
 (11,'test','a94a8fe5ccb19ba61c4c0873d391e987982fbbd3','test','test','DISPATCHER','test');
/*!40000 ALTER TABLE `w_account` ENABLE KEYS */;


--
-- Definition of table `w_cotation`
--

DROP TABLE IF EXISTS `w_cotation`;
CREATE TABLE `w_cotation` (
  `cotation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHYSICAL','PHONE') NOT NULL,
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') NOT NULL,
  `cotation_number_customer` varchar(45) DEFAULT NULL,
  `cotation_number` varchar(45) NOT NULL,
  `customer_fk` int(11) NOT NULL,
  `dispatcher_fk` int(11) NOT NULL,
  `commercial_fk` int(11) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_cotation`
--

/*!40000 ALTER TABLE `w_cotation` DISABLE KEYS */;
INSERT INTO `w_cotation` (`cotation_id`,`cotation_source`,`reception_date`,`limit_send_date`,`validity_start`,`validity_end`,`cotation_status`,`cotation_number_customer`,`cotation_number`,`customer_fk`,`dispatcher_fk`,`commercial_fk`) VALUES 
 (5,'MAIL','2013-06-17','2013-06-19','2013-06-17','2013-07-02','ACCEPTED','','5/C1/WD/13',4,8,9),
 (6,'PHONE','2013-06-18','2013-06-18','2013-06-18','2013-07-03','SENDED','','6/C1/WD/13',1,8,9),
 (7,'PHONE','2013-06-19','2013-06-19','2013-06-19','2013-07-04','SENDED','','7/RAS/WD/13',1,8,9),
 (8,'PHONE','2013-06-25','2013-06-25','2013-06-25','2013-07-10','SENDED','','8/RAS/WD/13',6,8,9),
 (9,'PHONE','2013-06-25','2013-06-25','2013-06-25','2013-07-10','SENDED','','9/RAS/WD/13',7,8,9),
 (10,'PHONE','2013-06-25','2013-06-25','2013-06-25','2013-07-10','ACCEPTED','','10/RAJ/WD/13',8,8,10);
/*!40000 ALTER TABLE `w_cotation` ENABLE KEYS */;


--
-- Definition of table `w_customer`
--

DROP TABLE IF EXISTS `w_customer`;
CREATE TABLE `w_customer` (
  `customer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_mail` varchar(45) NOT NULL,
  `customer_phone` varchar(45) NOT NULL,
  `purchaser_name` varchar(45) NOT NULL,
  `enterprise_fk` bigint(20) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_customer`
--

/*!40000 ALTER TABLE `w_customer` DISABLE KEYS */;
INSERT INTO `w_customer` (`customer_id`,`customer_mail`,`customer_phone`,`purchaser_name`,`enterprise_fk`) VALUES 
 (1,'sales@adema.mg','22457636','RANDRIA Nary',1),
 (2,'sales@edelec.com','2235468','RAJAO',2),
 (3,'sales@bfv.mg','2265487','RAKOTO Nirina',3),
 (4,'aquamad@sales.mg','022 224 16','RAKOTO Jaona',4),
 (5,'technical@adema.mg','2225465','RAJAONARY Adrien',1),
 (6,'rabary@gmail.com','2265456','RABARY',1),
 (7,'rasoa@gmail.com','2265875','RASOA',2),
 (8,'ranary@yahoo.fr','2265478','RANARY',2);
/*!40000 ALTER TABLE `w_customer` ENABLE KEYS */;


--
-- Definition of table `w_enterprise`
--

DROP TABLE IF EXISTS `w_enterprise`;
CREATE TABLE `w_enterprise` (
  `enterprise_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_name` varchar(255) NOT NULL,
  `enterprise_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`enterprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_enterprise`
--

/*!40000 ALTER TABLE `w_enterprise` DISABLE KEYS */;
INSERT INTO `w_enterprise` (`enterprise_id`,`enterprise_name`,`enterprise_address`) VALUES 
 (1,'ADEMA','Ivato Aeroport'),
 (2,'EDELEC','Andraharo'),
 (3,'BFV-SG','Antaninarenina'),
 (4,'ETECH','Anosisoa');
/*!40000 ALTER TABLE `w_enterprise` ENABLE KEYS */;


--
-- Definition of table `w_proposition_proforma`
--

DROP TABLE IF EXISTS `w_proposition_proforma`;
CREATE TABLE `w_proposition_proforma` (
  `proposition_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `price_unit` float NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_proposition_proforma`
--

/*!40000 ALTER TABLE `w_proposition_proforma` DISABLE KEYS */;
INSERT INTO `w_proposition_proforma` (`proposition_proforma_id`,`ref`,`designation`,`price_unit`,`qty`,`cotation_fk`) VALUES 
 (13,'','Toshiba Satellite C855-27G',1690000,1,5),
 (14,'','Dell Vostro 2520',1390000,1,5),
 (15,'','Lenovo Essentiel B590',1060000,1,5),
 (16,'','HP 620 - XX842EA',1650000,1,5),
 (17,'','Acer Aspire V5-571G',2150000,1,5),
 (18,'','Gateway NS40LX',1700000,1,5),
 (19,'','Onduleur UPS 650Va',130000,5,6),
 (20,'','Onduleur UPS 800Va',210000,2,6),
 (21,'','test',1750000,2,7),
 (22,'','test45',150000,5,9),
 (23,'','test14',320000,4,9),
 (24,'','test1',1500000,1,8),
 (25,'','test2',3200000,2,8),
 (26,'','Toshiba Satellite',1200000,2,10),
 (27,'','Acer Aspire 1640',1500000,2,10),
 (28,'','HP Turion',2000000,1,10);
/*!40000 ALTER TABLE `w_proposition_proforma` ENABLE KEYS */;


--
-- Definition of table `w_request_proforma`
--

DROP TABLE IF EXISTS `w_request_proforma`;
CREATE TABLE `w_request_proforma` (
  `request_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_request_proforma`
--

/*!40000 ALTER TABLE `w_request_proforma` DISABLE KEYS */;
INSERT INTO `w_request_proforma` (`request_proforma_id`,`ref`,`designation`,`qty`,`cotation_fk`) VALUES 
 (13,'','Toshiba Satellite C855-27G',1,5),
 (14,'','Dell Vostro 2520',1,5),
 (15,'','Lenovo Essentiel B590',1,5),
 (16,'','HP 620 - XX842EA',1,5),
 (17,'','Acer Aspire V5-571G',1,5),
 (18,'','Gateway NS40LX',1,5),
 (19,'','Onduleur UPS 650Va',5,6),
 (20,'','Onduleur UPS 800Va',2,6),
 (21,'','test',1,7),
 (22,'','test1',1,8),
 (23,'','test2',2,8),
 (24,'','test45',5,9),
 (25,'','test14',4,9),
 (26,'','Toshiba Satellite',2,10),
 (27,'','Acer Aspire 1640',2,10),
 (28,'','HP Turion',1,10);
/*!40000 ALTER TABLE `w_request_proforma` ENABLE KEYS */;


--
-- Definition of table `w_sessions`
--

DROP TABLE IF EXISTS `w_sessions`;
CREATE TABLE `w_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `w_sessions`
--

/*!40000 ALTER TABLE `w_sessions` DISABLE KEYS */;
INSERT INTO `w_sessions` (`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) VALUES 
 ('6681accd480f1f707eb3b710f1ee03ec','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0',1372136155,''),
 ('9f3df48ae4a4834eaf7133587a6d39d2','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0',1372136862,'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"user_login\";s:11:\"commercial1\";s:14:\"user_firstname\";s:6:\"Jeanne\";s:9:\"user_type\";s:10:\"COMMERCIAL\";s:10:\"account_id\";s:1:\"9\";}'),
 ('1c614efd54df46d27b0c8d3427538ce5','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0',1372141196,'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"user_login\";s:10:\"dispatcher\";s:14:\"user_firstname\";s:4:\"Jean\";s:9:\"user_type\";s:10:\"DISPATCHER\";s:10:\"account_id\";s:1:\"8\";}'),
 ('eb078472cf40f662fddc96c43b0c5767','127.0.0.1','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36',1372134486,'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"user_login\";s:10:\"dispatcher\";s:14:\"user_firstname\";s:4:\"Jean\";s:9:\"user_type\";s:10:\"DISPATCHER\";s:10:\"account_id\";s:1:\"8\";}');
/*!40000 ALTER TABLE `w_sessions` ENABLE KEYS */;


--
-- Definition of view `wv_customer`
--

DROP TABLE IF EXISTS `wv_customer`;
DROP VIEW IF EXISTS `wv_customer`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_customer` AS (select `c`.`customer_id` AS `customer_id`,`c`.`customer_mail` AS `customer_mail`,`c`.`customer_phone` AS `customer_phone`,`c`.`purchaser_name` AS `purchaser_name`,`c`.`enterprise_fk` AS `enterprise_fk`,`t`.`enterprise_id` AS `enterprise_id`,`t`.`enterprise_name` AS `enterprise_name`,`t`.`enterprise_address` AS `enterprise_address` from (`w_customer` `c` join `w_enterprise` `t`) where (`c`.`enterprise_fk` = `t`.`enterprise_id`));

--
-- Definition of view `wv_proposition_proforma`
--

DROP TABLE IF EXISTS `wv_proposition_proforma`;
DROP VIEW IF EXISTS `wv_proposition_proforma`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_proposition_proforma` AS select `pr`.`proposition_proforma_id` AS `proposition_proforma_id`,`pr`.`ref` AS `ref`,`pr`.`designation` AS `designation`,`pr`.`price_unit` AS `price_unit`,`pr`.`qty` AS `qty`,`pr`.`cotation_fk` AS `cotation_fk`,(`pr`.`price_unit` * `pr`.`qty`) AS `amount` from `w_proposition_proforma` `pr`;

--
-- Definition of view `wv_relance_cotation`
--

DROP TABLE IF EXISTS `wv_relance_cotation`;
DROP VIEW IF EXISTS `wv_relance_cotation`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_relance_cotation` AS select `ct`.`cotation_id` AS `cotation_id`,`cs`.`enterprise_name` AS `customer_name`,`cs`.`enterprise_address` AS `customer_address`,`cs`.`customer_mail` AS `customer_mail`,`cs`.`customer_phone` AS `customer_phone`,`cs`.`purchaser_name` AS `purchaser_name`,`cm`.`first_name` AS `commercial_firstname`,`cm`.`last_name` AS `commercial_lastname`,`cm`.`acronym` AS `commercial_acronym`,`ct`.`cotation_source` AS `cotation_source`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`customer_fk` AS `customer_fk`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`commercial_fk` AS `commercial_fk`,(to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) AS `duration`,(`ct`.`validity_start` + interval ((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) / 3) day) AS `date1`,(`ct`.`validity_start` + interval (((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) * 2) / 3) day) AS `date2`,(`ct`.`validity_start` + interval (to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) day) AS `date3` from ((`w_cotation` `ct` join `wv_customer` `cs`) join `w_account` `cm`) where ((`ct`.`customer_fk` = `cs`.`customer_id`) and (`ct`.`commercial_fk` = `cm`.`account_id`));



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
