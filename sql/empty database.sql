CREATE DATABASE `wd_manager` /*!40100 DEFAULT CHARACTER SET latin1 */;

DROP TABLE IF EXISTS `wd_manager`.`w_account`;
CREATE TABLE  `wd_manager`.`w_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) CHARACTER SET latin1 NOT NULL,
  `pwd` varchar(40) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_type` enum('ADMIN','DISPATCHER','COMMERCIAL') CHARACTER SET latin1 NOT NULL DEFAULT 'COMMERCIAL',
  `account_state` enum('ACTIVATED','DISABLED') CHARACTER SET latin1 NOT NULL DEFAULT 'DISABLED',
  `acronym` varchar(5) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_cotation`;
CREATE TABLE  `wd_manager`.`w_cotation` (
  `cotation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHONE','PHYSICAL') CHARACTER SET latin1 NOT NULL DEFAULT 'MAIL',
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED') CHARACTER SET latin1 NOT NULL DEFAULT 'PENDING',
  `customer_fk` int(10) unsigned NOT NULL,
  `cotation_number_customer` varchar(45) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_currency`;
CREATE TABLE  `wd_manager`.`w_currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency_acronym` varchar(5) NOT NULL,
  `currency_name` varchar(45) NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `wd_manager`.`w_customer`;
CREATE TABLE  `wd_manager`.`w_customer` (
  `customer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `customer_mail` varchar(45) CHARACTER SET latin1 NOT NULL,
  `customer_phone` varchar(45) CHARACTER SET latin1 NOT NULL,
  `purchaser_name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_proposition_proforma`;
CREATE TABLE  `wd_manager`.`w_proposition_proforma` (
  `proposition_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `price_unit` float NOT NULL DEFAULT '0',
  `qty` float NOT NULL DEFAULT '0',
  `currency_fk` int(10) unsigned NOT NULL,
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_rel_account_cotation`;
CREATE TABLE  `wd_manager`.`w_rel_account_cotation` (
  `account_id` int(10) unsigned NOT NULL,
  `cotation_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`account_id`,`cotation_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_request_proforma`;
CREATE TABLE  `wd_manager`.`w_request_proforma` (
  `request_proforma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `cotation_fk` int(10) unsigned NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `wd_manager`.`w_sessions`;
CREATE TABLE  `wd_manager`.`w_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP VIEW IF EXISTS `wd_manager`.`wv_cotations`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW  `wd_manager`.`wv_cotations` AS select `ac`.`account_id` AS `account_id`,`ct`.`cotation_id` AS `cotation_id`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`cs`.`customer_name` AS `customer_name`,`ac`.`first_name` AS `commercial_firstname`,`ac`.`last_name` AS `commercial_lastname`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`reception_date` AS `reception_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end` from (((`w_cotation` `ct` join `w_account` `ac`) join `w_rel_account_cotation` `rl`) join `w_customer` `cs`) where ((`ct`.`cotation_id` = `rl`.`cotation_id`) and (`rl`.`account_id` = `ac`.`account_id`) and (`ct`.`customer_fk` = `cs`.`customer_id`));