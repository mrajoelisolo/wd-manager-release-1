SELECT (TO_DAYS(ct.validity_end) - TO_DAYS(ct.validity_start)) duree,
ct.validity_start,
ct.validity_end,
DATE_ADD(ct.validity_start, INTERVAL (TO_DAYS(ct.validity_end) - TO_DAYS(ct.validity_start)) / 3 DAY) as test1,
DATE_ADD(ct.validity_start, INTERVAL (TO_DAYS(ct.validity_end) - TO_DAYS(ct.validity_start)) * 2 / 3 DAY) as test2,
DATE_ADD(ct.validity_start, INTERVAL (TO_DAYS(ct.validity_end) - TO_DAYS(ct.validity_start)) DAY) as test3
FROM wv_cotations ct
WHERE '2012-11-21' BETWEEN ct.validity_start AND ct.validity_end
AND ct.cotation_status = 'PENDING'
AND (TO_DAYS(ct.validity_end) - TO_DAYS(ct.validity_start) >= 14);