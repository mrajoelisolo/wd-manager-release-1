-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 27 Décembre 2013 à 20:45
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `wide_manager`
--

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `wv_customer`
--
CREATE TABLE IF NOT EXISTS `wv_customer` (
`customer_id` bigint(20) unsigned
,`customer_mail` varchar(45)
,`customer_phone` varchar(45)
,`purchaser_name` varchar(45)
,`enterprise_fk` bigint(20)
,`enterprise_id` bigint(20)
,`enterprise_name` varchar(255)
,`enterprise_address` varchar(255)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `wv_proposition_proforma`
--
CREATE TABLE IF NOT EXISTS `wv_proposition_proforma` (
`proposition_proforma_id` bigint(20) unsigned
,`ref` varchar(45)
,`designation` varchar(2048)
,`price_unit` float
,`qty` float
,`cotation_fk` int(11)
,`amount` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `wv_relance_cotation`
--
CREATE TABLE IF NOT EXISTS `wv_relance_cotation` (
`cotation_id` bigint(20) unsigned
,`customer_name` varchar(255)
,`customer_address` varchar(255)
,`customer_mail` varchar(45)
,`customer_phone` varchar(45)
,`purchaser_name` varchar(45)
,`commercial_firstname` varchar(45)
,`commercial_lastname` varchar(45)
,`commercial_acronym` varchar(5)
,`cotation_source` enum('MAIL','PHYSICAL','PHONE')
,`reception_date` date
,`limit_send_date` date
,`validity_start` date
,`validity_end` date
,`cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED')
,`cotation_number_customer` varchar(45)
,`cotation_number` varchar(45)
,`customer_fk` int(11)
,`dispatcher_fk` int(11)
,`commercial_fk` int(11)
,`duration` int(7)
,`date1` date
,`date2` date
,`date3` date
);
-- --------------------------------------------------------

--
-- Structure de la table `w_account`
--

CREATE TABLE IF NOT EXISTS `w_account` (
  `account_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `pwd` varchar(40) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_type` enum('ADMIN','COMMERCIAL','DISPATCHER') NOT NULL,
  `acronym` varchar(5) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `w_account`
--

INSERT INTO `w_account` (`account_id`, `login`, `pwd`, `first_name`, `last_name`, `user_type`, `acronym`) VALUES
(4, 'superadmin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Super', 'Admin', 'ADMIN', 'ADMN'),
(8, 'dispatcher', 'bdf70eff0e4d79093bd5f318014dd13348b89cdb', 'Jean', 'RABE', 'DISPATCHER', 'JN'),
(9, 'commercial1', '3cec6cf175c2438291042a7a392a0c71090d209b', 'Jeanne', 'RASOA', 'COMMERCIAL', 'RAS'),
(10, 'commercial2', 'e3581bfd855e281d7d258a8cea9a921699a20ed9', 'Andre', 'RAJAO', 'COMMERCIAL', 'RAJ'),
(11, 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test', 'test', 'DISPATCHER', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `w_cotation`
--

CREATE TABLE IF NOT EXISTS `w_cotation` (
  `cotation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cotation_source` enum('MAIL','PHYSICAL','PHONE') NOT NULL,
  `reception_date` date NOT NULL,
  `limit_send_date` date NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `cotation_status` enum('PENDING','SENDED','ACCEPTED','REJECTED') NOT NULL,
  `cotation_number_customer` varchar(45) DEFAULT NULL,
  `cotation_number` varchar(45) NOT NULL,
  `customer_fk` int(11) NOT NULL,
  `dispatcher_fk` int(11) NOT NULL,
  `commercial_fk` int(11) NOT NULL,
  PRIMARY KEY (`cotation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `w_cotation`
--

INSERT INTO `w_cotation` (`cotation_id`, `cotation_source`, `reception_date`, `limit_send_date`, `validity_start`, `validity_end`, `cotation_status`, `cotation_number_customer`, `cotation_number`, `customer_fk`, `dispatcher_fk`, `commercial_fk`) VALUES
(1, 'PHONE', '2013-06-29', '2013-06-29', '2013-06-29', '2013-07-14', 'SENDED', '', '001/RAS/WD/13', 5, 8, 9),
(2, 'PHONE', '2013-06-29', '2013-06-29', '2013-06-29', '2013-07-14', 'SENDED', '', '002/RAS/WD/13', 1, 8, 9),
(3, 'PHONE', '2013-06-29', '2013-06-29', '2013-06-29', '2013-07-14', 'SENDED', '', '003/RAS/WD/13', 4, 8, 9),
(4, 'PHONE', '2013-06-29', '2013-06-29', '2013-06-29', '2013-07-14', 'SENDED', '', '004/RAS/WD/13', 4, 8, 9),
(5, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '005/RAS/WD/13', 4, 8, 9),
(6, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '006/RAS/WD/13', 2, 8, 9),
(7, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '007/RAS/WD/13', 5, 8, 9),
(8, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '008/RAS/WD/13', 6, 8, 9),
(9, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '009/RAS/WD/13', 7, 8, 9),
(10, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '010/RAS/WD/13', 4, 8, 9),
(11, 'PHONE', '2013-06-30', '2013-06-30', '2013-06-30', '2013-07-15', 'SENDED', '', '011/RAS/WD/13', 4, 8, 9),
(12, 'PHONE', '2013-07-01', '2013-07-04', '2013-07-04', '2013-07-20', 'PENDING', '', '', 6, 8, 9),
(13, 'PHONE', '2013-07-03', '2013-07-03', '2013-07-03', '2013-07-18', 'PENDING', '', '', 4, 8, 9);

-- --------------------------------------------------------

--
-- Structure de la table `w_customer`
--

CREATE TABLE IF NOT EXISTS `w_customer` (
  `customer_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_mail` varchar(45) NOT NULL,
  `customer_phone` varchar(45) NOT NULL,
  `purchaser_name` varchar(45) NOT NULL,
  `enterprise_fk` bigint(20) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `w_customer`
--

INSERT INTO `w_customer` (`customer_id`, `customer_mail`, `customer_phone`, `purchaser_name`, `enterprise_fk`) VALUES
(1, 'sales@adema.mg', '22457636', 'RANDRIA Nary', 1),
(2, 'sales@edelec.com', '2235468', 'RAJAO', 2),
(3, 'sales@bfv.mg', '2265487', 'RAKOTO Nirina', 3),
(4, 'aquamad@sales.mg', '022 224 16', 'RAKOTO Jaona', 4),
(5, 'technical@adema.mg', '2225465', 'RAJAONARY Adrien', 1),
(6, 'rabary@gmail.com', '2265456', 'RABARY', 1),
(7, 'rasoa@gmail.com', '2265875', 'RASOA', 2),
(8, 'ranary@yahoo.fr', '2265478', 'RANARY', 2);

-- --------------------------------------------------------

--
-- Structure de la table `w_dispatch_queue`
--

CREATE TABLE IF NOT EXISTS `w_dispatch_queue` (
  `account_id` bigint(20) NOT NULL,
  `cotation_id` bigint(20) NOT NULL,
  PRIMARY KEY (`account_id`,`cotation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `w_enterprise`
--

CREATE TABLE IF NOT EXISTS `w_enterprise` (
  `enterprise_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enterprise_name` varchar(255) NOT NULL,
  `enterprise_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`enterprise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `w_enterprise`
--

INSERT INTO `w_enterprise` (`enterprise_id`, `enterprise_name`, `enterprise_address`) VALUES
(1, 'ADEMA', 'Ivato Aeroport'),
(2, 'EDELEC', 'Andraharo'),
(3, 'BFV-SG', 'Antaninarenina'),
(4, 'ETECH', 'Anosisoa');

-- --------------------------------------------------------

--
-- Structure de la table `w_proposition_proforma`
--

CREATE TABLE IF NOT EXISTS `w_proposition_proforma` (
  `proposition_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `price_unit` float NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`proposition_proforma_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `w_proposition_proforma`
--

INSERT INTO `w_proposition_proforma` (`proposition_proforma_id`, `ref`, `designation`, `price_unit`, `qty`, `cotation_fk`) VALUES
(1, '', 'Toshiba satellite 1202', 1500000, 1, 1),
(2, '', 'Acer Aspire 1640', 1200000, 2, 1),
(3, '', 'Samsung S4', 2000000, 2, 2),
(4, '', 'tatat', 1200, 2, 3),
(5, '', 'atatat', 1300, 3, 3),
(6, '', 'tatet', 1200, 3, 4),
(7, '', 'tetat', 1350, 2, 4),
(8, '', 'onduleur 650Va', 1200000, 1, 5),
(9, '', 'onduleur 800Va', 1500000, 1, 5),
(10, '', 'ttest', 1200000, 1, 6),
(11, '', 'ttestet', 1350000, 2, 6),
(12, '', 'testttt', 1200000, 2, 7),
(13, '', 'tttest', 1500000, 5, 7),
(14, '', 'ttererer', 1500000, 3, 8),
(15, '', 'ererretet', 1350000, 2, 8),
(16, '', 'ararar', 120000, 1, 9),
(17, '', 'rarara', 135000, 2, 9),
(18, '', 'atzeftzert', 120000, 1, 10),
(19, '', 'trzezer', 135000, 2, 10),
(20, '', 'gdfgdfg', 2000, 1, 11),
(21, '', 'hdfgfdg', 2500, 5, 11);

-- --------------------------------------------------------

--
-- Structure de la table `w_request_proforma`
--

CREATE TABLE IF NOT EXISTS `w_request_proforma` (
  `request_proforma_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(45) NOT NULL,
  `designation` varchar(2048) NOT NULL,
  `qty` float NOT NULL,
  `cotation_fk` int(11) NOT NULL,
  PRIMARY KEY (`request_proforma_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `w_request_proforma`
--

INSERT INTO `w_request_proforma` (`request_proforma_id`, `ref`, `designation`, `qty`, `cotation_fk`) VALUES
(1, '', 'Toshiba satellite 1202', 1, 1),
(2, '', 'Acer Aspire 1640', 2, 1),
(3, '', 'Samsung S4', 2, 2),
(4, '', 'tatat', 2, 3),
(5, '', 'atatat', 3, 3),
(6, '', 'tatet', 3, 4),
(7, '', 'tetat', 2, 4),
(8, '', 'onduleur 650Va', 1, 5),
(9, '', 'onduleur 800Va', 1, 5),
(10, '', 'ttest', 1, 6),
(11, '', 'ttestet', 2, 6),
(12, '', 'testttt', 2, 7),
(13, '', 'tttest', 5, 7),
(14, '', 'ttererer', 3, 8),
(15, '', 'ererretet', 2, 8),
(16, '', 'ararar', 1, 9),
(17, '', 'rarara', 2, 9),
(18, '', 'atzeftzert', 1, 10),
(19, '', 'trzezer', 2, 10),
(20, '', 'gdfgdfg', 1, 11),
(21, '', 'hdfgfdg', 5, 11),
(22, '', 'tara', 2, 12),
(23, '', 'secret', 3, 12),
(24, '', 'teetetet', 1, 13),
(25, '', 'ettte', 2, 13);

-- --------------------------------------------------------

--
-- Structure de la table `w_sessions`
--

CREATE TABLE IF NOT EXISTS `w_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `w_sessions`
--

INSERT INTO `w_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5f344e28ad48279a4df8067096823f31', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0', 1372855178, ''),
('c97c863947b39544f560c8166177162a', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36', 1374307286, 'a:5:{s:9:"user_data";s:0:"";s:10:"user_login";s:10:"dispatcher";s:14:"user_firstname";s:4:"Jean";s:9:"user_type";s:10:"DISPATCHER";s:10:"account_id";s:1:"8";}'),
('97fca662d141c95cf64315fc9c0534ce', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0', 1373044827, 'a:5:{s:9:"user_data";s:0:"";s:10:"user_login";s:10:"dispatcher";s:14:"user_firstname";s:4:"Jean";s:9:"user_type";s:10:"DISPATCHER";s:10:"account_id";s:1:"8";}');

-- --------------------------------------------------------

--
-- Structure de la vue `wv_customer`
--
DROP TABLE IF EXISTS `wv_customer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_customer` AS (select `c`.`customer_id` AS `customer_id`,`c`.`customer_mail` AS `customer_mail`,`c`.`customer_phone` AS `customer_phone`,`c`.`purchaser_name` AS `purchaser_name`,`c`.`enterprise_fk` AS `enterprise_fk`,`t`.`enterprise_id` AS `enterprise_id`,`t`.`enterprise_name` AS `enterprise_name`,`t`.`enterprise_address` AS `enterprise_address` from (`w_customer` `c` join `w_enterprise` `t`) where (`c`.`enterprise_fk` = `t`.`enterprise_id`));

-- --------------------------------------------------------

--
-- Structure de la vue `wv_proposition_proforma`
--
DROP TABLE IF EXISTS `wv_proposition_proforma`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_proposition_proforma` AS select `pr`.`proposition_proforma_id` AS `proposition_proforma_id`,`pr`.`ref` AS `ref`,`pr`.`designation` AS `designation`,`pr`.`price_unit` AS `price_unit`,`pr`.`qty` AS `qty`,`pr`.`cotation_fk` AS `cotation_fk`,(`pr`.`price_unit` * `pr`.`qty`) AS `amount` from `w_proposition_proforma` `pr`;

-- --------------------------------------------------------

--
-- Structure de la vue `wv_relance_cotation`
--
DROP TABLE IF EXISTS `wv_relance_cotation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `wv_relance_cotation` AS select `ct`.`cotation_id` AS `cotation_id`,`cs`.`enterprise_name` AS `customer_name`,`cs`.`enterprise_address` AS `customer_address`,`cs`.`customer_mail` AS `customer_mail`,`cs`.`customer_phone` AS `customer_phone`,`cs`.`purchaser_name` AS `purchaser_name`,`cm`.`first_name` AS `commercial_firstname`,`cm`.`last_name` AS `commercial_lastname`,`cm`.`acronym` AS `commercial_acronym`,`ct`.`cotation_source` AS `cotation_source`,`ct`.`reception_date` AS `reception_date`,`ct`.`limit_send_date` AS `limit_send_date`,`ct`.`validity_start` AS `validity_start`,`ct`.`validity_end` AS `validity_end`,`ct`.`cotation_status` AS `cotation_status`,`ct`.`cotation_number_customer` AS `cotation_number_customer`,`ct`.`cotation_number` AS `cotation_number`,`ct`.`customer_fk` AS `customer_fk`,`ct`.`dispatcher_fk` AS `dispatcher_fk`,`ct`.`commercial_fk` AS `commercial_fk`,(to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) AS `duration`,(`ct`.`validity_start` + interval ((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) / 3) day) AS `date1`,(`ct`.`validity_start` + interval (((to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) * 2) / 3) day) AS `date2`,(`ct`.`validity_start` + interval (to_days(`ct`.`validity_end`) - to_days(`ct`.`validity_start`)) day) AS `date3` from ((`w_cotation` `ct` join `wv_customer` `cs`) join `w_account` `cm`) where ((`ct`.`customer_fk` = `cs`.`customer_id`) and (`ct`.`commercial_fk` = `cm`.`account_id`));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
